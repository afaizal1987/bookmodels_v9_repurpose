$(document).ready(function(){
  $('.timepicker').timepicker({
      timeFormat: 'h:mm p',
      interval: 30,
      minTime: '08',
      maxTime: '11:00pm',
      defaultTime: '09',
      startTime: '09:00',
      dynamic: false,
      dropdown: true,
      scrollbar: true
  });
});
