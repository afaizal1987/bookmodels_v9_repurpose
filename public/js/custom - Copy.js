$(document).ready(function() {

	$("#slider").responsiveSlides({
		auto: true,
		pager: false,
		nav: true,
		pauseControls: true,
		random: true,
		speed: 500,
		timeout: 8000,
		manualControls: true,     // Selector: Declare custom pager navigation
		namespace: "callbacks"

	});

});