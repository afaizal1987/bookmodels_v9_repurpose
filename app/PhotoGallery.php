<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model
{
    //
    protected $table='photo_gallery';

    protected $fillable = ['user_id', 'image_name', 'thumbnail'];

    // public function photos(){
    //   return $this->belongsTo('App\User');
    // }
    public function index(){

    }
}
