<?php
namespace App;
use Laravel\Socialite\Contracts\User as ProviderUser;
class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $email = (!empty($providerUser->getEmail())) ? $providerUser->getEmail():str_slug($providerUser->getId()).str_random(4).'@bookmodels.asia';


            $user = User::whereEmail($email)->first();
            if (!$user) {
                $user = User::create([
                    'email' => $email,
                    'name' => $providerUser->getName(),
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }

    public function createOrGetVoting(ProviderUser $providerUser)
    {
        $account = Votingsc::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
		
        if ($account) {
			// $test = 'Teeston123';
			
			// echo '<pre>';
			// print_r($account);
			// echo '</pre>';
			// exit;
			
			// return$test;
            return $account->voting;
        } else {
            $account = new Votingsc([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $email = (!empty($providerUser->getEmail())) ? $providerUser->getEmail():str_slug($providerUser->getId()).str_random(4).'@bookmodels.asia';


            $user = Voting::whereEmail($email)->first();
            if (!$user) {
                $user = Voting::create([
                    'email' => $email,
                    'name' => $providerUser->getName(),
                ]);
            }
            $account->voting()->associate($user);
            $account->save();
            return $user;
        }
    }
}
