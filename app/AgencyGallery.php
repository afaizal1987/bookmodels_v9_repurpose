<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyGallery extends Model
{
    //
    protected $table='agency_gallery';

    protected $fillable = ['user_id', 'image_name', 'thumbnail'];

    public function index(){

    }
}
