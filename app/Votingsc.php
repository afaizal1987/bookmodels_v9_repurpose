<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votingsc extends Model
{
    protected $fillable = ['voting_id', 'provider_user_id', 'provider'];
    //Voters
    public function voting(){
      return $this->belongsTo(Voting::class);
    }
}
