<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'title',
        'agency_id',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'casting_date',
        'casting_time',
        'casting_start_date',
        'casting_start_time',
        'casting_end_date',
        'casting_end_time',
        'venue',
        'casting_venue',
        'location',
        'casting_location',
		'jobtype',
        'payment',
        'payment_terms',
        'gender',
        'age_min',
        'age_max',
        'height_min',
        'height_max',
        'language',
        'ethnicity',
        'additional_info',
        'enable',
    ];

    public function applications()
    {
        return $this->hasMany('App\JobApplication', 'job_id');
    }
	
	//Lets give this another shot
	public function application_status(){
		return $this->applications()->whereIn('status',[1,2,6]);
	}
}
