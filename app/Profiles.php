<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    //
    protected $fillable=[
      'user_id',
      'fullname',
      'nickname',
      'workemail',
      'phone_number',
      'gender_select',
      'location',
      'birthday',
      'nationality',
      'agency',
      'marital_status',
      'language',
      'ethnicity',
      'height',
      'weight',
      'Bust',
      'Waist',
      'Hips',
      'Leg_length',
      'Chest',
      'shoes',
      'piercing',
      'tattoos',
      'qualities',
      'experience',
      'interest',
      'terms',
    ];

    // public function model_User(){
    //   return $this->belongsTo('App\User');
    // }

    public function modelvotecount()
    {
        return $this->belongsTo('App\modelvotecount', 'profile_id');
    }
	
	
}
