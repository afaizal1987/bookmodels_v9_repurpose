<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgenciesProfiles extends Model
{
    //Yes.... this is the working version
    protected $fillable = [
      'user_id',
      'name',
      'company_name',
      'business_type',
      'email',
      'reg_no',
      'reg_type',
      'founding',
      'street_name',
      'postcode',
      'city',
      'state',
      'country',
      'contact_person',
      'phone_no',
      'website',
      'facebook',
      'twitter',
      'instagram',
      'terms',
    ];
}
