<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyRating extends Model
{
    //

    protected $fillable = [
       'user_id',
       'job_rate',
       'job_total'
   ];
}
