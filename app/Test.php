<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\JobApplication;
use App\Profiles;
use App\AgenciesProfiles;
use Auth;
class Test
{
	public static function testing($value1, $value2){

		$status = array(1,2,3,5);

		$testing = JobApplication::where('job_id',$value1)
		->where('user_id',$value2)
		->whereIn('status',$status)
		->first();
		if ($testing === null) {
		   return true;
		}
	}

	public static function name(){
		$value1 = Auth::user()->id;
		$testing = Profiles::select('nickname')
		->where('user_id',$value1)
		->first();

		$complink = strtr($testing['nickname'], array('.' => ' '));
		$newnickname=ucwords($complink);

		return $newnickname;
	}
	
	public static function name2(){
		$value1 = Auth::user()->id;
		$testing = Profiles::select('nickname')
		->where('user_id',$value1)
		->first();

		$newnickname=ucwords($testing['nickname']);

		return $newnickname;
	}

	public static function image(){
		$value1 = Auth::user()->id;
		$testing = Profiles::select('avatar')
		->where('user_id',$value1)
		->first();

		return $testing['avatar'];
	}

	public static function image2(){
		$value1 = Auth::guard('agency')->user()->id;
		$testing = AgenciesProfiles::select('avatar')
		->where('user_id',$value1)
		->first();

		return $testing['avatar'];
	}

}
