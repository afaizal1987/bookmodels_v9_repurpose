<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfVoting
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'voting')
	{
	    if (Auth::guard($guard)->check()) {
	        // return redirect('voting/home');
	        return redirect('modelsearch/home');
	    }

	    return $next($request);
	}
}