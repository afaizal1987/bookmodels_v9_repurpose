<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\SocialAccountService;
use Socialite; // socialite namespace

class SocialAuthController extends Controller
{
    // redirect function
    public function redirect(){
      // echo 'redirect';
      // exit;
      return Socialite::driver('facebook')->redirect();
    }
    // callback function
    public function callback(SocialAccountService $service){
      // when facebook call us a with token
      // echo 'callbackV';
      // exit;
      try{
        $socialUser = Socialite::driver('facebook')->user();
      }catch(\Exception $e){
        return redirect('/');
      }

      $user = $service->createOrGetUser($socialUser);

      auth()->login($user);


      // return redirect()->to('/models/home');
      return redirect()->to('/models/job_model');
    }

    public function redirectV(){
      // echo 'redirectV';
      // exit;
      return Socialite::driver('facebook2')->redirect();

    }
    // callback function
    public function callbackV(SocialAccountService $service){

      try{
        $socialUser = Socialite::driver('facebook2')->user();
      }catch(\Exception $e){
        /* echo 'Caught exception: ', $e->getMessage(), "\n"; */
		return redirect('/');

      }
      $user = $service->createOrGetVoting($socialUser);

      // auth()->login($user);
      \Auth::guard('voting')->login($user, true);


      return redirect()->to('modelsearch/home');
    }


}
