<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Profiles;
use App\User;
use App\Agency;
use App\AgenciesProfiles;
use Image;
use Illuminate\Support\Facades\Validator;

class FileUploadController extends Controller
{
  //
  public function __construct()
  {
      $this->middleware('auth');
  }


  protected function imageUploadPost(Request $request){

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);


        $user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->first();


        $profile_test = $profiles->profile_id;
        // Using image intervention
        $avatar = $request->file('image');
        $name_hash = $profile_test.'.'.$avatar->getClientOriginalName();
        $ext = $avatar->getClientOriginalExtension();
        $imageName = time().hash('md5',$name_hash).'.'.$ext;

        $imageDestination =  public_path('./uploads/avatars/');

        Image::make($avatar)->fit(300)->save($imageDestination.$imageName);

        $profiles->where('profile_id',$profile_test)->update(['avatar'=>$imageName]);

    	return back()
    		->with('success','Image Uploaded successfully.');

  }
  protected function imageUploadvar2(Request $request){
        $data = ($request['image']);
		
		
        $this->validate($request, [
            'image' => 'is_png',
        ]);

        $user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->first();
        $profile_test = $profiles->profile_id;

        $data = ($request['image']);
        list($type,$data) = explode(';',$data);
        list(, $data)      = explode(',', $data);

        $imageName = time().hash('md5',$user_id).'.jpg';

        $imageDestination =  public_path('./uploads/avatars/');

        $test = Image::make($data)->encode('jpg')->orientate();
		
		$testingO = $test->exif('Orientation');
		
		
        if($test->exif('Orientation')){
          $test = orientate($test, $test->exif('Orientation'));
        }
		
        $test->fit(640)->save($imageDestination.$imageName);

        $profiles->where('profile_id',$profile_test)->update(['avatar'=>$imageName]);

        $nickname = $profiles->nickname;

        return response(['data'=>$nickname]);
  }

  protected function orientate($image, $orientation)
  {
      switch ($orientation) {
        case 1:
          return $image;
        case 2:
          return $image->flip('h');
        case 3:
          return $image->rotate(180);
        case 4:
          return $image->rotate(180)->flip('h');
        case 5:
          return $image->rotate(-90)->flip('h');
        case 6:
          return $image->rotate(-90);
        case 7:
          return $image->rotate(-90)->flip('v');
        case 8:
          return $image->rotate(90);
        default:
          return $image;
      }
  }



  protected function imageregistration(){
      $user_id = Auth::user()->id;
      $profilesAvatar = Profiles::select('avatar')->where('user_id',$user_id)->first();

      $profilesImg = $profilesAvatar['avatar'];


      return view('modelling.imagereg', compact('profilesImg'));
  }

  protected function imageUploadPost2(){
      $user_id = Auth::user()->id;
      $profilesAvatar = Profiles::select('avatar')->where('user_id',$user_id)->first();
      $profilesnickname = Profiles::select('nickname')->where('user_id',$user_id)->first();

      $profilesImg = $profilesAvatar['avatar'];
	  $profilesNickname = $profilesnickname['nickname'];


      return view('modelling.imgupload', compact('profilesImg','profilesNickname'));
  }
  
  protected function imageUploadPost3(){
      $user_id = Auth::user()->id;
      $profilesAvatar = Profiles::select('avatar')->where('user_id',$user_id)->first();
      $profilesnickname = Profiles::select('nickname')->where('user_id',$user_id)->first();

      $profilesImg = $profilesAvatar['avatar'];
	  $profilesNickname = $profilesnickname['nickname'];


      return view('modelling.imgupload2', compact('profilesImg','profilesNickname'));
  }

  // Single photos
  protected function addphotos($id, Request $request){

    // Validation
    $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $file = $request->file('image');

    $name = time().hash('md5',$file->getClientOriginalName());

    //This is the thumbnail. Below is the real image.
    // This is working. So I wish to keep i intact
    $destinationPath = public_path('./uploads/model_photos/');
    $image= Image::make($file)->encode('png')->orientate();
    $image->resize(null, 380, function ($constraint) {
        $constraint->aspectRatio();
    });
    $savinggrace=$destinationPath.$name;

    $this->canvasing($image,$savinggrace);

	$image2= Image::make($file)->encode('jpg',100);
	$image2->orientate();
	
    //RealImage. Yes, folder is thumbnails
	$destinationPath2 = public_path('./uploads/thumbnails/');
	$image2->save($destinationPath2.$name);

    $photo = User::with('photos')->find($id);

    $photo->photos()->create([
      'image_name'=>"{$name}"
    ]);

    return back()
      ->with('success','Image Uploaded successfully.');
    // echo 'Working on it';
  }

  //from dropzone.js - Models
  protected function addmassphotos($id,Request $request){

    $this->validate($request, [
        'images' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $file = $request->file('images');

    $hashed = time().hash('md5',$file->getClientOriginalName());
    $ext = $file->getClientOriginalExtension();
    $name = $hashed.'.'.$ext;

    $destinationPath = public_path('./uploads/model_photos/');
    $image= Image::make($file)->resize(null, 380, function ($constraint) {
        $constraint->aspectRatio();
    });
	$image->orientate();
	
	$destinationPath2 = public_path('./uploads/thumbnails/');
	$image2= Image::make($file)->encode('jpg',100);
	$image2->orientate();
	
    $savinggrace=$destinationPath.$name;

    $this->canvasing($image,$savinggrace);
	$image2->save($destinationPath2.$name);
    //RealImage. Yes, folder is thumbnails
    // $file->move(public_path('./uploads/thumbnails/'), $name);

    $photo = User::with('photos')->find($id);

    $photo->photos()->create([
      'image_name'=>"{$name}"
    ]);

    return back()->with('success','Image Uploaded successfully.');

  }

  // Agency profile

  protected function agencyimage(Request $request){
    $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);
    $user_id=Auth::guard('agency')->user()->id;
    $profiles = AgenciesProfiles::where('user_id',$user_id)->first();
    $profile_test = $profiles->user_id;
    // Using image intervention
    $avatar = $request->file('image');
    $imageName = time().'_'.$profile_test.'.'.$avatar->getClientOriginalExtension();
    $imageDestination =  public_path('./uploads/avatars/');
    $image=Image::make($avatar)->encode('png');
    $image->resize(null, 180, function ($constraint) {
        $constraint->aspectRatio();
    });

    $savinggrace=$imageDestination.$imageName;

    $this->canvasingProfile($image,$savinggrace);

    $profiles->where('user_id',$user_id)->update(['avatar'=>$imageName]);

    return back()->with('success','Image Uploaded successfully.');
  }

  protected function agencyimage2(){
    $user_id=Auth::guard('agency')->user()->id;
    $profiles = AgenciesProfiles::where('user_id',$user_id)->first();

    $profilesImg = $profiles['avatar'];

    return view('agency.imgupload', compact('profilesImg'));
  }

  // Agency profile

  protected function agencyupload(Request $request){
    $this->validate($request, [
        'image' => 'is_png',
    ]);

    $user_id=Auth::guard('agency')->user()->id;
    $profiles = AgenciesProfiles::where('user_id',$user_id)->first();
    $profile_test = $profiles->profile_id;

    $data = ($request['image']);
    list($type,$data) = explode(';',$data);
    list(, $data)      = explode(',', $data);

    $imageName = time().hash('md5',$user_id).'.jpg';

    $imageDestination =  public_path('./uploads/avatars/');

    $test = Image::make($data)->encode('jpg');
    $test->fit(640)->save($imageDestination.$imageName);

    $profiles->where('user_id',$user_id)->update(['avatar'=>$imageName]);

    return response(['data'=>'home']);
  }

  // Agency gallery

  protected function agencygallery($id, Request $request){
    $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $file = $request->file('image');

    $name = time().hash('md5',$file->getClientOriginalName());

    //Thumbnail
    $destinationPath = public_path('/uploads/model_photos/');
    $image= Image::make($file)->resize(null, 400, function ($constraint) {
        $constraint->aspectRatio();
    })->orientate();
	
	$destinationPath2 = public_path('./uploads/thumbnails/');
	$image2= Image::make($file)->encode('jpg',100);
	$image2->orientate();
	
	$savinggrace=$destinationPath.$name;

    $this->canvasing($image,$savinggrace);
	$image2->save($destinationPath2.$name);
	
    $photo = Agency::with('photos')->find($id);

    $photo->photos()->create([
      'image_name'=>"{$name}"
    ]);

    return back()
      ->with('success','Image Uploaded successfully.');
    // echo 'Working on it';
  }

  //from dropzone.js - Agency
  protected function addagencymassphotos($id,Request $request){

    $this->validate($request, [
        'images' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $file = $request->file('images');

    $hashed = time().hash('md5',$file->getClientOriginalName());
    $ext = $file->getClientOriginalExtension();
    $name = $hashed.'.'.$ext;

    $destinationPath = public_path('./uploads/model_photos/');
    $image= Image::make($file)->resize(null, 400, function ($constraint) {
        $constraint->aspectRatio();
    })->orientate();
	
	$destinationPath2 = public_path('./uploads/thumbnails/');
	$image2= Image::make($file)->encode('jpg',100);
	$image2->orientate();
	
    $savinggrace=$destinationPath.$name;

    $this->canvasing($image,$savinggrace);
	$image2->save($destinationPath2.$name);
	
    //RealImage. Yes, folder is thumbnails
    // $file->move(public_path('./uploads/thumbnails/'), $name);

    // Change the user
    $photo = Agency::with('photos')->find($id);

    //Check the Agency function. Photos, to show relation
    $photo->photos()->create([
      'image_name'=>"{$name}"
    ]);

    return back()->with('success','Image Uploaded successfully.');

  }

  protected function canvasing($image,$savinggrace){
    $canvas = Image::canvas(220,330);
    $canvas->insert($image, 'center');
    $canvas->save($savinggrace);
  }
  protected function canvasingProfile($image,$savinggrace){
    $canvas = Image::canvas(180,180);
    $canvas->insert($image, 'center');
    $canvas->save($savinggrace);
  }

  public function verification(){
    return view('agency.verification');
  }



}
