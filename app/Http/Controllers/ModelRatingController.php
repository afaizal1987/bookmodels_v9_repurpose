<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\Job;
use App\JobApplication;
use App\PhotoGallery;
use App\Profiles;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ModelRatingController extends Controller
{
    //
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function job_rating(){
		$user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->first();
        $p_id = $profiles['profile_id'];
        $var = $profiles['birthday'];
        $date = date('Y-m-d', strtotime($var));
        $age = date_diff(date_create($date), date_create('today'))->y;
		
		$mytime = \Carbon\Carbon::now();
		$today =  $mytime->toDateString();

        $gender = array($profiles['gender_select'],'Any');
        $ethnicity = array($profiles['ethnicity'],'Any');

        $jobsgalore2 = Job::select('jobs.*','agencies_profiles.company_name','agencies_profiles.name')
						->leftJoin('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
						->where([
							['age_min','<=',$age],
							['age_max','>=',$age],
						])
						->whereIn('jobs.gender',$gender)
						->whereIn('jobs.ethnicity',$ethnicity)
						->where('start_date','>=',$today)
						->OrderBy('jobs.created_at','desc')
						->get();
		
		
		
		
		  // $jobtest=JobApplication::where('user_id',$p_id)->select('job_id')->get();
		  $jobtest = Job::select('jobs.*')
					->leftJoin('job_applications','jobs.id','=','job_applications.job_id')
					// ->leftJoin('profiles','profiles.user_id','=','job_applications.user_id')
					->where('job_applications.user_id',$p_id)
					->where('start_date','<=',$today)
					->get();

		 // dd($jobtest);
		 
		$jobtestcount = count($jobtest);
		$jobtestcount = 0;
		$jobsgalorecount = count($jobsgalore2);


		 $job3 = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name','agencies_profiles.name')
            ->Join('job_applications','jobs.id','=','job_applications.job_id')
            ->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
			->where('start_date','>=',$today)
            ->where('job_applications.user_id',$p_id)
            ->where('job_applications.status',3)
            // ->paginate(3);
            ->get();

			$applied = array(1,5);

          $job1 = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name','agencies_profiles.name')
            ->Join('job_applications','jobs.id','=','job_applications.job_id')
            ->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
			->where('start_date','>=',$today)
            ->where('job_applications.user_id',$p_id)
            ->whereIn('job_applications.status',$applied)
            ->get();

          $j3count = count($job3);
          $j1count = count($job1);


          $data=compact(
	         'jobtestcount',
	          'jobsgalorecount',
            'p_id',
            'jobsgalore2',
	          'jobtest',
            'user_id',
            'job3',
            'job1',
            'j3count',
            'j1count'
          );
		
		return view('modelling.jobs2', $data);
	}
}
