<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Mail\Contactus;

class ContactUsController extends Controller
{
    //
    public function contactus(){
      return view('contactus');
    }


    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function contactuspost(Request $request){


      $this->validate(request(), [
        'title' => 'required',
        'name' => 'required|alpha',
        'email' => 'required|email',
        'inquiry' => 'required',
        'math' => 'required|numeric'
      ]);

	  if($request['math'] !=8){
		return back()->with('failure', 'Opps! Your math answer is incorrect')->withInput();
	  }

	  $data = array(
		$request['title'],
		$request['name'],
		$request['email'],
		$request['inquiry'],
	  );

	  $this->contactusemail($data);
      //
      // $this->contactusemail();
      //
      return back()->with('success', 'Thank you for contacting us!');
    }

    public function contactusemail($input){

		$emailInquiry = new Contactus($input);

		\Mail::to('hello@bookmodels.asia')->send($emailInquiry);
		// \Mail::to('faizal@pixaworks.com')->send($emailInquiry);
    }
}
