<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Request;
use Nexmo;
use App\Profiles;
use App\User;
use Image;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    //
    protected function store(Request $request){
		$this->validate(request(),[
        'fullname' =>'required|max:255',
        'nickname' =>'required|without_spaces|without_specialchar|unique:profiles,nickname',
        'workemail' => 'required|unique:profiles,workemail',
        'phone_number' =>'numeric|min:10',
        'location' =>'required|max:255',
        'gender_select' =>'required',
        'birthday' =>'required',
        'nationality' =>'required|max:255',
        'agency' =>'required|max:255',
        'marital_status' =>'required',
        'language' =>'required|max:255',
        'ethnicity' =>'required',
        'height' =>'numeric|min:3',
        'weight' =>'numeric|min:2',
        'Waist' =>'numeric|min:2',
        'Hips' =>'numeric|min:2',
        'Leg_length' =>'numeric|min:2',
        'shoes' =>'numeric|min:2',
        'piercing' =>'max:255',
        'tattoos' =>'max:255',
        'qualities' =>'max:255',
        'experience' =>'required',
        'interest' =>'required'

      ]);

      $input = Request::all();
      // $input = serialize($input);

      unset($input['interest']);

      // var_dump($input);

      $interest = Request::get('interest');

      $email = Request::get('workemail');
      $interest = implode(',',$interest);

      $input['interest'] = $interest;
		
		
      $input2 = array_map('strtolower',$input);
      $input = array_map('ucwords',$input2);
		
		$input['nickname'] = $input2['nickname'];
	  
      $input['workemail'] = strtolower($email);

      if (preg_match('/^01/', $input['phone_number'])){
        $input['phone_number'] = '+6'.$input['phone_number'];
      }
	  
	  if (preg_match('/^60/', $input['phone_number'])){
        $input['phone_number'] = '+6'.$input['phone_number'];
      }

      if(empty($input['piercing'])&&isset($input['piercing'])){
        $input['piercing'] = 'None';
      }
      if(empty($input['tattoos'])&&isset($input['tattoos'])){
        $input['tattoos'] = 'None';
      }
      if(empty($input['qualities'])&&isset($input['qualities'])){
        $input['qualities'] = 'None';
      }
		$input['terms'] =1;

      $user = User::with('model_Profile')->find($input['user_id']);
      if($user->model_profile === null){
        $profile = new Profiles($input);
        $user->model_Profile()->save($profile);

        // redirect
        return redirect('./models/image_registration');
      }else{
		return redirect('./models/image_registration');
	  }

        // return redirect('./models/thankyou')->with('success','Profile has been Created! Check your Work email.');
    }

    public function update(){
      // var_dump($_POST);


      $input = Request::all();
      unset($input['_token']);
      unset($input['_method']);

      $user_id = $input['user_id'];
      $interest = Request::get('interest');
      $interest = implode(',',$interest);
      $input['interest'] = $interest;
      $input2 = strtolower($input['nickname']);
      Profiles::where('user_id',$user_id)
                ->update($input);

      return redirect("{$input2}")->with('status', 'Your profile is now updated!');
    }

    public function compgen(){
      var_dump($_GET);
      // $profiles = Profiles::findOrFail();
    }

    public function public($nickname){

      $nickname = str_replace('_',' ',$nickname);

      $profiles = Profiles::where('nickname',$nickname)->first();

      if(is_null($profiles)&& (empty($profiles))){
        $count = 0;
        return view('modelling.profile2', compact('count'));
      }

      $count=$profiles->count();
      $user_id = $profiles['user_id'];

      // Birthday special case

      $birthdate = new \DateTime($profiles['birthday']);
      $today = new \DateTime('today');
      $age = $birthdate->diff($today)->y;

      $photos = DB::table('photo_gallery')->select('image_name')->where('user_id',$user_id)->latest()->get();

      $arrayTest=[];

      foreach ($photos as $key => $value) {
        $arrayTest[]=$value->image_name;
      }


      $interest = array();
      $repeat = explode(',',$profiles->interest);
      if(in_array('1',$repeat)){
        array_push($interest, 'Acting');}

      if(in_array('2',$repeat)){
        array_push($interest, 'Runway');}

      if(in_array('3',$repeat)){
        array_push($interest, 'Print/Web');}

      if(in_array('4',$repeat)){
        array_push($interest, 'Sports');}

      if(in_array('5',$repeat)){
        array_push($interest, 'Swimsuit');}

      if(in_array('6',$repeat)){
        array_push($interest, 'Lingerie');}

      if(in_array('7',$repeat)){
        array_push($interest, 'Artistic Nude');}

      if(in_array('8',$repeat)){
        array_push($interest, 'Usher');}

        $interest = implode(",",$interest);

      $data = compact(
          'count',
          'arrayTest',
          'profiles',
          'repeat',
          'photos',
          'age',
          'interest'
      );

      return view('modelling.profile2', $data);
    }





}
