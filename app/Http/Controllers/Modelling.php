<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Nexmo;
use App\User;
use App\Job;
use App\JobApplication;
use App\PhotoGallery;
use App\Profiles;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\ThankYou;
//use Illuminate\Support\Facades\DB;

class Modelling extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['profile2']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // This is just a sample. We'll use the model profile's table for the display
        $user_id = \Auth::user()->id;
        // echo $user_id;
        $profiles = Profiles::where('user_id',$user_id)->first();
        if(!empty($profiles)){
          // return redirect('./models/profile');
          $input2 = strtolower($profiles['nickname']);
          $nickname = str_replace(' ','.',$input2);

          return redirect("{$nickname}");
		  // return redirect('/models/job_model');
        }else{
		// $this->registration();
          return view('modelling.registration');
        }

    }

    // List all jobs! And must be able to see further details
    public function jobs()
    {
        $user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->first();
        $p_id = $profiles['profile_id'];
        $var = $profiles['birthday'];
        $date = date('Y-m-d', strtotime($var));
        $age = date_diff(date_create($date), date_create('today'))->y;
		$mytime = \Carbon\Carbon::now();
		$today =  $mytime->toDateString();

        $gender = array($profiles['gender_select'],'Any');
        $ethnicity = array($profiles['ethnicity'],'Any');

        $jobsgalore2 = Job::select('jobs.*','agencies_profiles.company_name','agencies_profiles.name')
						->leftJoin('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
						->where([
							['age_min','<=',$age],
							['age_max','>=',$age],
						])
						->whereIn('jobs.gender',$gender)
						->whereIn('jobs.ethnicity',$ethnicity)
						->where('start_date','>=',$today)
						->OrderBy('jobs.created_at','desc')
						->get();

		  // $jobtest=JobApplication::where('user_id',$p_id)->select('job_id')->get();
		  $jobtest = Job::select('jobs.*')
          ->leftJoin('job_applications','jobs.id','=','job_applications.job_id')
          // ->leftJoin('profiles','profiles.user_id','=','job_applications.user_id')
         ->where('job_applications.user_id',$p_id)
		 ->get();

		// TEmporary measures. Be on the lookout. Faizal_1070516
		// $jobtestcount = count($jobtest);
		$jobtestcount =0;
		$jobsgalorecount = count($jobsgalore2);
		  // dd($jobsgalore2);
		  // where is the code to toggle the applied button? so in your javascript, you should check if the profile id is null
		  // you ther?

		  // so if the user id is null the person hasn't applied ? Yes.
		  // Initially... there was duplicates: one was a null, the other has a user id.
		  // sort of: Job A -> Person A; Job A-> Null. Hence theorWhereNull. The null went missing... so did my sanity
		  // if there's no null value, the nyou have to check the db. Usually I create a helper method, to check each
		  // item individually. So I don't do it on the query level but on the data level. So I use ajax, to
		  // change the status of each of the retrieved jobs. Or just have a status column only for the logged in user.
		  // so if status = 1, then the user would have applied else nah.
		  // Is it like how I've done it?
		  // yes but i don't see a join between your job_applications and your profiles table though. So you'll only get the same id every time
		  // Ok. Can you show me how it should be done? I am drawing blanks here

		//THis is the one that joins with the pivot
		// THe tables are Jobs and User
		// THe pivot is Job_applications. nope it has to join on the user_id
		// Go nuts
		// can I dd the data? yes. wait table name starts with a capital?
		// Ajay, hold on. There was a confusion with my friend the other day
		// THe pivot is for PROFILES table. Thats where all the information are stores. User are
		// Just for login, cause I was using fb login. So it will join profiles
		// where  is the age_min column? Which table? Try this out
		// is that it?
		// THe main problem?
		// referring to the age_min? yes..  thats it
		// age min is calculated
		// what's the min age? 18? yes.
		// is that it? Min age? that's the min age. the birthday has to be greater than 18, yes. Greater than 18, less than the age_max
		// what's the max age? i guess this would work?
		// Ajay, may I show whats my actual problem?
		// sure
		//Here goes
		//Right now, both can view testing 1 and testing 2
		//See the problem? So, when one yupper

		// $query = DB::table('job_applications')
		// ->join('jobs', 'jobs.id','=','job_applications.job_id')
		// ->join('profiles', 'profiles.user_id','=','job_applications.user_id')
		// ->WhereDate('profiles.birthday', '>=', Carbon::now()->subYears(60))
		// ->orWhereDate('profiles.birthday','<=', Carbon::now()->subYears(18))
		// ->latest('jobs.created_at')
		// ->select('job_applications.*','profiles.fullname','jobs.*')
		// ->get();

		// dd($query);

		//

		 $job3 = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name','agencies_profiles.name')
            ->Join('job_applications','jobs.id','=','job_applications.job_id')
            ->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
			->where('start_date','>=',$today)
            ->where('job_applications.user_id',$p_id)
            ->where('job_applications.status',3)
            // ->paginate(3);
            ->get();

			$applied = array(1,5);

          $job1 = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name','agencies_profiles.name')
            ->Join('job_applications','jobs.id','=','job_applications.job_id')
            ->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
			->where('start_date','>=',$today)
            ->where('job_applications.user_id',$p_id)
            ->whereIn('job_applications.status',$applied)
            ->get();

          $j3count = count($job3);
          $j1count = count($job1);


          $data=compact(
	         'jobtestcount',
	          'jobsgalorecount',
            'p_id',
            'jobsgalore2',
	          'jobtest',
            'user_id',
            'job3',
            'job1',
            'j3count',
            'j1count'
          );

        return view('modelling.jobs', $data);
    }


    // history of application
    public function history(){

      $user_id = Auth::user()->id;
      $profiles = Profiles::where('user_id',$user_id)->first();
      $p_id = $profiles['profile_id'];
	    $mytime = \Carbon\Carbon::now();
	    $today =  $mytime->toDateString();

		$jobsgalore = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name')
		  ->leftJoin('job_applications','jobs.id','=','job_applications.job_id')
		  ->leftJoin('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
		  ->where('job_applications.user_id',$p_id)
		  ->where('job_applications.status',2)
		  ->where('jobs.start_date','>=',$today)
		  ->orderBy('jobs.created_at','desc')
		  ->paginate(3);

		$jobsgalore_old = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name')
		  ->leftJoin('job_applications','jobs.id','=','job_applications.job_id')
		  ->leftJoin('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
		  ->where('job_applications.user_id',$p_id)
		  ->where('job_applications.status',2)
		  ->where('jobs.start_date','<',$today)
		  ->orderBy('jobs.created_at','desc')
		  ->paginate(3);


        $data=compact(
          'jobsgalore',
		      'jobsgalore_old',
          'p_id'
        );
      return view('modelling.history',$data);
    }


    public function profile2($nickname)
    {
        $profiles = Profiles::where('nickname',$nickname)->first();

        $user = Auth::user();

        if(is_null($profiles)&& (empty($profiles))){
          $count = 0;

          return view('modelling.profile2', compact('count'));
        }

        $age = $this->birthday($profiles['birthday']);

        $arrayTest = $this->imagesgallery($profiles['user_id']);

        $interest = $this->interest($profiles['interest']);

        $data = compact(
            'arrayTest',
            'profiles',
            'age',
            'interest',
            'user'
        );

        return view('modelling.profile', $data);
    }

    // Finding birthday
    public function birthday($date){
      $birthdate = new \DateTime($date);
      $today = new \DateTime('today');
      $age = $birthdate->diff($today)->y;

      return $age;
    }

    // Images
    protected function imagesgallery($user_id){
      $photos = DB::table('photo_gallery')->select('image_name')->where('user_id',$user_id)->latest()->get();

      $arrayTest=[];
      foreach ($photos as $key => $value) {
        # code...
        $arrayTest[]=$value->image_name;
      }

      $countdown = 10-($photos->count());

      for ($i=0; $i<$countdown; $i++){
        array_push($arrayTest,'photo_add.png');
      }

      return $arrayTest;
    }

    // Exploding interest
    public function interest($interest){

      $repeat = explode(',',$interest);
      $interest = array();

      if(in_array('1',$repeat)){
        array_push($interest, 'Acting');}

      if(in_array('2',$repeat)){
        array_push($interest, 'Runway');}

      if(in_array('3',$repeat)){
        array_push($interest, 'Print/Web');}

      if(in_array('4',$repeat)){
        array_push($interest, 'Sports');}

      if(in_array('5',$repeat)){
        array_push($interest, 'Swimsuit');}

      if(in_array('6',$repeat)){
        array_push($interest, 'Lingerie');}

      if(in_array('7',$repeat)){
        array_push($interest, 'Artistic Nude');}

      if(in_array('8',$repeat)){
        array_push($interest, 'Usher');}

      if (count($repeat)>0){
        $interest = implode(", ",$interest);
        }
      return $interest;
    }

    // Editting profile.
    public function registration()
    {
	    $user_id = Auth::user()->id;
		$profiles = Profiles::where('user_id',$user_id)->first();
	    if(!empty($profiles)){
        $input2 = strtolower($profiles['nickname']);
        $nickname = str_replace(' ','.',$input2);

        return redirect("{$nickname}")->with('status', 'Your profile has already been created');;
      }else{
		return view('modelling.registration');
	  }

    }

    public function yourname()
    {
      $name = $_POST['name'];
      $name = preg_replace('/[.,]/',' ', $name);
      $name = strtolower($name);
      // echo $name;
      // Agency
      if(!empty($name) && isset($name)){
        $count = Profiles::where('nickname', 'like', $name)->count();

        if($count !== 0) {
             return response([
                 'status'  => '0',
                 'message' => 'Name Taken'
             ]);
         }

         return response([
           'status'  => '1',
           'message' => 'Name Available'
         ]);
      }else{
        return response([
            'status'  => '0',
            'message' => 'Name Taken'
        ]);
      }

    }

    // Update profile. Progress and tabbing. You know why
    protected function update()
    {
        $user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->firstOrFail();
        $repeat = explode(',',$profiles->interest);


        return view('modelling.update',compact('profiles','repeat'));
    }

    public function thankyou()
    // public function thankyou($id)
    {
      $user_id = Auth::user()->id;
      $this->sendmessage($user_id);
      $this->sendemail($user_id);
        /* Auth::logout(); */

        return view('modelling.thankyou');
    }

    public function sendmessage($id){
      $phone = Profiles::where('user_id',$id)->first(['phone_number']);
      Nexmo::message()->send([
          'to' => $phone['phone_number'],
          'from' => '16105552344',
          'text' => 'Thank you for creating your model profile with BookModels.asia. Go to your Profile to update your personal details, or go to your Photos to upload photos of your recent work before applying for jobs at the Job Listing section. '
      ]);
    }

    public function sendemail($id){
      $nickname = Profiles::where('user_id',$id)->first(['nickname']);
      $workemail = Profiles::where('user_id',$id)->first(['workemail']);

      $email = new ThankYou(new Profiles(['nickname'=>$nickname]));

      \Mail::to($workemail['workemail'])->send($email);

    }



}
