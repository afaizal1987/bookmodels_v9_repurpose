<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AgenciesProfilesController extends Controller
{
    //
    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('agency');
    }

    public function profile(){

      $user_id=Auth::guard('agency')->user()->id;
      $agentProfile = AgenciesProfiles::where('user_id',$user_id)->first();

      return view('agency.profile', compact('agentProfile'));
    }

    

}
