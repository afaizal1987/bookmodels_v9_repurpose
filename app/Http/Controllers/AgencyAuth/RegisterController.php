<?php

namespace App\Http\Controllers\AgencyAuth;

use App\Agency;
use App\AgenciesProfiles;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/agency/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('agency.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|without_spaces|without_specialchar|max:255',
            'email' => 'required|email|max:255|unique:agencies',
            'password' => 'required|min:6|confirmed',
            'business_type' => 'required',
            'contact_person' => 'required|max:255',
            'reg_no' => 'max:255',
            'founding' => 'required|max:255',
            'reg_type' => 'required|max:255',
            'street_name' => 'required|max:255',
            'postcode' => 'required|max:255',
            'state' => 'required|max:255',
            'country' => 'required',
            'phone_no' => 'required|max:255',
            'website' => 'max:255',
            'facebook' => 'max:255',
            'twitter' => 'max:255',
            'instagram' => 'max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Agency
     */
    protected function create(array $data)
    {

        $user = Agency::create([
            'name' => strtolower($data['name']),
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

		if (preg_match('/^01/', $data['phone_no'])){
        $data['phone_no'] = '+6'.$data['phone_no'];
		  }

		  if (preg_match('/^60/', $data['phone_no'])){
			$data['phone_no'] = '+6'.$data['phone_no'];
		  }

        if(!empty($data['website'])){
          if(preg_match("/https?/", $data['website']) == 0)  {
               $data['website'] = 'http://'.$data['website'];
          }
        }
        if(!empty($data['facebook'])){
          if(preg_match("/https?/", $data['facebook']) == 0)  {
    				$data['facebook'] = 'http://'.$data['facebook'];
    		  }
        }
        if(!empty($data['twitter'])){
          if(preg_match("/https?/", $data['twitter']) == 0)  {
    				$data['twitter'] = 'http://'.$data['twitter'];
    		  }
        }
        if(!empty($data['instagram'])){
          if(preg_match("/https?/", $data['instagram']) == 0)  {
    				$data['instagram'] = 'http://'.$data['instagram'];
    		  }
        }

        $user->agency_profiles = AgenciesProfiles::create([
          'user_id'=>$user->id,
          'name' => strtolower($data['name']),
          'company_name'=>$data['agency_name'],
          'business_type'=>$data['business_type'],
          'email'=>$data['email'],
          'reg_no'=>$data['reg_no'],
          'reg_type'=>$data['reg_type'],
          'founding'=>$data['founding'],
          'street_name'=> $data['street_name'],
          'postcode'=> $data['postcode'],
          'city'=> $data['city'],
          'state'=> $data['state'],
          'country'=> $data['country'],
          'contact_person'=>$data['contact_person'],
          'phone_no'=>$data['phone_no'],
          'website'=>$data['website'],
          'facebook'=>$data['facebook'],
          'twitter'=>$data['twitter'],
          'instagram'=>$data['instagram'],
          'terms'=>1,
        ]);

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */

    public function showRegistrationForm()
    {
        return view('agency.auth.register');
    }

	public function showRegistrationForm2()
    {
        return view('agency.auth.register2');
    }

    public function yourname()
    {
      $name = $_POST['name'];
      // echo $name;
      // Agency
       $count = Agency::where('name', 'like', $name)->count();

       if($count !== 0) {
            return response([
                'status'  => '0',
                'message' => 'Name Taken'
            ]);
        }

        return response([
          'status'  => '1',
          'message' => 'Name Available'
        ]);
    }



    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('agency');
    }
}
