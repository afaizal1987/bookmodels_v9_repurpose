<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Alert;

class PhotoGallery extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function index(){

      $user_id = Auth::user()->id;

      $photos = $this->images($user_id);
      $count = 10-(count($photos));
      // $imagesEmpty = $photos->thumbnail;
      $array = [];

      foreach ($photos as $key => $value) {
        # code...
        $array[]=$value->image_name;
      }

      for ($i=0; $i<(10-($photos->count())); $i++){
        array_push($array,'photo_add.png');
      }

      $data = compact(
          'array',
          'user_id',
          'count'
      );

      return view('modelling.photo',$data);
    }

   protected function images($user_id){
     $photos = DB::table('photo_gallery')->select('image_name')->where('user_id',$user_id)->latest()->get();
     return $photos;
   }



    public function destroy(){
      $user_id = Auth::user()->id;
      $img_id = $_POST['img_id'];
      $imageDestination =  public_path('uploads/model_photos/');
      $imageThumbnails =  public_path('uploads/thumbnails/');

      unlink($imageDestination.$img_id);
      unlink($imageThumbnails.$img_id);
      $photos = DB::table('photo_gallery')->where('user_id',$user_id)->where('image_name',$img_id)->delete();

      return response(['delete'=>1]);
    }

    public function adestroy(){
      $user_id=Auth::guard('agency')->user()->id;
      $img_id = $_POST['img_id'];

      $photos = DB::table('agency_gallery')->where('agency_id',$user_id)->where('image_name',$img_id)->delete();

      return response(['delete'=>1]);
    }

    public function agallery(){
      $user_id=Auth::guard('agency')->user()->id;

      $photos = DB::table('agency_gallery')->select('image_name')->where('agency_id',$user_id)->latest()->get();

      $count = 10-(count($photos));
      // $imagesEmpty = $photos->thumbnail;
      $array = [];

      foreach ($photos as $key => $value) {
        # code...
        $array[]=$value->image_name;
      }

      for ($i=0; $i<(10-($photos->count())); $i++){
        array_push($array,'photo_add.png');
      }

      $data = compact(
          'array',
          'user_id',
          'count'
      );
      return view('agency.photo',$data);
    }
}
