<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobApplication;
use App\AgenciesProfiles;
use App\Job;
use Nexmo;
use Auth;
use Carbon;
use DB;
use App\Mail\Bookedagent;
use App\Mail\Castingcall;
use App\Mail\Bookedmodel;
use App\Mail\CancellationModel;
use Illuminate\Support\Facades\Input;

class JobsController extends Controller
{
  public function __construct()
  {
      $this->middleware('agency');
  }

    public function index()
    {
		$users[] = Auth::user();
		$users[] = Auth::guard()->user();
		$users[] = Auth::guard('agency')->user();
		$agency_id = auth()->guard('agency')->user()->id;
		$name = AgenciesProfiles::where('user_id',$agency_id)->first();
		$company_name = $name->company_name;

		// The original one. Will consult with Hussaini to include only the right status
		// $jobs = Job::where('agency_id', $agency_id)->withCount('applications')->paginate(6);

		$mytime = Carbon\Carbon::now();
		$today =  $mytime->toDateString();

		$jobs = Job::where('agency_id', $agency_id)
		->where('start_date','>=',$today)
		->withCount('application_status')
		->orderBy('created_at', 'DESC')
		->paginate(3);
		$data = compact(
			'jobs',
			'company_name'
		);

		// echo'<pre>';
		// var_dump($jobs);
		// echo'</pre>';

       return view('agency.jobs.index', $data);

    }

	public function index2()
    {
		$users[] = Auth::user();
		$users[] = Auth::guard()->user();
		$users[] = Auth::guard('agency')->user();
		$agency_id = auth()->guard('agency')->user()->id;
		$name = AgenciesProfiles::where('user_id',$agency_id)->first();
		$company_name = $name->company_name;

		// The original one. Will consult with Hussaini to include only the right status
		// $jobs = Job::where('agency_id', $agency_id)->withCount('applications')->paginate(6);

		$mytime = Carbon\Carbon::now();
		$today =  $mytime->toDateString();

		$jobs = Job::where('agency_id', $agency_id)
		->where('start_date','>=',$today)
		->withCount('application_status')
		->orderBy('created_at', 'DESC')
		->paginate(3);
		$data = compact(
			'jobs',
			'company_name'
		);

		// echo'<pre>';
		// var_dump($jobs);
		// echo'</pre>';

       return view('agency.casting.index2', $data);

    }

    public function create()
    {
      $users[] = Auth::user();
      $users[] = Auth::guard()->user();
      $users[] = Auth::guard('agency')->user();
        $user_id=Auth::guard('agency')->user()->id;
        $job = new Job();
        $route = 'jobs.store';
        $method = 'POST';

        $data = compact(
            'job',
            'route',
            'method'
        );

        return view('agency.jobs.create',$data);
    }

    public function store()
    {
		$input = Input::all();
        $input['agency_id'] = auth()->guard('agency')->user()->id;

		//Start Date
		$startdate = strtotime( $input['start_date'] );
		$input['start_date'] = date( 'Y-m-d', $startdate );

		//End Date
		$enddate = strtotime( $input['end_date'] );
		$input['end_date'] = date( 'Y-m-d', $enddate );
		$input['enable'] =1;


        $job = Job::create($input);

        // return redirect()->route('jobs.index')->with('message', "{$job->title} has been created");
		return redirect('agency/job_listing')->with('message', "{$job->title} has been created");
    }

    public function edit(Job $job)
    {
      $users[] = Auth::user();
      $users[] = Auth::guard()->user();
      $users[] = Auth::guard('agency')->user();
        $route = ['jobs.update', $job->id];
        $method = 'PATCH';

        $data = compact(
            'job',
            'route',
            'method'
        );


        return view('agency.jobs.edit', $data);
    }

    public function update(Job $job)
    {
        $input = Input::all();

		$input['agency_id'] = auth()->guard('agency')->user()->id;

		//Start Date
		$startdate = strtotime( $input['start_date'] );
		$input['start_date'] = date( 'Y-m-d', $startdate );

		//End Date
		$enddate = strtotime( $input['end_date'] );
		$input['end_date'] = date( 'Y-m-d', $enddate );
		$input['enable'] =1;

		if(!empty($input['casting_start_date'])){
		  $castingdate = strtotime( $input['casting_date'] );
		  $input['casting_start_date'] = date( 'Y-m-d', $castingdate );
		}else{
		  $input['casting_start_date'] = '0';
		  $input['casting_start_time'] = '0';
		  $input['casting_end_time'] = '0';
		  $input['casting_end_time'] = '0';
		}

		$job->update($input);

        // return redirect()->route('jobs.index')->with('message', "{$job->title} has been updated");
		return redirect('agency/job_listing')->with('message', "{$job->title} has been created");
    }

    // public function destroy(Job $job)
    // {
        // $title = $job->title;
        // $job->delete();
        // return redirect()->route('jobs.index')->with('delete_message', "{$title} has been deleted");

    // }

	public function disable(Request $request)
    {
		$input = Input::all();

		if($input['status']=='DISABLE'){

			Job::where('id',$input['job_id'])
			->update(['enable'=>0]);

			return response(['disabled'=>1]);
		}

		if($input['status']=='ENABLE'){
			Job::where('id',$input['job_id'])
			->update(['enable'=>1]);
			return response(['enabled'=>1]);
		}

    }

	public function edit2($id,Job $job)
    {

        $route = ['jobs.update', $job->id];
        $method = 'PATCH';


        $data = compact(
            'job',
            'route',
            'method'
        );


        return view('agency.jobs.edit2', $data);
    }

	public function history()
    {
		$users[] = Auth::user();
		$users[] = Auth::guard()->user();
		$users[] = Auth::guard('agency')->user();
		$agency_id = auth()->guard('agency')->user()->id;
		$name = AgenciesProfiles::where('user_id',$agency_id)->first();
		$company_name = $name->company_name;

		// Finding today's date
		$mytime = Carbon\Carbon::now();
		$today =  $mytime->toDateString();

		// The original one. Will consult with Hussaini to include only the right status
		// $jobs = Job::where('agency_id', $agency_id)->withCount('applications')->paginate(6);
		$jobs = Job::where('agency_id', $agency_id)
		->where('start_date','<',$today)
		->withCount('application_status')->paginate(3);
		$data = compact(
			'jobs',
			'company_name'
		);

		// echo'<pre>';
		// var_dump($jobs);
		// echo'</pre>';

       return view('agency.history', $data);

    }

    public function confirmJob(Request $request){
      $input = Input::all();
      $accepted = [
        'job_id'=>$input['job_id'],
        'user_id'=>$input['user_id'],
        'status'=>'2'
      ];
	  $denied = [
        'job_id'=>$input['job_id'],
        'user_id'=>$input['user_id'],
        'status'=>'5'
      ];

		if($input['submission']=='CONFIRM'){
			    $this->sendmessage($input['user_id'],$input['job_id']);
				$this->doubleemail($input['user_id'],$input['job_id']);
			    JobApplication::where('job_id',$input['job_id'])
			        ->where('user_id',$input['user_id'])
			           ->where('status',1)
			              ->update($accepted);

			     return response(['confirm'=>1]);

		}else if($input['submission']=='REMOVE'){
			$this->sendemail($input['user_id'],$input['job_id']);
			JobApplication::where('job_id',$input['job_id'])->where('user_id',$input['user_id'])->delete();
			return response(['remove'=>1]);
		}else{
			JobApplication::where('job_id',$input['job_id'])
			        ->where('user_id',$input['user_id'])
			           ->where('status',1)
			              ->update($denied);
        return response(['cancel'=>1]);

      }
    }

	// Cating Job. Cause we need specific ones. According to boss
	public function casting_create()
    {
      $users[] = Auth::user();
      $users[] = Auth::guard()->user();
      $users[] = Auth::guard('agency')->user();
        $user_id=Auth::guard('agency')->user()->id;

        return view('agency.casting.create');
    }

    public function casting_store()
    {
		$input = Input::all();


        $input['agency_id'] = auth()->guard('agency')->user()->id;

		//Start Date
		$startdate = strtotime( $input['start_date'] );
		$input['start_date'] = date( 'Y-m-d', $startdate );

		//End Date
		$enddate = strtotime( $input['end_date'] );
		$input['end_date'] = date( 'Y-m-d', $enddate );

		//Casting Start Date
		$castingstartdate = strtotime( $input['casting_start_date'] );
		$input['casting_start_date'] = date( 'Y-m-d', $castingstartdate );

		//Casting End Date
		if($input['casting_end_date']!=0){
			$castingenddate = strtotime( $input['casting_end_date'] );
			$input['casting_end_date'] = date( 'Y-m-d', $castingenddate );
		}else{
			$input['casting_end_date'] =0;
		}

		$input['enable'] =1;

        $job = Job::create($input);

        return redirect('agency/job_listing')->with('message', "{$job->title} has been created");
    }

    public function casting_edit($job_id)
    {
      $users[] = Auth::user();
      $users[] = Auth::guard()->user();
      $users[] = Auth::guard('agency')->user();

		$job = Job::where('id',$job_id)->first();

		$data = compact(
            'job'
        );

        return view('agency.casting.edit', $data);
    }

    public function casting_update(Job $job)
    {
        $input = Input::all();

		$input['agency_id'] = auth()->guard('agency')->user()->id;

		//Start Date
		$startdate = strtotime( $input['start_date'] );
		$input['start_date'] = date( 'Y-m-d', $startdate );

		//End Date
		$enddate = strtotime( $input['end_date'] );
		$input['end_date'] = date( 'Y-m-d', $enddate );

		//Casting Start Date
		$castingstartdate = strtotime( $input['casting_start_date'] );
		$input['casting_start_date'] = date( 'Y-m-d', $castingstartdate );

		//Casting End Date

		if($input['casting_end_date']!=0){
			$castingenddate = strtotime( $input['casting_end_date'] );
			$input['casting_end_date'] = date( 'Y-m-d', $castingenddate );
		}else{
			$input['casting_end_date'] =0;
		}

		$input['enable'] =1;

		$job_id = $input['job_id'];
		unset($input['job_id'],$input['_method'],$input['_token']);

		$job = Job::where('id',$job_id)->update($input);

        return redirect('agency/job_listing')->with('message', "{$input['title']} has been updated");
    }

    public function confirmCasting(Request $request){
		$input = Input::all();
		$accepted = [
			'job_id'=>$input['job_id'],
			'user_id'=>$input['user_id'],
			'status'=>'2'
		];

		$shorlisted = [
			'job_id'=>$input['job_id'],
			'user_id'=>$input['user_id'],
			'status'=>'6'
		];

		$denied = [
			'job_id'=>$input['job_id'],
			'user_id'=>$input['user_id'],
			'status'=>'5'
		];

		if($input['submission']=='SHORTLIST'){
			// $this->sendmessageShortlisted($input['user_id'],$input['job_id']);
			$this->sendcastingemail($input['user_id'],$input['job_id']);
			JobApplication::where('job_id',$input['job_id'])
				->where('user_id',$input['user_id'])
				->where('status',1)
				->update($shorlisted);
			return response(['shortlist'=>1]);
		}else if($input['submission']=='CONFIRM'){
			    $this->sendmessage($input['user_id'],$input['job_id']);
				$this->doubleemail($input['user_id'],$input['job_id']);
			    JobApplication::where('job_id',$input['job_id'])
			        ->where('user_id',$input['user_id'])
			        ->where('status',6)
			        ->update($accepted);
				return response(['confirm'=>1]);
		}else if($input['submission']=='REMOVE'){
			$this->sendemail($input['user_id'],$input['job_id']);
			JobApplication::where('job_id',$input['job_id'])->where('user_id',$input['user_id'])->delete();
			return response(['remove'=>1]);
		}else{
			JobApplication::where('job_id',$input['job_id'])
			        ->where('user_id',$input['user_id'])
			           ->where('status',1)
			              ->update($denied);
        return response(['cancel'=>1]);

      }
    }

	public function sendmessage($user_id,$job_id){
      $phone = \App\Profiles::where('profile_id',$user_id)->first(['phone_number']);
      $numpadded = sprintf("%04s",$job_id);
      $text = "Congratulations! You've been booked via BookModels.asia. Job ID: ".$numpadded.". Please login to check details.";

      Nexmo::message()->send([
          'to' => $phone['phone_number'],
          'from' => '16105552344',
          'text' => $text
      ]);
    }

	public function sendmessageShortlisted($user_id,$job_id){
      $phone = \App\Profiles::where('profile_id',$user_id)->first(['phone_number']);
      $numpadded = sprintf("%04s",$job_id);
      $text = "Congratulations! You've been shortlisted for Job ID: ".$numpadded." via BookModels.asia. Please login to check details.";

      Nexmo::message()->send([
          'to' => $phone['phone_number'],
          'from' => '16105552344',
          'text' => $text
      ]);
    }

	public function doubleemail($user_id,$job_id){

      $profiles = \App\Profiles::where('profile_id',$user_id)->first();
      $job = \App\Job::where('id',$job_id)->first();

      $value1 = Auth::user()->id;
      $testing = \DB::table('agencies_profiles')->where('user_id',$value1)->first();

      if(empty($job['additional_info'])){
        $job['additional_info'] = 0;
      }

	  if(empty($job['jobcastingdate'])){
        $job['casting_date'] = 0;
        $job['casting_time'] = 0;
      }

	  if(empty($job['language'])){
        $job['joblanguage'] = 0;
      }

      $input=([
        'talentname'=>$profiles['nickname'],
        'agencyname'=>$testing->company_name,
        'agencycontactperson'=>$testing->contact_person,
        'agencynumber'=>$testing->phone_no,
        'jobid'=>$job['id'],
        'jobtitle'=>$job['title'],
        'jobstartdate'=>$job['start_date'],
        'jobstarttime'=>$job['start_time'],
        'jobvenue'=>$job['venue'],
        'jobcity'=>$job['location'],
        'jobpayment'=>$job['payment'],
        'joblanguage'=>$job['language'],
        'jobcastingdate'=>$job['casting_date'],
        'jobcastingtime'=>$job['casting_time'],
        'jobinfo'=>$job['additional_info'],
        'emailmodel'=>$profiles['workemail'],
        'phonenumbermodel'=>$profiles['phone_number'],
      ]);

      // echo '<pre>';
      // print_r($input);
      // echo '</pre>';

      $emailAgent = new Bookedagent($input);
      $emailTalent = new Bookedmodel($input);

      $when = Carbon\Carbon::now()->addMinutes(2);

      \Mail::to($testing->email)->send($emailAgent);
      \Mail::to($profiles['workemail'])->later($when, $emailTalent);
	}

	public function sendemail($user_id,$job_id){

		$profiles = \App\Profiles::where('profile_id',$user_id)->first();
		$job = \App\Job::where('id',$job_id)->first(['title']);

		$value1 = Auth::user()->id;
		$testing = \DB::table('agencies_profiles')->where('user_id',$value1)->first(['contact_person']);

		$emailing=$profiles['workemail'];
		$input=([
			'job_contact' =>$profiles['nickname'],
			'nickname' => $testing->contact_person,
			'job_id' => $job_id,
			'job_title' => $job['title'],
		]);

		$email = new CancellationModel($input);
		\Mail::to($emailing)->send($email);
    }

	public function sendcastingemail($user_id,$job_id){

		$profiles = \App\Profiles::where('profile_id',$user_id)->first();
      $job = \App\Job::where('id',$job_id)->first();

      $value1 = Auth::user()->id;
      $testing = \DB::table('agencies_profiles')->where('user_id',$value1)->first();

      if(empty($job['additional_info'])){
        $job['additional_info'] = 0;
      }

	  if(empty($job['jobcastingdate'])){
        $job['casting_date'] = 0;
        $job['casting_time'] = 0;
      }

	  if(empty($job['language'])){
        $job['joblanguage'] = 0;
      }

      $input=([
        'talentname'=>$profiles['nickname'],
        'agencyname'=>$testing->company_name,
        'agencycontactperson'=>$testing->contact_person,
        'agencynumber'=>$testing->phone_no,
        'jobid'=>$job['id'],
        'jobtitle'=>$job['title'],
        'jobstartdate'=>$job['start_date'],
        'jobstarttime'=>$job['start_time'],
        'jobvenue'=>$job['venue'],
        'jobcity'=>$job['location'],
        'jobpayment'=>$job['payment'],
        'joblanguage'=>$job['language'],
        'jobstartcastingdate'=>$job['casting_start_date'],
        'jobendcastingdate'=>$job['casting_end_date'],
        'jobstartcastingtime'=>$job['casting_start_time'],
        'jobendcastingtime'=>$job['casting_end_time'],
        'jobcastingvenue'=>$job['casting_venue'],
        'jobcastinglocation'=>$job['casting_location'],
        'jobinfo'=>$job['additional_info'],
        'emailmodel'=>$profiles['workemail'],
        'phonenumbermodel'=>$profiles['phone_number'],
      ]);

		// dd($input);

      // echo '<pre>';
      // print_r($input);
      // echo '</pre>';

		$emailTalent = new Castingcall($input);

      \Mail::to($testing->email)->send($emailTalent);
    }

    public function ratethem(){

      echo 'Yes';

    }


}
