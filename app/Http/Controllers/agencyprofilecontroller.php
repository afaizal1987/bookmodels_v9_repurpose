<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AgenciesProfiles;
use App\Joblistings;
use Auth;

class agencyprofilecontroller extends Controller
{
    //

    public function __construct()
    {
		$this->middleware('agency', ['except' => ['agencypublic']]);
    }
    //
    public function profile(){
      $user_id=Auth::guard('agency')->user()->id;
      $linkname=Auth::guard('agency')->user()->name;
      $agentProfile = AgenciesProfiles::where('user_id',$user_id)->first();

      $array1 = array($agentProfile['street_name'],$agentProfile['postcode']);
      $agentProfile1=implode(", ",$array1);

      $array2 = array($agentProfile['city'],$agentProfile['state'],$agentProfile['country']);
      $agentProfile2=implode(", ",$array2);

      $agentProfile['office_address']=$agentProfile1.' '.$agentProfile2;

      $photos = DB::table('agency_gallery')->select('image_name')->where('agency_id',$user_id)->latest()->get();
      // $agentProfile['business_type'];

      switch($agentProfile['business_type']){
        case 'model_agency':
          $agentProfile['business_type'] = 'Model Agency';
          break;
        case 'event':
          $agentProfile['business_type'] = 'Event Management';
          break;
        case 'advertising':
          $agentProfile['business_type'] = 'Advertising Agency';
          break;
        case 'photographer':
          $agentProfile['business_type'] = 'Photographer';
          break;
        case 'fashion':
          $agentProfile['business_type'] = 'Fashion Designer';
          break;
        case 'others':
          $agentProfile['business_type'] = 'Others';
          break;
      }

      $arrayTest=[];
      foreach ($photos as $key => $value) {
        # code...
        $arrayTest[]=$value->image_name;
      }

      $countdown = 10-($photos->count());

      for ($i=0; $i<$countdown; $i++){
        array_push($arrayTest,'photo_add.png');
      }

      // echo '<pre>';
      // echo $agentProfile['business_type'];
      // echo '</pre>';

      $data=compact('agentProfile','agentProfilePhoto','arrayTest','linkname');

      return view('agency.profile', $data);
    }

    public function joblistings(){

      $user_id=Auth::guard('agency')->user()->id;
      $firstJobs = Joblistings::where('user_id',$user_id)->first();
      $increment = 1;
      if(!empty($firstJobs)){
        $allJobs = Joblistings::where('user_id',$user_id)->get();
        // echo 'Again?';
      }else{
        $allJobs = NULL;
      }

      // return view('agency.jobs', compact('allJobs','increment'));
    }

    public function agencypublic($agentlink){

      // $agentname = strtr($agentlink, array('.' => ' ', ',' => ' '));
      $agentname = ucwords($agentlink);

      $agentProfile = AgenciesProfiles::where('name',$agentname)->firstorFail();
      $linkname = $agentProfile['company_name'];
      $array1 = array($agentProfile['street_name'],$agentProfile['postcode']);
      $agentProfile1=implode(", ",$array1);

      $array2 = array($agentProfile['city'],$agentProfile['state'],$agentProfile['country']);
      $agentProfile2=implode(", ",$array2);

      $agentProfile['office_address']=$agentProfile1.' '.$agentProfile2;

      $photos = DB::table('agency_gallery')->select('image_name')->where('agency_id',$agentProfile['user_id'])->latest()->get();

	  // echo '<pre>';
	  // print_r($photos);
	  // echo '</pre>';

      // $agentProfile['business_type'];

      switch($agentProfile['business_type']){
        case 'model_agency':
          $agentProfile['business_type'] = 'Model Agency';
          break;
        case 'event':
          $agentProfile['business_type'] = 'Event Management';
          break;
        case 'advertising':
          $agentProfile['business_type'] = 'Advertising Agency';
          break;
        case 'photographer':
          $agentProfile['business_type'] = 'Photographer';
          break;
        case 'fashion':
          $agentProfile['business_type'] = 'Fashion Designer';
          break;
        case 'others':
          $agentProfile['business_type'] = 'Others';
          break;
      }

      $arrayTest=[];
      foreach ($photos as $key => $value) {
        # code...
        $arrayTest[]=$value->image_name;
      }

      $counter = count($arrayTest);

      $data=compact('agentProfile','agentProfilePhoto','arrayTest','linkname','counter');

      return view('agency.profile3', $data);

    }

    public function update(){
      $user_id=Auth::guard('agency')->user()->id;
      $linkname=Auth::guard('agency')->user()->name;
      $agentProfile = AgenciesProfiles::where('user_id',$user_id)->first();

      $data=compact(
        'agentProfile',
        'linkname'
      );
      return view('agency.update', $data);
    }

    public function updateProfile(Request $request){

      $user_id=Auth::guard('agency')->user()->id;

      if(!empty($request['website'])){
        if(preg_match("/https?/", $request['website']) == 0)  {
             $request['website'] = 'http://'.$request['website'];
        }
      }

      if(!empty($request['facebook'])){
        if(preg_match("/https?/", $request['facebook']) == 0)  {
          $request['facebook'] = 'http://'.$request['facebook'];
        }
      }
      if(!empty($request['twitter'])){
        if(preg_match("/https?/", $request['twitter']) == 0)  {
          $request['twitter'] = 'http://'.$request['twitter'];
        }
      }
      if(!empty($request['instagram'])){
        if(preg_match("/https?/", $request['instagram']) == 0)  {
          $request['instagram'] = 'http://'.$request['instagram'];
        }
      }

      $patchProfile = array(
        'business_type'=>$request['business_type'],
        'name'=>$request['name'],
        'company_name'=>$request['agency_name'],
        'reg_no'=>$request['reg_no'],
        'reg_type'=>$request['reg_type'],
        'founding'=>$request['founding'],
        'street_name'=>$request['street_name'],
        'postcode'=>$request['postcode'],
        'state'=>$request['state'],
        'city'=>$request['city'],
        'country'=>$request['country'],
        'contact_person'=>$request['contact_person'],
        'phone_no'=>$request['phone_no'],
        'website'=>$request['website'],
        'facebook'=>$request['facebook'],
        'twitter'=>$request['twitter'],
        'instagram'=>$request['instagram'],
      );

      AgenciesProfiles::where('user_id',$user_id)
      ->update($patchProfile);
      return redirect('agency/home')->with('status', 'Your profile is now updated!');


    }

}
