<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModelcheck;
use App\Modelvotecount;
use Illuminate\Support\Facades\DB;

class VotingCheckController extends Controller
{
    //
    protected function check(Request $request){

      $profile_id = $request['profile_id'];
      $user_id = $request['user_id'];

      $test = UserModelcheck::where('user_id',$user_id)->first();
		
      if(!empty($test)){
         return response('done');
      }else{
        $test2 = new UserModelcheck;
        $test2->profile_id = $profile_id;
        $test2->user_id = $user_id;
        $test2->save();

        $this->count($profile_id);

       return response('update');
      }
    }

    protected function count($profile_id){
      $counter = DB::table('modelvotecounts')->where('profile_id',$profile_id)->first();

      $test = $counter->vote;
      $counter->vote= ++$test;
      $wakakak = $counter->vote;

      Modelvotecount::where('profile_id',$profile_id)->update(['vote'=> $wakakak]);


      // $counter = Modelvotecount::where('profile_id',$profile_id)->get();



    }
}
