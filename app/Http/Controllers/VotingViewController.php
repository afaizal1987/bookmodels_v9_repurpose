<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profiles;

class VotingViewController extends Controller
{
    //
    public function __construct()
    {
        // $this->middleware('voting');
    }


    public function showLoginForm()
    {
        // $profiles = Profiles::with('votingcount')->get();
        $profiles = DB::table('profiles')
        ->join('modelvotecounts','profiles.profile_id','=','modelvotecounts.profile_id')
        ->get();

        $data=compact(
          'profiles'
        );

        return view('voting.auth.login',$data);
    }

}
