<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobApplication;
use App\Job;
use App\Profiles;
use Auth;
use DB;

use Illuminate\Support\Facades\Input;
use App\Mail\Cancellation;
use App\Mail\Applicationalert;

class JobModelController extends Controller
{
    //

    public function __construct()
    {
        // Check the auth why it has bugs
        $this->middleware('auth');
    }

    public function model_apply(Request $request)
    {
        $job_array = [
          'job_id'=>$request->job_id,
          'user_id'=>$request->id
        ];

        $job_save = [
          'job_id'=>$request->job_id,
          'user_id'=>$request->id,
          'status'=>'3'
        ];

        if($request->status == 'CANCEL'){
          JobApplication::where('job_id',$request->job_id)->where('user_id',$request->id)->delete();
          // return redirect('models/job_model')->with('message', 'Job has been deleted.');
          return response(['cancel'=>1]);
        }elseif($request->status == 'REMOVE'){
          JobApplication::where('job_id',$request->job_id)->where('user_id',$request->id)->delete();
          // return redirect('models/job_model')->with('message', 'Job has been deleted.');
          return response(['removed'=>1]);
        }elseif($request->status == 'SAVE'){
          $job_test = JobApplication::firstorCreate([
            'job_id'=>$request->job_id,
            'user_id'=>$request->id,
            'status'=>'3'
          ]);
          return response(['save'=>1]);
        }elseif($request->status == 'ATTEND'){
          $job_test = JobApplication::updateOrCreate([
				'job_id'=>$request->job_id,
				'user_id'=>$request->id],
				['status'=>1]);
			$this->appliedemail($request->job_id,$request->id);
          return response(['attend'=>1]);
        }else{
			$job_test = JobApplication::updateOrCreate([
				'job_id'=>$request->job_id,
				'user_id'=>$request->id],
				['status'=>1]);
			$this->appliedemail($request->job_id,$request->id);
          return response(['applied'=>1]);
        }
    }

    public function booking_cancel(Request $request)
    {
      $this->sendemail($request->job_id);
      JobApplication::where('job_id',$request->job_id)->where('user_id',$request->id)->delete();
      // return redirect('models/job_model')->with('message', 'Job has been deleted.');
      return response(['bookcancel'=>1]);
    }

    public function sendemail($job_id){
      $value1 = Auth::user()->id;
  		$testing = Profiles::select('nickname')->where('user_id',$value1)->first();
      $job_title = Job::select('title','agency_id')->where('id',$job_id)->first();
      $contact_person = \DB::table('agencies_profiles')->where('user_id',$job_title['agency_id'])->first(['contact_person','email']);
		$input=([
        'job_title'=>$job_title['title'],
        'job_id'=>$job_id,
        'job_contact'=>$contact_person->contact_person,
        'nickname'=>$testing['nickname'],
      ]);

      $email = new Cancellation($input);
      \Mail::to($contact_person->email)->send($email);
    }
	
	public function appliedemail($job_id,$user_id){
	
      $testing = Profiles::select('nickname','fullname')->where('profile_id',$user_id)->first();
      $job_title = Job::select('title','agency_id')->where('id',$job_id)->first();
      $contact_person = \DB::table('agencies_profiles')->where('user_id',$job_title['agency_id'])->first(['contact_person','email']);
		$input=([
        'job_title'=>$job_title['title'],
        'job_id'=>$job_id,
        'job_contact'=>$contact_person->contact_person,
        'talentname'=>$testing['nickname'],
        'fullname'=>$testing['fullname'],
      ]);
		
      $email = new Applicationalert($input);
      \Mail::to($contact_person->email)->send($email);
    }
	
	public function jobinfo($job_id){
		echo $job_id;
	}

}
