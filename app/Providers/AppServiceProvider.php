<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('is_png',function($attribute, $value, $params, $validator) {

          return $result = 'reached';

          // $image = base64_decode($value);
          // $f = finfo_open();
          // $result = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);
          // return $result == 'image/png';
        });

        Validator::extend('without_spaces', function ($attribute, $value, $parameters, $validator) {

            return preg_match("/^\S*$/u", $value);
        });

        Validator::extend('without_specialchar', function ($attribute, $value, $parameters, $validator) {

            return preg_match("/^[a-zA-Z0-9.]+$/", $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
      if ($this->app->environment() == 'local') {
          $this->app->register('Hesto\MultiAuth\MultiAuthServiceProvider');
      }
    }
}
