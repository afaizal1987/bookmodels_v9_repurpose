<?php

namespace App\Http\Controllers;

use Auth;
use Nexmo;
use App\User;
use App\Job;
use App\JobApplication;
use App\PhotoGallery;
use App\Profiles;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Modelling extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['profile2']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // This is just a sample. We'll use the model profile's table for the display
        $user_id = Auth::user()->id;
        // echo $user_id;
        $profiles = Profiles::where('user_id',$user_id)->first();
        if(!empty($profiles)){
          // return redirect('./models/profile');
          $input2 = strtolower($profiles['nickname']);
          $nickname = str_replace(' ','.',$input2);

          // return redirect("{$nickname}");
		  return redirect('/models/job_model');
		  
        }else{
          return view('modelling.registration');
        }

    }

    // List all jobs! And must be able to see further details
    public function jobs()
    {
        $user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->first();
        $p_id = $profiles['profile_id'];
        $var = $profiles['birthday'];
        $date = date('Y-m-d', strtotime($var));
        $age = date_diff(date_create($date), date_create('today'))->y;
        // $birthdate = new \DateTime($date);
        // $today = new \DateTime('today');
        // $age = $birthdate->diff($today)->y;
        $gender = array($profiles['gender_select'],'Any');
        $ethnicity = array($profiles['ethnicity'],'Any');

        $jobsgalore2 = Job::select('jobs.*','job_applications.status','job_applications.user_id')
			->Join('job_applications','jobs.id','=','job_applications.job_id')
			->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
			->where([
                    ['age_min','<=',$age],
                    ['age_max','>=',$age],
                  ])
			->where([
                    ['height_min','<=',$age],
                    ['height_max','>=',$age],
                  ])
			->whereIn('gender',$gender)
			->whereIn('ethnicity',$ethnicity)
			->groupBy('job_applications.job_id')
			->OrderBy('jobs.created_at','desc')
			->paginate(6);


          $array1 = array(
                    'user_id'=>$p_id,
                    'status'=>'3'
                  );

          $job3 = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name')
            ->Join('job_applications','jobs.id','=','job_applications.job_id')
            ->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
            ->where('job_applications.user_id',$p_id)
            ->where('job_applications.status',3)
            ->paginate(6);
          $job1 = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name')
            ->Join('job_applications','jobs.id','=','job_applications.job_id')
            ->Join('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
            ->where('job_applications.user_id',$p_id)
            ->where('job_applications.status',1)
            ->paginate(6);

          $job3c = JobApplication::where('job_applications.user_id',$p_id)
              ->where('job_applications.status',3)->get();
          $job1c = JobApplication::where('job_applications.user_id',$p_id)
              ->where('job_applications.status',1)->get();

          $j3count = count($job3c);
          $j1count = count($job1c);


          $data=compact(
            'p_id',
            'jobsgalore2',
            'user_id',
            'job3',
            'job1',
            'j3count',
            'j1count'
          );

        return view('modelling.jobs', $data);
    }

    // history of application
    public function history(){

      $user_id = Auth::user()->id;
      $profiles = Profiles::where('user_id',$user_id)->first();
      $p_id = $profiles['profile_id'];
      $jobsgalore = Job::select('jobs.*','job_applications.status','job_applications.user_id','agencies_profiles.company_name')
      ->leftJoin('job_applications','jobs.id','=','job_applications.job_id')
      ->leftJoin('agencies_profiles','jobs.agency_id','=','agencies_profiles.user_id')
      ->where('job_applications.user_id',$p_id)
      ->where('job_applications.status',2)
      ->orderBy('jobs.created_at','desc')
      ->paginate(6);

        // var_dump($jobsgalore);

        $data=compact(
          'jobsgalore',
          'p_id'
        );
      return view('modelling.history',$data);
    }


    public function profile2($nickname)
    {
        $nickname2 = str_replace('.',' ',$nickname);
        $profiles = Profiles::where('nickname',$nickname2)->first();

        $user = Auth::user();

        if(is_null($profiles)&& (empty($profiles))){
          $count = 0;

          return view('modelling.profile2', compact('count'));
        }

        $age = $this->birthday($profiles['birthday']);

        $arrayTest = $this->imagesgallery($profiles['user_id']);

        $interest = $this->interest($profiles['interest']);

        $data = compact(
            'arrayTest',
            'profiles',
            'age',
            'interest',
            'user'
        );

        return view('modelling.profile', $data);
    }

    // Finding birthday
    public function birthday($date){
      $birthdate = new \DateTime($date);
      $today = new \DateTime('today');
      $age = $birthdate->diff($today)->y;

      return $age;
    }

    // Images
    protected function imagesgallery($user_id){
      $photos = DB::table('photo_gallery')->select('image_name')->where('user_id',$user_id)->latest()->get();

      $arrayTest=[];
      foreach ($photos as $key => $value) {
        # code...
        $arrayTest[]=$value->image_name;
      }

      $countdown = 10-($photos->count());

      for ($i=0; $i<$countdown; $i++){
        array_push($arrayTest,'photo_add.png');
      }

      return $arrayTest;
    }

    // Exploding interest
    public function interest($interest){

      $repeat = explode(',',$interest);
      $interest = array();

      if(in_array('1',$repeat)){
        array_push($interest, 'Acting');}

      if(in_array('2',$repeat)){
        array_push($interest, 'Runway');}

      if(in_array('3',$repeat)){
        array_push($interest, 'Print/Web');}

      if(in_array('4',$repeat)){
        array_push($interest, 'Sports');}

      if(in_array('5',$repeat)){
        array_push($interest, 'Swimsuit');}

      if(in_array('6',$repeat)){
        array_push($interest, 'Lingerie');}

      if(in_array('7',$repeat)){
        array_push($interest, 'Artistic Nude');}

      if(in_array('8',$repeat)){
        array_push($interest, 'Usher');}

      if (count($repeat)>0){
        $interest = implode(", ",$interest);
        }
      return $interest;
    }

    // Editting profile.
    public function registration()
    {
        return view('modelling.registration');
    }

    public function yourname()
    {
      $name = $_POST['name'];
      // echo $name;
      // Agency
       $count = Profiles::where('nickname', 'like', $name)->count();

       if($count !== 0) {
            return response([
                'status'  => '0',
                'message' => 'Name Taken'
            ]);
        }

        return response([
          'status'  => '1',
          'message' => 'Name Available'
        ]);
    }

    // Update profile. Progress and tabbing. You know why
    protected function update()
    {
        $user_id = Auth::user()->id;
        $profiles = Profiles::where('user_id',$user_id)->firstOrFail();
        $repeat = explode(',',$profiles->interest);


        return view('modelling.update',compact('profiles','repeat'));
    }

    public function thankyou()
    // public function thankyou($id)
    {
      $user_id = Auth::user()->id;
      $this->sendmessage($user_id);
      $this->sendemail($user_id);
        /* Auth::logout(); */
        // $email = Profile->workemail

        return view('modelling.thankyou');
    }

    public function sendmessage($id){
      $phone = Profiles::where('user_id',$id)->first(['phone_number']);
      Nexmo::message()->send([
          'to' => $phone['phone_number'],
          'from' => '16105552344',
          'text' => 'Thank you for creating your model profile with BookModels.asia. Go to your Profile to update your personal details, or go to your Photos to upload photos of your recent work before applying for jobs at the Job Listing section. '
      ]);
    }

    public function sendemail($id){
      $phone = Profiles::where('user_id',$id)->first(['phone_number']);

    }


}
