<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
  protected $fillable = [
     'job_id',
     'user_id',
     'status',
 ];

 public function profile()
 {
     return $this->belongsTo('App\Profiles', 'user_id', 'profile_id');
 }

 public function job()
 {
     return $this->belongsTo('App\Job');
 }

 public function user(){
   return $this->belongsTo('App\Profiles','user_id');
 }

}
