<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Applicationalert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $input;

	public function __construct($input)
	{
		$this->input = $input;
	}


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$job_id = (sprintf("%04s",$this->input['job_id']));
		$talentname = ucwords($this->input['fullname']);
		$talentfullname = substr($talentname,0,20);
		$jobtitle = $this->input['job_title'];
		$subject = $talentname." has applied for ".$jobtitle." [Job ID: ".$job_id."]";
		$name = 'Sasha from BookModels.asia';
		$address = 'support@bookmodels.asia';
		return $this->view('email.Applied')
						  ->from($address, $name)
				  ->subject($subject)
				  ->with(['input'=>$this->input,]);
    }
}
