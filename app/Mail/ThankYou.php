<?php

namespace App\Mail;
use App\Profiles;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYou extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $nickname;


    public function __construct(Profiles $nickname)
    {
        //
        $this->nickname = $nickname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $address = 'hello@bookmodels.asia';
      $name = 'BookModels.asia Admin!';
      $subject = 'Welcome to BookModels.asia';

      return $this->view('email.ThankYou')
                  ->from($address, $name)
                  ->replyTo($address, $name)
                  ->subject($subject);
    }
}
