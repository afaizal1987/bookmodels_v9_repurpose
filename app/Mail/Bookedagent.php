<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Bookedagent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     protected $input;


     public function __construct($input)
     {
       $this->input = $input;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $test = ucfirst($this->input['talentname']);
      $job_id = (sprintf("%04s",$this->input['jobid']));
      $subject = 'You have booked '.$test.' [Job ID: '.$job_id.']';
      $name = 'Sasha from BookModels.asia';
      $address = 'support@bookmodels.asia';
      return $this->view('email.BookedAgent')
				          ->from($address, $name)
                  ->subject($subject)
                  ->with(['input'=>$this->input,]);
    }
}
