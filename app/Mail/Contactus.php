<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class Contactus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	 
	protected $input;
	
    public function __construct($input)
    {
		$this->input = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		
		// return $this->view('email.ShortlistTalents')
				          // ->from($address, $name)
                  // ->subject($subject)
                  // ->with(['input'=>$this->input,]);
				  
		$address = 'support@bookmodels.asia';
		$name = 'Bookmodels.asia Support';
		$subject = 'Queries from '.$this->input[1];
		
        return $this->view('email.Contactus')
					->from($address, $name)
					->subject($subject)
					->with(['input'=>$this->input,]);
    }
}
