<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class Cancellation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $input;


    public function __construct($input)
    {
      $this->input = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$subject = 'Cancellation of Booking';
		$name = 'Sasha from BookModelsAsia';
		$address = 'noreply@Bookmodels.asia';
        return $this->view('email.Cancellation')
					->from($address, $name)
					->subject($subject)
                    ->with(['input'=>$this->input,]);
    }
}
