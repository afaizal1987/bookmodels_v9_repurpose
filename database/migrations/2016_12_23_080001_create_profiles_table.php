<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('profile_id');
            $table->integer('user_id')->references('id')->on('users')->unique();
            $table->string('fullname');
            $table->string('nickname');
            $table->string('workemail');
            $table->string('birthday');
            $table->string('gender_select');
            $table->string('nationality');
            $table->string('language');
            $table->string('ethnicity');
            $table->integer('height');
            $table->integer('weight');
            $table->integer('bust')->nullable();
            $table->integer('waist');
            $table->integer('hips')->nullable();
            $table->integer('leg_length')->nullable();
            $table->integer('chest')->nullable();
            $table->integer('shoes')->nullable();
            $table->string('piercing')->nullable();
            $table->string('tattoos')->nullable();
            $table->string('qualities')->nullable();
            $table->string('experience');
            $table->string('interest');
            $table->string('avatar')->default('default.png');
            $table->string('phone_number',256);
            $table->string('location',256);
            $table->string('agency',256);
            $table->string('marital_status',256);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
