<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('agencies_profiles', function (Blueprint $table) {
            $table->increments('ap_id');
            $table->integer('user_id')->references('id')->on('agencies')->unique();
            $table->string('name');
            $table->string('reg_no');
            $table->string('reg_type');
            $table->string('email');
            $table->string('office_address');
            $table->string('contact_person');
            $table->string('phone_no');
            $table->string('verification')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies_profiles');
    }
}
