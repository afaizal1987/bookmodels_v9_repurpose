<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmintalentsActivationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admintalents_activations', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_admintalents')->unsigned();
        $table->foreign('id_admintalents')->references('id')->on('admintalents')->onDelete('cascade');
        $table->string('token');
        $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
      });

      Schema::table('admintalents', function (Blueprint $table) {
          $table->boolean('is_activated')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop("admintalents_activations");
		Schema::table('admintalents', function (Blueprint $table) {
			$table->dropColumn('is_activated');
		});
    }
}
