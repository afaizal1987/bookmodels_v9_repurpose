<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Joblistings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('joblistings', function(Blueprint $table){
          $table->increments('job_id');
          $table->integer('user_id');
          $table->date('job_date');
          $table->time('start_time');
          $table->time('end_time');
          $table->string('address');
          $table->string('payment');
          $table->string('payment_terms');
          $table->string('gender');
          $table->integer('age_min');
          $table->integer('age_max');
          $table->integer('height_min');
          $table->integer('height_max');
          $table->string('ethnicity');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('joblistings', function($table) {
                $table->dropColumn('job_id');
                $table->dropColumn('user_id');
                $table->dropColumn('job_date');
                $table->dropColumn('start_time');
                $table->dropColumn('end_time');
                $table->dropColumn('address');
                $table->dropColumn('payment');
                $table->dropColumn('payment_terms');
                $table->dropColumn('gender');
                $table->dropColumn('age_min');
                $table->dropColumn('age_max');
                $table->dropColumn('height_min');
                $table->dropColumn('height_max');
                $table->dropColumn('ethnicity');
           });
    }
}
