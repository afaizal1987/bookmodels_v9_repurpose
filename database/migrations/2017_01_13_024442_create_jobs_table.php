<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('agency_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->date('casting_date');
            $table->time('casting_time');
            $table->string('venue');
            $table->string('location');
            $table->string('payment');
            $table->string('payment_terms');
            $table->integer('gender');
            $table->integer('age_min');
            $table->integer('age_max');
            $table->integer('height_min');
            $table->integer('height_max');
            $table->string('language');
            $table->string('ethnicity');
            $table->string('additional_info');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
