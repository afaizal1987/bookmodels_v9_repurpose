<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('voting')->user();
	$voting_id = auth()->guard('voting')->user()->id;
    $profiles = \DB::table('profiles')
    ->join('modelvotecounts','profiles.profile_id','=','modelvotecounts.profile_id')
	->orderBy('profiles.nickname', 'asc')
    ->get();

    $data=compact(
      'profiles',
      'voting_id'
    );

    //dd($users);

    return view('voting.home',$data);
})->name('home');
Route::post('/verify','VotingCheckController@check');
