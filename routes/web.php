<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	/* When it goes live */
    return view('welcome');
    // return view('welcome2');

});

// Route::get('/welcomedemo', function () {
	// /* When it goes live */
    // return view('welcome');
    // return view('welcome2');

// });


Route::get('/agency', function () {
    return view('agencylanding');
});

Route::get('/signup',function(){
	return view('modelling.signupmodel');
});



//Only agency
Route::group(['prefix' => 'agency'], function () {
  Route::get('/login', 'AgencyAuth\LoginController@showLoginForm');
  Route::post('/login', 'AgencyAuth\LoginController@login');
  Route::post('/logout', 'AgencyAuth\LoginController@logout');

  Route::get('/register', 'AgencyAuth\RegisterController@showRegistrationForm');
  Route::get('/register2', 'AgencyAuth\RegisterController@showRegistrationForm2');
  Route::post('/register', 'AgencyAuth\RegisterController@register');

  //Yourname checker
  Route::post('/yourname', 'AgencyAuth\RegisterController@yourname');

  Route::post('/password/email', 'AgencyAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AgencyAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AgencyAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AgencyAuth\ResetPasswordController@showResetForm');


  Route::model('jobs', 'Job');
  Route::resource('jobs', JobsController::class); // Finds the corresponding routes. Use this to make the system faster


  // Any new routes... please go to agency.php. Else... no authenticated user
});

//Only for the modelsearch. The human models
Route::group(['prefix' => 'modelsearch'], function () {
	Route::get('', 'VotingAuth\LoginController@showLoginForm');
	Route::post('', 'VotingAuth\LoginController@login');
	Route::post('/logout', 'VotingAuth\LoginController@logout');
	Route::get('/redirectv', 'SocialAuthController@redirectV');
	Route::get('/callback', 'SocialAuthController@callbackV');
});

//Models search
Route::group(['prefix'=>'models'],function(){

  // Modelling setting
  Route::get('/home', 'Modelling@index');
  Route::get('/registration', 'Modelling@registration');
	Route::post('/yourname', 'Modelling@yourname');
  Route::get('/update', 'Modelling@update');
  Route::get('/profile', 'Modelling@profile');
  Route::post('/profile', 'FileUploadController@update_avatar');
  Route::patch('/img_upload', 'FileUploadController@imageUploadPost');

  Route::patch('/img_upload2', 'FileUploadController@imageUploadvar2');
  Route::get('/image_upload', 'FileUploadController@imageUploadPost2');
  Route::get('/job_model', 'Modelling@jobs');
  Route::get('/thankyou', 'Modelling@thankyou');

  // Profile pages
  Route::get('/compcard', 'ProfileController@compgen');
  Route::post('/edit_test','ProfileController@store');
  Route::patch('/update_test','ProfileController@update'); // Remember dimwit... If patch, must meet with patch

  // Jobs
  Route::post('/job_test','JobModelController@model_apply');
  Route::post('/book_cancel','JobModelController@booking_cancel');

  //File upload. In this case images
  Route::get('/image_registration','FileUploadController@imageregistration');
  Route::post('/{id}/photos','FileUploadController@addphotos');
  Route::post('/{id}/massphotos','FileUploadController@addmassphotos');
  Route::get('/myphotos','PhotoGallery@index');
  Route::post('/destroyImage','PhotoGallery@destroy');

  Route::get('/bookings', 'Modelling@history');
	Route::get('/image_upload2', 'FileUploadController@imageUploadPost3');

	Route::get('/ratings', 'ModelRatingController@job_rating');
});


Auth::routes();

// Route::get('/home', 'HomeController@index');

//After this we shall try with the model prefix. Have to make it look good
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

// All them main pages
Route::get('/about', function () { return view('about'); });
Route::get('/plans', function () { return view('plans'); });
Route::get('/terms', function () { return view('terms'); });
Route::get('/privacy', function () { return view('privacy'); });
Route::get('/help', function () { return view('help'); });
Route::get('/contact', function () { return view('contact'); });

Route::get('/{nickname}', 'Modelling@profile2');
Route::get('/a/{nickname}', 'agencyprofilecontroller@agencypublic');
Route::get('/job/{job_id}', 'agencyprofilecontroller@agencypublic');


//Test activation and admin panel
Route::group(['prefix' => 'admintalent'], function () {
  Route::get('/login', 'AdmintalentAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdmintalentAuth\LoginController@login');
  Route::post('/logout', 'AdmintalentAuth\LoginController@logout');

  Route::get('/register', 'AdmintalentAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdmintalentAuth\RegisterController@register');

  Route::post('/password/email', 'AdmintalentAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdmintalentAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdmintalentAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdmintalentAuth\ResetPasswordController@showResetForm');
  Route::get('/activation/{token}', 'AdmintalentAuth\RegisterController@userActivation');
});
