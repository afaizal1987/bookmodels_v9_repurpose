<?php

// Route::get('/home', function () {
//     $users[] = Auth::user();
//     $users[] = Auth::guard()->user();
//     $users[] = Auth::guard('agency')->user();
//
//
//     return view('agency.home');
// })->name('home');
Route::get('/home','agencyprofilecontroller@profile');
// Route::get('/profile','agencyprofilecontroller@profile');
Route::get('/update','agencyprofilecontroller@update');
Route::patch('/updateprofile','agencyprofilecontroller@updateProfile');
Route::get('/job_listing','JobsController@index');
Route::get('/job_listing2','JobsController@index2');
Route::patch('/img_upload','FileUploadController@agencyimage');
Route::post('/{id}/aphotos','FileUploadController@agencygallery');
Route::post('/adestroyImage','PhotoGallery@adestroy');
Route::post('/jobconfirm','JobsController@confirmJob');
Route::get('/verification','FileUploadController@verification');
Route::post('/verify',function(){
  $file = request()->file('file');
  $ext = $file->extension();
  $id =  Auth::guard('agency')->id();

  $file->storeAs('verificationfile/'.$id,'file.'.$ext);
  return redirect('agency/home')->with('status','Thank you for submitting your document for verification. Your request will be processed soon.');
});
Route::get('/image_upload','FileUploadController@agencyimage2');
Route::patch('/image_uploader','FileUploadController@agencyupload');

Route::get('/myphotos', 'PhotoGallery@agallery');
Route::post('/{id}/amassphotos','FileUploadController@addagencymassphotos');
Route::post('/disable','JobsController@disable');
Route::get('applications/{job_id}', function($job_id = null) {
	$agency_id = auth()->guard('agency')->user()->id;
	
	$job_applications_applied = \App\JobApplication::with('profile', 'job')->whereHas('job', function ($query) use ($agency_id, $job_id) {
	  $query->where('agency_id', $agency_id);

	  if (!is_null($job_id)) {
		  $query->where('job_id', $job_id);
	  }
	})->whereIn('status',array('1'))->get()->sortBy('created_at');
	
	$job_applications_booked = \App\JobApplication::with('profile', 'job')->whereHas('job', function ($query) use ($agency_id, $job_id) {
	  $query->where('agency_id', $agency_id);

	  if (!is_null($job_id)) {
		  $query->where('job_id', $job_id);
	  }
	})->whereIn('status',array('2'))->get()->sortBy('created_at');
	
	$number = $job_applications_applied->count();
	$number2 = $job_applications_booked->count();
	
	$data = compact(
	  'job_applications_booked',
	  'job_applications_applied',
	  'number',
	  'number2'
	);
	return view('agency.applications.index', $data);
});
  
	Route::get('/history','JobsController@history');
	
	// Specifically for Casting stuff. Will have to consult with Hussaini on making this into a non-spaghetti code. I need to clean this shit up
	Route::model('jobs', 'Job');
	Route::get('/casting/create','JobsController@casting_create');
	Route::post('/casting/store','JobsController@casting_store');
	Route::get('/{id}/castingedit','JobsController@casting_edit');
	Route::patch('/casting/update','JobsController@casting_update');
	
	Route::get('castings/{job_id}', function($job_id = null) {
		$agency_id = auth()->guard('agency')->user()->id;
		
		$job_applications_applied = \App\JobApplication::with('profile', 'job')->whereHas('job', function ($query) use ($agency_id, $job_id) {
		  $query->where('agency_id', $agency_id);

		  if (!is_null($job_id)) {
			  $query->where('job_id', $job_id);
		  }
		})->whereIn('status',array('1'))->get()->sortBy('created_at');
		
		$job_applications_shortlisted = \App\JobApplication::with('profile', 'job')->whereHas('job', function ($query) use ($agency_id, $job_id) {
		  $query->where('agency_id', $agency_id);

		  if (!is_null($job_id)) {
			  $query->where('job_id', $job_id);
		  }
		})->whereIn('status',array('6'))->get()->sortBy('created_at');
		
		$job_applications_booked = \App\JobApplication::with('profile', 'job')->whereHas('job', function ($query) use ($agency_id, $job_id) {
		  $query->where('agency_id', $agency_id);

		  if (!is_null($job_id)) {
			  $query->where('job_id', $job_id);
		  }
		})->whereIn('status',array('2'))->get()->sortBy('created_at');
		
		$number = $job_applications_applied->count();
		$number2 = $job_applications_booked->count();
		
		$data = compact(
		  'job_applications_booked',
		  'job_applications_shortlisted',
		  'job_applications_applied',
		  'number',
		  'number2'
		);
		return view('agency.applicationcastings.index', $data);
	});
	Route::post('/castingconfirm','JobsController@confirmCasting');
// Route::get('/{agency_name}','agencyprofilecontroller@agencypublic');
