@extends('layouts.app')
@section('title')
    <title>Terms of Service - Model Booking Made Easy</title>

@stop

@section('meta')
  <meta property="og:url" content="https://bookmodels.asia/terms"/>
  <meta property="og:title" content="Terms of Service - Model Booking Made Easy"/>
  <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
  <meta name='description' content='The number one model booking platform to connect Malaysian Models and Talents directly to verified Agencies and Photographers'>
  <meta charset="utf-8">
  <meta property="og:image" content="https://bookmodels.asia/images/banner_models.jpg"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
    <div id="pagetext">

    <h1>Terms of Service - BookModels.asia</h1>
    <p>By signing up on BookModels.asia, Models and Agencies accept the following Terms of Service (hereinafter the &ldquo;Terms&rdquo;) for using BookModels.asia. The Models and Agencies enter into this agreement on the use of BookModels.asia services.</p>
    <h4>A. DEFINITIONS</h4>
        <ul>
          <li><strong>&ldquo;Agency&rdquo;:</strong> Agencies, photographers, fashion designers or anyone who want to hire models and talents for various advertising or promotional jobs.;</li>
          <li><strong>&ldquo;Model&rdquo;:</strong> Models using BookModels.asia to apply for Modelling Jobs;</li>
          <li><strong>&ldquo;Users&rdquo;:</strong> Agencies and Models using BookModels.asia;</li>
          <li><strong>&ldquo;Terms&rdquo;:</strong> General Terms of Services for the use of BookModels.asia accepted by Agencies and Models, including Sections A to C.</li>
          <li><strong>&ldquo;Booking System&rdquo;:</strong> Feature on BookModels.asia through which Agencies are able to book a Model of legal age (18 years old or over) for a certain job;</li>
          <li><strong>&ldquo;Website&rdquo;:</strong> BookModels.asia;</li>
          <li><strong>&ldquo;Subscription Fee&rdquo;:</strong> The total amount that Agencies must pay for the preferred subscription plan;</li>
          <li><strong>&ldquo;Job Date&rdquo;:</strong> Date of the job requested by the Agencies to the Model through the Booking System and that has been agreed between both of them;</li>
          <li><strong>&ldquo;Agreed Payment&rdquo;:</strong> Fee agreed between the Agencies and the Model for the job requested by the Agencies to the Model through the Booking System.</li>
        </ul>
        <h4>B. USE OF BOOKMODELS.ASIA</h4>


        <p><strong>1.0 Subject Matter</strong></p>
        <ol>
          <li>BookModels.asia offers Models a platform for presenting their models profiles and compcards, particularly to agents/agencies, photographers, fashion designers or others looking for models and a feature to apply for Model Jobs.</li>
          <li>Features of the Website are offered for free for the Models, while the use of the full range of services for the Agencies requires subscription to our “Plans”. Details about the applicable subscription fees for the use of the Website for Agencies are set forth under Plans &amp; Pricing. The fees listed thereunder are binding. The applicable subscription fee for the service chosen by the Agencies shall be due immediately upon the end of the trial period. Payments can be made using the available payment methods. The Agencies may start using the Services if and when BookModels.asia has received the applicable subscription fee.</li>
          <li>The Models and Agencies acknowledge and agree that it is technically impossible for the Website to achieve 100% availability. BookModels.asia shall nonetheless endeavor to keep the Website available without interruption. Events related to maintenance, security or capacity requirements, and/or events beyond the Website’s control (e.g. disruptions in public communication networks, power failures etc.), may result in brief malfunctions or temporary interruptions of the services.</li>
          <li>BookModels.asia provides Models with a platform on which to interact with other persons or entities, in particular model agencies. BookModels.asia does not take part in any communication between Models and/or any other users of its services. If users enter into agreements with one another via the Website, BookModels.asia shall not be a contracting party to these agreements. The users alone are responsible for the execution and/or fulfillment of agreements in which they enter with one another. BookModels.asia shall not be held liable if users are unable to contact one another over BookModels.asia regarding such agreements. Furthermore, BookModels.asia shall not be liable for breaches of agreements entered into between users.</li>
        </ol>
      <p><strong>2.0 Creating your Profile on BookModels.asia</strong></p>
        <ol>
          <li>Models must register before using the services of BookModels.asia via Facebook Authentication. Upon authorizing the login via Facebook, the Model must complete the registration form with all the required information.</li>
          <li>The Model shall provide only accurate and complete data for registration. The data include a unique nickname to be chosen by the Model for public display with the Model’s profile. The Model shall report any changes in the registration data to BookModels.asia without undue delay.</li>
          <li>The Model warrants and represents that he or she is of legal age (18 and above) at the time of registration, failing which the membership will be cancelled.</li>
          <li>By completing the registration process, the Model accepts these Terms on the use of the services of the Website. BookModels.asia accepts this offer by activating the services.</li>
          <li>Agencies must register before using the services of BookModels.asia. Agencies shall provide only accurate and complete data for registration. The data may include a copy of the Agency’s business registration document in order for the Agency to be classified as a Verified user.The Agency shall report any changes in the registration data to BookModels.asia without undue delay.</li>
          <li>The Agency shall choose a password upon registration, which is needed in order to access the account on the Website. The Agency is obliged to keep this password a secret. BookModels.asia shall not disclose the password to any third party and the Website’s employees shall not ask for the Agency’s password at any time. The Agency shall notify BookModels.asia immediately of any unauthorized use of his/her password or account.</li>
          <li>By completing the registration process, the Agency accepts these Terms on the use of the services of BookModels.asia. BookModels.asia accepts this offer by activating the services on BookModels.asia.</li>
        </ol>
        <p><strong>3.0 Obligations of the Models &amp; Agencies</strong></p>
        <ol>
          <li>The Users are obliged to provide only true and non-misleading statements in his/her profile and in communications with other users.</li>
          <li>The Users shall post on BookModels.asia only photographs, videos, content and other materials (the “Content”) that are in compliance with the guidelines for photographs published by BookModels.asia under “guidelines”. The Model warrants and represents that the public display of such Content is not prohibited.</li>
          <li>The Content posted by the User shall comply with all applicable legislation, and respect all third-party rights. In particular, the User shall not: (i) post any pornographic, insulting or defamatory Contents, or any Contents that violate any applicable legislation for the protection of minors; (ii) advertise or promote, offer or distribute any products or services which do not comply with any applicable legislation for the protection of minors; (iii) use without authorization any Contents protected by law (e.g. by copyright, trademark, patent, or design patent laws).</li>
          <li>The User shall not distribute or publicly disclose the contents of BookModels.asia, or perform any actions which may impair the operability of BookModels.asia’s infrastructure, particularly actions which may overload said infrastructure.</li>
          <li>The User agrees that BookModels.asia may use his/her name and a customer reference, e.g. in publications about success stories, and may publicly disclose and advertise the fact that the Model is using the Website.</li>
        </ol>
        <p><strong>4.0 Changes to the Services on the Website</strong></p>
        <ol>
          <li>BookModels.asia reserves the right to modify the services offered on BookModels.asia and/or to offer services different from those offered at the time of the registration of the Agencies at any time, unless this is unreasonable for the Agencies.</li>
        </ol>
        <p><strong>5.0 Termination of Subscription</strong></p>
        <ol>
          <li>The Agency may terminate his/her subscription to BookModels.asia at any time without cause via the Subsription section or by pressing the “Delete Account” button, if available, at any time. The subscription to BookModels.asia shall terminate automatically in case the Agency does not pay the subscription fee for the renewal of its subscription as and when due as set forth under “fees”.</li>
          <li>During any term for which an Agency has paid the applicable subscription fee, BookModels.asia may only terminate such subscription for good cause. A “good cause” is defined as an event which makes it unacceptable for BookModels.asia to continue the subscription to the end of the subscription period, taking into account all circumstances of the individual case and weighing the interests of BookModels.asia against those of the Agency. A good cause includes, among others, any the following events
            <ul>
              <li> If the Agency fails to comply with any applicable legal provisions; If the Agency breaches a material contractual obligation set forth in these Terms; If the Agency causes harm to any other user(s) of BookModels.asia;</li>
              <li>If BookModels.asia considers that the Agency is involved in any immoral activity; *If the Agency makes any changes or removes information in his/her profile which could affect his/her status. </li>
            </ul>
          </li>
          <li>In the event of a good cause in accordance with section 5.2, BookModels.asia is entitled to:
            <ul>
              <li>Delete the contents posted by the Agency.</li>
              <li>Issue a warning, or</li>
              <li>Block the Agency’s access to the services on BookModels.asia.</li>
            </ul>
          </li>
          <li> In the following cases, the Agency shall not be entitled to claim reimbursement of any subscription fees already paid:
            <ul>
              <li>If BookModels.asia has terminated the contract for good cause pursuant to section 5.2,</li>
              <li>If BookModels.asia has blocked the Agency’s access in accordance with section 5.3,</li>
              <li>or If the Agency has terminated the agreement. However, the Agency’s right to claim reimbursement of any subscription fees shall not be excluded in this case if the Agency has terminated the agreement for a good cause attributable to BookModels.asia.</li>
            </ul>
          </li>
        </ol>
        <p><strong>6.0 Responsibility for the Model’s Content, Data or other Information</strong></p>
        <ol>
          <li>BookModels.asia does not make any warranties or representations regarding any data and/or information provided or made available by any user on BookModels.asia. In particular, BookModels.asia does not warrant or represent that said data and/or information is true or accurate, or that it fulfils or serves any particular purpose.</li>
          <li>The Model shall report any activities of any other user which violate applicable laws, and/or any of these Terms using the contact form available on BookModels.asia.</li>
        </ol>
        <p><strong>7.0 Customer Service/Support</strong></p>
        <ol>
          <li>Queries regarding the agreement with BookModels.asia or regarding BookModels.asia’s services may be sent to BookModels.asia using the contact form available on BookModels.asia or by sending an e-mail to sasha@bookmodel.asia.</li>
        </ol>
        <p><strong>8.0 Liability of BookModels.asia</strong></p>
        <ol>
          <li>BookModels.asia (including its agents) shall not be liable for damages suffered by the Users as a consequence of their subscription to BookModels.asia.</li>
        </ol>
        <p><strong>9.0 Indemnity</strong></p>
        <ol>
          <li>The User shall indemnify and hold BookModels.asia harmless from and against all actions, including damage claims, asserted by other users or third parties against BookModels.asia resulting from an infringement of their rights by the Contents posted by the User on the Website. The User assumes all reasonable costs BookModels.asia incurs due to an infringement of third party rights, including all reasonable legal-defense costs. All other rights of BookModels.asia, including the right to claim damages, shall remain unaffected.</li>
          <li>In the event the Contents posted by the User infringes any rights of any third party, the User shall, at his/her own expense and at BookModels.asia’s discretion, either obtain the right to use said Contents or render said Contents free of any infringement.</li>
        </ol>
        <p><strong>10.0 Data Protection</strong></p>
        <ol>
          <li>BookModels.asia recognizes that any personal data provided by the User to BookModels.asia is extremely important to the User, and BookModels.asia shall therefore be particularly sensitive in handling such data. BookModels.asia shall comply with all applicable legal provisions regarding data protection. In particular, BookModels.asia shall not provide or otherwise disclose any personal data of the User to any third party without authorization.</li>
        </ol>
        <p><strong>11. Feedback, Reputation and Reviews</strong></p>
        <ol>
          <li>Users accept and acknowledge transferring copyright of all the feedback, reputation and reviews that they may post on the Website. This feedback belongs solely to BookModels.asia. Models acknowledge that they cannot use this feedback without prior written permission of BookModels.asia.</li>
        </ol>
      <p><strong>12. Taxes</strong></p>
        <ol>
          <li>Users are responsible for paying their own taxes.</li>
        </ol>
        <p><strong>13. Language</strong></p>
        <ol>
          <li>Models accepting the Terms for using BookModels.asia declare that they fully and perfectly understand the content of the English version of these Terms.</li>
        </ol>
      <p><strong>14. Miscellaneous</strong></p>
        <ol>
          <li>Any amendments to these Terms must be made in writing in order to be valid.</li>
          <li>BookModels.asia reserves the right to amend these Terms at any time, without giving reasons, unless an amendment is unreasonable to the User.BookModels.asia shall give due notice of any amendments of these Terms to the User. If the User does not object to the applicability of the revised Terms within two (2) weeks after receipt of said notice, the amended Terms shall be deemed to be accepted by the User. BookModels.asia shall inform the User about the User's right to object and of the relevance of the objection deadline in said notice.</li>
          <li>Unless otherwise stated in these Terms, the User may submit all notices to BookModels.asia using the contact form provided on BookModels.asia, or by sending an e-mail to sasha@bookmodels.asia.</li>
          <li>If any provision of these Terms are, for any reason, invalid and/or unenforceable, the remaining provisions shall continue to be valid and enforceable to the fullest extent permitted by law. The parties agree to replace an invalid and/or unenforceable provision with a valid and/or enforceable provision which comes as close as possible to the intent and economic purpose of the invalid and/or unenforceable provision. This also applies to regulatory gaps in these Terms.</li>
        </ol>
        <h4>C. BOOKING SYSTEM OF BOOKMODELS.ASIA</h4>
        <p>The Booking System is a feature on BookModels.asia through which registered Agencies are able to book registered Models of legal age for a certain job. Models and Agencies accept the following General Terms and Conditions for using the Booking System of BookModels.asia:</p>
        <p><strong>1.0 General Terms and Conditions for the use of BookModels.asia</strong></p>
        <ol>
          <li>These General Terms and Conditions for the use of the Booking System (herein after “Booking Terms”) include all the above-mentioned terms and conditions (Section A and B) as long as they are not contrary to the Booking Terms (Section C).</li>
        </ol>
        <p><strong>2.0 Use of the Booking System</strong></p>
        <ol>
          <li>Agencies shall create a Job Listing with all the required information, for the Model to apply through the Booking System. Agencies must determine in their Job Listing the specific job required and the fees that they consider appropriate for this job.</li>
          <li>If the Model applies for a Job, agrees to the specific terms and conditions for the job, including the fee (herein after the “Agreed Payment”) and gets booked by the Agency, the Agency is obliged to pay the Model directly upon completion of the Job.</li>
        </ol>
      <p><strong>3.0 Method and Conditions of Payment</strong></p>
        <ol>
          <li>The currency of the Agreed Payment between Agency and Model is limited to MYR. The Subscription Fee is charged in the same currency.</li>
          <li>The payment of the Subscription Fee by the Agencies must be made via BillPlz through the mechanisms available on the Subscription section. No additional fees are charged to Agencies by BookModels.asia for the use of payment processors.</li>
        </ol>
        <p><strong>4.0 Job Cancellations </strong></p>
        <ol>
          <li>Models may cancel the booking 24 hours prior to the call time of the job via the Active Booking section. If the Model cancels the booking on the day of the Job, Agencies may rate the Model poorly.</li>
          <li>Agencies may also cancel the booking 24 hours prior to the call time of the job via the Job Application section. If the Agency cancels the booking on the day of the Job, Models may rate the Agency poorly.</li>
        </ol>
        <p><strong>5. Intervention of BookModels.asia in the Booking Process.</strong></p>
        <ol>
          <li>If an Agency effectively books a Model through the Booking System, the persons intervening in this process are the Agency, the Model and BookModels.asia.</li>
          <li>The intervention of BookModels.asia in the process is only for the following purposes: To provide, host and maintain a list of Models on the Website in order to enable the Agency to select Models for the Job that they may require;</li>
          <li>BookModels.asia is only an intermediary. The agreement is reached by the Model and the Agency without any recommendation of BookModels.asia. BookModels.asia does not take any decision on the suitability of a Model for a certain job. The only persons responsible of the decision and of the adequacy of the services of a Model for a Job is the Agency.</li>
          <li>If any worker of BookModels.asia makes any remark regarding the work of a Model or its suitability for a job in any means of communication, including BookModels.asia website, these will be considered as simple personal opinions, for which BookModels.asia will not be held responsible.</li>
          <li>BookModels.asia is not responsible for the Job carried out by the Model. Agencies and Models agree and acknowledge that BookModels.asia is not responsible of any issue arising between them regarding the completion of the specific job.</li>
          <li>Agencies and Models agree and acknowledge that the relationship between them is that of independent contractors.</li>
          <li>Agencies and Models agree and acknowledge that BookModels.asia is not responsible for:          </li>
          <ul>
            <li>any expenses, including travelling and food expenses, incurred during the Job;</li>
            <li>the correct completion of the Job;</li>
            <li>the accuracy of any information shared between the Model or the Agency;</li>
            <li>and the use by the information and documents exchanged between them.</li>
          </ul>
          <li>BookModels.asia does not offer any form of insurance since the Website only provides an online platform Agencies to offer and buy services of Models.</li>
        </ol>
      <p><strong>6.0 Complaints and Disputes</strong></p>
        <ol>
          <li>Agencies and Models may raise complaints that they may have regarding the completion of the Job to BookModels.asia for notification purposes only. BookModels.asia will not be responsible to settle any disputes.</li>
          <li>Agencies and Models may rate and review each other accordingly after the completion of each Job.</li>
          <li>When any dispute regarding the agreement reached between the Agency and Model through the Booking System arises:
            <ul>
              <li>The Agency and the Model commit themselves to try to solve the conflict by reaching an agreement between them, with no intervention of third parties, including BookModels.asia.</li>
              <li>If the Agency and the Model do not reach an agreement within 1 month, both parties will have to solve their problems judicially or extra-judicially, with no intervention of BookModels.asia. </li>
            </ul>
          </li>
          <li>Agencies and Models agree and acknowledge that BookModels.asia does not provide legal services or legal advises of any kind.</li>
        </ol>
      </div> <!-- End Pagetext -->
</div> <!-- End Content -->
@endsection
