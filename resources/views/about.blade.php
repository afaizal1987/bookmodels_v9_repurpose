@extends('layouts.app')
@section('title')
    <title>About BookModels.asia - Model Booking Made Easy</title>

@stop

@section('meta')
  <meta property="og:url" content="https://bookmodels.asia/about"/>
  <meta property="og:title" content="About BookModels.asia - Model Booking Made Easy"/>
  <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
  <meta name="description" content="Who and What we are">
  <meta charset="utf-8">
  <meta property="og:image" content="https://bookmodels.asia/images/banner_models.jpg"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">

	<div id="pagetext">

		<h1>About Us</h1>
		<p>BookModels.asia is a fresh new platform to effectively connect Asian models and aspiring talents directly to agencies, photographers, fashion designers or anyone who want to hire them for various advertising or promotional jobs. We aim to be straight to the point, and business-oriented to make it easy for business users to book models, unlike other model sites that tend to operate as social networking platforms.</p>
        <p>We believe there are more than enough social networking sites that allow models to network with potential clients, but there&rsquo;s a clear need for an easy-to-use, time-saving booking platform so that models can easily get hired for paid jobs. Besides that, there&rsquo;s also a desperate need for a safe environment for models to look for jobs, without falling prey to fake agents or unscrupulous clients.</p>
        <p>As for the agencies or photographers, BookModels.asia will cut down the hassle of managing their own model database, setting up a management system or scouting for fresh new faces on social media. The limitations of existing social media platforms when it comes to business communication and sharing of work portfolio is a common problem. Our easy-to-use features will help to streamline the model booking and payment process, and help bring more structure to the industry.</p>
        <p><em>Note: Currently, our platform is only available to Agency users in Malaysia and Singapore.</em></p>
        <p>&nbsp;</p>

	</div> <!-- End Pagetext -->
</div> <!-- End Content -->



@endsection
