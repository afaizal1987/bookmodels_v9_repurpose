<ul class="navigation">

    @if (Auth::guest())
	<li class="profile"><i class="fa fa-user"></i> DASHBOARD</li>
    <li><a href="/"><i class="fa fa-user"></i> Home</a></li>
    <li><a href="/login"><i class="fa fa-user"></i> Login</a></li>
    @else

    <li><i class="fa fa-user"></i>WELCOME <?php $test = (App\Test::name()); echo strtoupper($test);?></li>
    <li class="profile"><a href="/models/home"><i class="fa fa-user"></i> My Profile</a></li>

    <li class="profile"><a href="/models/myphotos"><i class="fa fa-file-text-o"></i> My Photos</a></li>
    <li class="profile"><a href="/models/job_model"><i class="fa fa-file-text-o"></i> Job Listing</a></li>
    <!-- For now -->
    <li class="profile"><a href="/models/bookings"><i class="fa fa-hourglass"></i> My Booking</a></li>

    <li class="profile">
        <a href="{{ url('/logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
            Logout
        </a>

        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
    @endif

    <!-- Normal links -->
    <!--<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>-->
    <li><a href="/about"><i class="fa fa-file-text-o"></i> About BookModels.asia</a></li>
    <li><a href="/agency"><i class="fa fa-file-text-o"></i> Plans & Pricing</a></li>
    <li><a href="/terms"><i class="fa fa-file-text-o"></i> Terms of Use</a></li>
    <!--<li><a href="/privacy"><i class="fa fa-file-text-o"></i> Privacy Policy</a></li>-->
    <!--<li><a href="/help"><i class="fa fa-file-text-o"></i> Help Centre</a></li>-->
    <li><a href="/contactus"><i class="fa fa-phone-square"></i> Contact Us</a></li>
</ul>

