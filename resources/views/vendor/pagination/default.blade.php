@if ($paginator->hasPages())
<!-- <div id="pagination">
        <a href="#">PREV</a>
        <a href="#">1</a>
        <a href="#">2</a>
        <a href="#">...</a>
        <a href="#">9</a>
        <a href="#">NEXT</a>
      </div> -->
      <div id="pagination">
              {{-- Previous Page Link --}}
              @if ($paginator->onFirstPage())
                  <a href="#"><span>PREV</span></a>
              @else
                  <a href="{{ $paginator->previousPageUrl() }}" rel="prev">PREV</a>
              @endif
              {{-- Pagination Elements --}}
              @foreach ($elements as $element)
                  {{-- "Three Dots" Separator --}}
                  @if (is_string($element))
                      <a><span>{{ $element }}</span></a>
                  @endif

                  {{-- Array Of Links --}}
                  @if (is_array($element))
                      @foreach ($element as $page => $url)
                          @if ($page == $paginator->currentPage())
                              <a class="active"><span>{{ $page }}</span></a>
                          @else
                              <a href="{{ $url }}">{{ $page }}</a>
                          @endif
                      @endforeach
                  @endif
              @endforeach

              {{-- Next Page Link --}}
              @if ($paginator->hasMorePages())
                  <a href="{{ $paginator->nextPageUrl() }}" rel="next">NEXT</a>
              @else
                  <a class="disabled"><span>NEXT</span></a>
              @endif
            </div>
    <!-- <ul id="pagination" class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><span>&laquo;</span></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="disabled"><span>&raquo;</span></li>
        @endif
    </ul> -->
@endif
