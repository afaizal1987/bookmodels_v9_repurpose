<!DOCTYPE html>
<html>
<head>
@yield('title')


@yield('meta')
<meta property="og:type" content="website"/>
<!-- <meta property="og:url" content="https://bookmodels.asia"/> -->

<meta property="og:site_name" content="Bookmodels.asia"/>
<meta property="og:description" content="BookModels.asia is a fresh new platform to effectively connect Malaysian models and aspiring talents directly to verified agencies or photographers."/>
<meta property="fb:app_id" content="1622986338008242"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/column_normaliser.css">
<link rel="stylesheet" type="text/css" href="/css/fluid-labels.css">
<link rel="stylesheet" type="text/css" href="/css/lightbox.min.css">
<link rel="stylesheet" type="text/css" href="/css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="/css/dropzone.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="/css/croppie.css" />

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="/js/croppie.min.js"></script>


@yield('js')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3190436-84', 'auto');
  ga('send', 'pageview');

</script>


<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!--
<div id="wrapper">
-->
	<?php //include('sidemenu.php'); ?>
	@include('sidebar')
	<div class="site-wrap"><!-- for slide menu -->

	<header class="clearfix">
		<div class="header-inner">
        <input type="checkbox" id="nav-trigger" class="nav-trigger" />
        <label for="nav-trigger" class="visible-md visible-lg visible-sm visible-xs"><img src="/images/ico_menu.png" width="77" height="20" alt="Menu"></label>

		<div id="logo"><a href="/index.php"><img src="/images/BookModels_Logo.png" class="logo"></a></div>
			@if (Auth::guest())
				<div id="headerlogin"><a href=" /login" class="button">LOGIN</a></div>
			@else
			<div id="header-profile">
				<a href="/<?php echo $test = (App\Test::name());?>"><img class="profileimg" src="/uploads/avatars/<?php echo $test = (App\Test::image());?>" alt="Profile Image"/></a>
				<div class="profile-box">
					<span class="fullname">WELCOME, <?php $test = (App\Test::name()); echo strtoupper($test); ?></span><br/>
					<!--<p>Last login: </p>-->
					<p><a href="{{ url('/logout') }}"
	            onclick="event.preventDefault();
	                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
	            LOGOUT
	        </a>

	        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	            {{ csrf_field() }}
	        </form></p>
				</div>
			</div>
			<!--<div style="margin: 10px 0 10px 0;"></div>-->
			@endif
        <!--<div id="search"><input type="text" id="searchField" placeholder="Search WebTVasia" name="search" value="" /></div>-->
        </div>
	</header>

	@yield('content')

  <footer>
		<a href="http://facebook.com/bookmodelsasia" target="_blank">
			<img src="/images/ico_fb.png" width="57" height="56" alt="Facebook" /></a>
		<a href="http://twitter.com/bookmodelsasia" target="_blank">
			<img src="/images/ico_tw.png" width="57" height="56" alt="Twitter" /></a>
		<a href="http://instagram.com/bookmodelsasia" target="_blank">
			<img src="/images/ico_ig.png" width="57" height="56" alt="Instagram" /></a>
		<a href="https://www.youtube.com/channel/UCNkRGbulbxwlIupLnZShVhg" target="_blank">
			<img src="/images/ico_yt.png" width="57" height="56" alt="YouTube" /></a>
	<br/><br/>

	&copy; Copyright 2017 BookModels.asia. All rights reserved.<br/>
	<a href="/terms">Terms of Use</a> |
	<!-- <a href="/privacy">Privacy Policy</a> |-->
	<a href="/contact">Contact</a><br/>
	</footer>


	<script src="/js/app.js" async></script>
	<script src="/js/sweetalert.min.js"></script>
	@include('sweet::alert')
	@yield('scripts')

	</div><!-- /.site_wrap -->

	</body>
	</html>
