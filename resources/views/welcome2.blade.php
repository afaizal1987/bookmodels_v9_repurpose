<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<title>BookModels.asia - model booking made easy</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name='description' content='The number one model booking platform to connect Malaysian Models and Talents directly to verified Agencies and Photographers'>
<meta name='keywords' content='model, talent, booking, agency, runway, fashion, photoshoot, casting'>

<meta property='fb:app_id' content='1503944309863696'>
<meta property='og:type' content='website'>
<meta property='og:url' content='http://bookmodels.asia'>
<meta property='og:site_name' content='BookModels.asia'>
<meta property='og:title' content='BookModels.asia - model booking made easy'>
<meta property='og:description' content='The number one model booking platform to connect Malaysian Models and Talents directly to verified Agencies and Photographers'>
<meta property='og:image' content='http://bookmodels.asia/images/BookModels_Logo_Square.png'>

<style type="text/css">

body {
font-family: Arial, Helvetica, sans-serif;
font-size: 16px;
line-height: 28px;
color: #666;
margin: 0px;
background: #fff;
}

#wrapper {
max-width: 900px;
width:100%;
padding: 20px;
margin: 10px auto 10px auto;
text-align: center;
}

img {
    width: 100%;
    max-width: 500px;
}

.button {
    background-color: #C1272D; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
	border-radius: 8px;
	-webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
}

.button:hover {
    background-color: #ED1C24; /* Green */
    color: white;
}

h1 {
font-family: Arial, Helvetica, sans-serif;
font-size: 40px;
font-weight: bold;
margin: 0px;
padding: 0px;
color: #262626;
}

a {
color: #000;
font-weight: bold;
text-decoration: none;
}

a:hover {
color: #C1272D;
font-weight: bold;
text-decoration: none;
}

p {
margin: 0 auto 10px auto;
}

.footer {
font-size: 12px;
padding: 10px;
}

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3190436-84', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>
<div id="wrapper">
<p>&nbsp;</p>
<p>&nbsp;</p>
<div align="center"><a href="http://bookmodels.asia"><img src="images2/BookModels_Logo.png"/></a></div>
<p>&nbsp;</p>
  <p>BookModels.asia is a fresh new platform to effectively connect Malaysian models and aspiring talents directly to verified agencies or photographers. Get ready to be part of the most easy-to-use model booking platform ever. If you think this is cool, join the mailing list below to get more updates.</p>
  <p>&nbsp;</p>
  <a href="http://eepurl.com/chQBVr" class="button">Sign-up Now!</a>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
<p class="footer">© 2017 BookModels.asia. All Rights Reserved.<div align="center"> Questions? Mail us!</div><a href="mailto:hello@bookmodels.asia?Subject=Hello" target="_top">hello@bookmodels.asia</a>.</p>
</div>

</body>
</html>
