@extends('layouts.app2')

@section('title')
    <title>Create your Model Profile - BookModels.asia</title>
	<meta property="og:title" content="Create your Model Profile - BookModels.asia"/>
@stop

@section('meta')
    <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
	<meta name="description" content="Create your model profile at BookModels.asia and apply for modelling jobs around you.">
	<meta property="og:description" content="Create your model profile at BookModels.asia and apply for modelling jobs around you."/>
@stop


@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
		<div id="loginbox">
      <!-- Error message -->
    @if (session('error'))
      <div class="alert alert-danger">
        {{ session('error') }}
      </div>
    @endif

		<h1>CREATE MODEL PROFILE</h1>
		<hr/>
    <form id='signupform' role="form" action="/models/edit_test" method="POST" name="signupform">
    {{ csrf_field() }}
		<input type='hidden' name='user_id' value='{{ Auth::user()->id }}'>
		<h3 style="text-align:center">PERSONAL INFORMATION</h3>

			<div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
				<div class="fluid-label">
					<input type='text' name='fullname' placeholder='Full Name' value="{{old('fullname')}}" class='form-control' required/>
				</div>
				@if ($errors->has('fullname'))
						<span class="help-block">
								<strong>{{ $errors->first('fullname') }}</strong>
						</span>
				@endif
			</div>

			<div class="form-group{{ $errors->has('nickname') ? ' has-error' : '' }}">
					<div class="fluid-label">
								<input id='nickname' type='text' name='nickname' placeholder='Unique Nickname' value="{{ old('nickname') }}" class='form-control' required/>
								<span id="usercheck" class="help-block"></span>
					</div>
							@if ($errors->has('nickname'))
									<span class="help-block">
											<strong>{{ $errors->first('nickname') }}</strong>
									</span>
							@endif
			</div>

			<div class="form-group{{ $errors->has('workemail') ? ' has-error' : '' }}">
				<!-- <label for='workemail'>Work Email</label> -->
				<div class="fluid-label">
					<input type='text' name='workemail' placeholder='Work Email' value="{{old('workemail')}}" class='form-control' required/>
				</div>
					@if ($errors->has('workemail')) <span class="help-block"><strong>{{ $errors->first('workemail') }}</strong></span> @endif
			</div>

			<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
				<!-- <label for='phone_number'>Phone Number</label> -->
				<div class="fluid-label">
					<input type='text' name='phone_number' placeholder='Phone Number' value="{{old('phone_number')}}" class='form-control' required/>
				</div>
					@if ($errors->has('phone_number')) <span class="help-block"><strong>{{ $errors->first('phone_number') }}</strong></span> @endif
			</div>

			<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
				<!-- <label for='location'>Current Location (City)</label> -->
				<div class="fluid-label">
					<input type='text' name='location' placeholder='Current Location (City)' value="{{old('location')}}" class='form-control' required/>
				</div>
					@if ($errors->has('location')) <span class="help-block"><strong>{{ $errors->first('location') }}</strong></span> @endif
					<!-- <span class='text-danger'></span> -->
			</div>

			<div class="form-group{{ $errors->has('gender_select') ? ' has-error' : '' }}">
				<label for="gender">Your Gender:</label> <br/>
				<select class="select form-control2" name="gender_select" required>
					<option value=""> -- Select One -- </option>
					<option id="female" value="Female">Female</option>
					<option id="male" value="Male">Male</option>
				</select>
				@if ($errors->has('gender_select')) <span class="help-block"><strong>{{ $errors->first('gender_select') }}</strong></span> @endif
			</div>

			<div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
					<!-- <label for='birthday'>Birthday</label> -->
					<div class="fluid-label">
						<input type='text' id="birthday" name='birthday' placeholder='Birthday (DD-MM-YYY)'class='form-control' required/>
							<span class="add-on"><i class="icon-th"></i></span>
					</div>
						@if ($errors->has('birthday')) <span class="help-block"><strong>{{ $errors->first('birthday') }}</strong></span> @endif
						<!-- <span class='text-danger'></span> -->
			</div>

			<div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
				<!-- <label for='nationality'>Nationality</label> -->
				<div class="fluid-label">
					<input type='text' name='nationality' placeholder='Nationality' value="{{old('nationality')}}" class='form-control' required/>
				</div>
					@if ($errors->has('nationality')) <span class="help-block"><strong>{{ $errors->first('nationality') }}</strong></span> @endif
					<!-- <span class='text-danger'></span> -->
			</div>

			<div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">
				<!-- <label for='agency'>Agency (If none or new, type freelance,)</label> -->
				<div class="fluid-label">
					<input type='text' name='agency' placeholder='Agency, if none, type Freelance' value="{{old('agency')}}" class='form-control' required/>
				</div>
					@if ($errors->has('agency')) <span class="help-block"><strong>{{ $errors->first('agency') }}</strong></span> @endif
					<!-- <span class='text-danger'></span> -->
			</div>

			<div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">
				<label for="marital">Marital Status:</label>
				<select class="select form-control2" name="marital_status" required>
					<option value=""> -- Select your Marital Status -- </option>
					<option id="Single" value="Single">Single</option>
					<option id="Married" value="Married">Married</option>
				</select>
				@if ($errors->has('marital_status')) <span class="help-block"><strong>{{ $errors->first('marital_status') }}</strong></span> @endif
			</div>

			<div class="form-group{{ $errors->has('language') ? ' has-error' : '' }}">
				<!-- <label for='language'>Language</label> -->
				<div class="fluid-label">
					<input type='text' name='language' placeholder='Language' value="{{old('language')}}" class='form-control' required/>
				</div>
					@if ($errors->has('language')) <span class="help-block"><strong>{{ $errors->first('language') }}</strong></span> @endif
					<!-- <span class='text-danger'></span> -->
			</div>
		<h3 style="text-align:center">PHYSICAL ATTRIBUTES</h3>
			<div class="form-group{{ $errors->has('ethnicity') ? ' has-error' : '' }}">
	      <label for="gender">Ethnicity (Race):</label><br/>
		      <select class="select form-control2" name="ethnicity" required>
		        <option value=""> -- Select your Ethnicity (Race) -- </option>
		        <option id="malay" value="Malay">Malay</option>
		        <option id="indian" value="Indian">Indian</option>
		        <option id="chinese" value="Chinese">Chinese</option>
		        <option id="caucasian" value="Caucasian">Caucasian</option>
		        <option id="panasian" value="Panasian">Pan-Asian</option>
		        <option id="others" value="Others">Others</option>
		      </select>
					@if ($errors->has('ethnicity')) <span class="help-block"><strong>{{ $errors->first('ethnicity') }}</strong></span> @endif
	      <!-- <span class='text-danger'></span> -->
	  	</div>

		  <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
		    <div class="fluid-label">
		    <input type="number"   class='form-control' name="height" value="{{old('height')}}" placeholder="Height (cm)" required maxlength="2"/>
		    </div>
		      @if ($errors->has('height')) <span class="help-block"><strong>{{ $errors->first('height') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="weight" value="{{old('weight')}}" placeholder="Weight (kg)" required maxlength="2"/>
		    </div>
		      @if ($errors->has('weight')) <span class="help-block"><strong>{{ $errors->first('weight') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <div class="form-group{{ $errors->has('Waist') ? ' has-error' : '' }}">
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="Waist" value="{{old('Waist')}}" placeholder="Waist (in)" required maxlength="2"/>
		    </div>
		      @if ($errors->has('Waist')) <span class="help-block"><strong>{{ $errors->first('Waist') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <!-- For them ladies -->
		  <!-- <div class='form-group conditional' data-cond-option="gender_select" data-cond-value="Female"> -->
		  <div class='form-group conditional' data-cond-option="gender_select" data-cond-value="Female">
		    <!-- <label for='bust'>Bust (in)</label> -->
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="Bust" placeholder="Bust (in)" value="{{old('bust') ?? 0}}" maxlength="2"/>
		    </div>
		      @if ($errors->has('bust')) <span class="help-block"><strong>{{ $errors->first('bust') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <!-- /For them ladies -->

		  <!-- For them lads -->
		  <div class='form-group conditional' data-cond-option="gender_select" data-cond-value="Male">
		    <!-- <label for='Chest'>Chest (in)</label> -->
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="Chest" placeholder="Chest (in)" value="{{old('Chest') ?? 0}}" maxlength="2"/>
		    </div>
		      @if ($errors->has('Chest')) <span class="help-block"><strong>{{ $errors->first('Chest') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <!-- /For them lads -->

		  <div class="form-group{{ $errors->has('Hips') ? ' has-error' : '' }}">
		    <!-- <label for='Hips'> Hips (in) </label> -->
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="Hips" placeholder="Hips (in)" value="{{old('Hips') ?? 0}}" maxlength="2" required/>
		    </div>
		      @if ($errors->has('Hips')) <span class="help-block"><strong>{{ $errors->first('Hips') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <div class="form-group{{ $errors->has('Leg_length') ? ' has-error' : '' }}">
		    <!-- <label for='Legs'>Leg length (in)</label> -->
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="Leg_length" placeholder="Leg Length (in)" value="{{old('Leg_length') ?? 0}}" maxlength="2"/>
		    </div>
		      @if ($errors->has('Leg_length')) <span class="help-block"><strong>{{ $errors->first('Leg_length') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <div class="form-group{{ $errors->has('shoes') ? ' has-error' : '' }}">
		    <!-- <label for='shoe'>Shoe (UK)</label> -->
		    <div class="fluid-label">
		      <input type="number"   class='form-control' name="shoes" placeholder="Shoe Size (UK Size)" value="{{old('shoes')}}" required/>
		    </div>
		      @if ($errors->has('shoe')) <span class="help-block"><strong>{{ $errors->first('shoe') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <div class="form-group{{ $errors->has('piercing') ? ' has-error' : '' }}">
		    <label for='piercing'>If you have piercings, please specify.</label>
		    <div class="fluid-label">
		      <input type="text" class='form-control' name="piercing" placeholder="If you have piercings, please specify" value="{{old('piercing')}}"/>
		    </div>
		      @if ($errors->has('piercing')) <span class="help-block"><strong>{{ $errors->first('piercing') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>

		  <div class="form-group{{ $errors->has('tattoos') ? ' has-error' : '' }}">
		    <label for='tattoos'>If you have tattoos, please specify.</label>
		    <div class="fluid-label">
		      <input type="text" class='form-control' name="tattoos" placeholder="If you have tattoos, please specify" value="{{old('tattoos')}}"/>
		    </div>
		      @if ($errors->has('tattoos')) <span class="help-block"><strong>{{ $errors->first('tattoos') }}</strong></span> @endif
		      <!-- <span class='text-danger'></span> -->
		  </div>
		<h3 style="text-align:center">INTEREST & AVAILABILITY</h3>
			<br/>
			<div class="form-group{{ $errors->has('qualities') ? ' has-error' : '' }}">
	      <label for='qualities'>If you have any relevant talents/skills, please specify.</label>
	      <div class="fluid-label">
	        <input type="text" class='form-control' name="qualities" placeholder="If you have any talents/skills, please specify" value="{{old('qualities')}}"/>
	      </div>
	        @if ($errors->has('qualities')) <span class="help-block"><strong>{{ $errors->first('qualities') }}</strong></span> @endif
	        <!-- <span class='text-danger'></span> -->
	    </div>

	    <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
	      <label for='experience'>Experience</label>
	      <select id="experience" name="experience" class='form-control2' required>
	        <option value="1" >New</option>
	        <option value="2" >1-2 years</option>
	        <option value="3" >3-5 years</option>
	        <option value="4" >5-10 years</option>
	        <option value="5" >10 years and above</option>
	      </select>
	    </div>
	    @if ($errors->has('experience')) <span class="help-block"><strong>{{ $errors->first('experience') }}</strong></span> @endif

	    <div class="form-group{{ $errors->has('interest') ? ' has-error' : '' }}">
	      <label for='interest'>Interest or Availability</label><br/>
	        <input type="checkbox" id="tvc" name="interest[]" value="1"/> Acting / Video Productions<br>
	        <input type="checkbox" id="runway" name="interest[]" value="2"/> Runway / Fashion<br>
	        <input type="checkbox" id="print" name="interest[]" value="3"/> Print or Web Advertising<br>
	        <input type="checkbox" id="sports" name="interest[]" value="4"/> Outdoor / Sports Modelling<br>
	        <input type="checkbox" id="swimsuit" name="interest[]" value="5"/> Swimsuit / Bikini Modelling<br>
	        <input type="checkbox" id="lingerie" name="interest[]" value="6"/> Lingerie Modelling<br>
	        <input type="checkbox" id="nude" name="interest[]" value="7"/> Artistic Nude Modelling<br>
	        <input type="checkbox" id="promoter" name="interest[]" value="8"/> Usher / Promoter<br>
	    </div>

		<div class="form-group">
			<label>
				<input id='cb1' type="checkbox" name="terms" value="1" /><span class="terms">I agree to the <a href="/terms">Terms of Use</a><!-- and Privacy policy-->.</span>
			</label>
		</div>

		<div class="form-group">
				<input id="submission" type="submit" class="button pink" value='Next' onclick="if(!this.form.cb1.checked){swal('', 'Please accept the Terms of Use', 'warning');return false}"/>
				<span id="usercheck" class="help-block"></span>
		</div>
    </form>
  </div><!--/ Loginbox-->
</div><!--/ Content-->

@endsection

@section('scripts')
<script type='text/javascript' src="{{ asset('/js/conditionize.js') }}" async></script>

<script src="{{ asset('/js/fluid-labels.js') }}"></script>
<script type='text/javascript' async>
  $('.fluid-label').fluidLabel();
</script>
<script type='text/javascript'>
  $("#birthday").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: 'true'
  });
</script>

<script type='text/javascript'>
$(document).ready(function(){
        $("#nickname").change(function(){
             $("#message").html("<i class='fa fa-spinner' aria-hidden='true'></i>");
				var name=$("#nickname").val();
				// Setting the CSRF
				$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
				//Sending and receive
        $.ajax({
              type:"post",
               dataType: "json",
               url :"{{URL::to('models/yourname') }}",
               data: {"name": name},
               success: function(receivedData) {
                 if(receivedData['status']==1){
                    $('#usercheck').parent('div').removeClass('has-error').addClass('has-success');
								}else{
                   $('#usercheck').parent('div').removeClass('has-success').addClass('has-error');
                 }
                 $('#usercheck').html(receivedData.message);
              }
           });
				//
        });
	});
</script>
<script>
	$('#signupform').submit(function() {
		swal({title:"", text:"CREATING PROFILE", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
	});
</script>
@endsection
