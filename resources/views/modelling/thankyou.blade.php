@extends('layouts.app')

@section('title')
    <title>Thank You</title>
	<meta property="og:title" content="Thank You"/>
@stop


@section('content')

<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
		<div id="loginbox">

			<h1>THANK YOU FOR SIGNING-UP! YOU’RE JUST A STEP AWAY FROM LANDING YOUR 1ST JOB</h1>

                Thank you for creating your model profile with BookModels.asia. Go to your <a href="{{url('/models/home')}}">Profile</a> to update your personal details, or go to your <a href="{{url('/models/myphotos')}}">Photos</a> to upload photos
of your recent work before applying for jobs at the <a href="{{url('/models/job_model')}}">Job Listing</a> section.
		</div> <!-- loginbox -->
</div> <!-- End Content -->
@endsection
