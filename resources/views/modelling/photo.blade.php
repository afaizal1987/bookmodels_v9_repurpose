@extends('layouts.app')

@section('title')
    <title>My Photos - <?php $test = (App\Test::name()); echo ($test); ?></title>
	<meta property="og:title" content="My Photos - <?php $test = (App\Test::name()); echo ($test); ?>"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
	<div id="profile">

		<h1>My Photos</h1>
		@if ($message = Session::get('success'))
		<div class="alert alert-success alert-block">
		  <button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{{ $message }}</strong>
		</div>
		@endif
		<h2 id="top"> Add Multiple Photos</h2>

		<div style="width:93%">
		  <form id='addPhotos' action='./{{$user_id}}/massphotos' method='post' class="dropzone">
			{{ csrf_field() }}
		  </form>
		</div>

		<hr style="width:93%; margin-left:1px;"/>

		@foreach ($array as $key=>$value)
		<div class="photo-box">
		  <a href='/uploads/thumbnails/{{$value}}' data-lightbox="model_photos">
			 <img class="photo" src="/uploads/model_photos/{{$value}}" alt="Image" /></a>
		  @if($value =='photo_add.png')
		  <form id='photoupload' action="./{{$user_id}}/photos" enctype="multipart/form-data" id='login_form' method="POST">
			<input type="hidden" name="MAX_FILE_SIZE" value="3145728" />
			{{ csrf_field() }}
			  <input id="buttonID" class="foo3" accept="image/*" type="file" style='width:100%' name='image' onchange="this.form.submit()"/><br/>
		  </form>
		  <!-- <div id="overlay" class="photo-box">
		  <p>The image is uploading, be patient...</p><br/><br/>
		  <img src="/images/loading_white.gif"  alt="loading">
		  </div> -->
		  @else
		  <input id='delete' class="buttonlink2 pink" type="submit" data-id="{{$value}}" value="DELETE PHOTO" /><br/>
		  @endif
			  </div>
		@endforeach
		<br/>

		<hr/>
	</div> <!-- End Profile -->
</div> <!-- End Content -->



@endsection
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>

<script async>
  Dropzone.options.addPhotos={

    maxFiles: '<?php echo $count;?>',
    dictDefaultMessage: "ADD YOUR PHOTOS HERE!",
    paramName: "images", // The name that will be used to transfer the file
    maxFilesize: 5, // MB
    acceptedFiles:'.jpg, .jpeg, .png',
    init: function() {
    		this.on('success', function(){
    			if (this.getQueuedFiles().length === 0 && this.getUploadingFiles().length === 0) {
            swal({
              title: "Photo successfully uploaded",
              type: "success",
               timer: 1700,
               showConfirmButton: false
              // closeOnConfirm: false
            },function(){
              location.reload();
            });
    			}
    	    });
    }
  };
</script>
<script>
    lightbox.option({
      'alwaysShowNavOnTouchDevices':true,
      'resizeDuration': 200,
      'fitImagesInViewport': true,
      'wrapAround': true
    })
</script>
<script type='text/javascript' async>
$('input#delete').on('click',function(){

  var id = $(this).data('id');
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  swal({
    title: "Delete this photo?",
    // text: "Are you sure?",
    // type: "warning",
     imageUrl: "/uploads/model_photos/"+id,
     imageSize: '150x85',
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
		confirmButtonText: "DELETE",
    cancelButtonText: "CANCEL",
    closeOnConfirm: false
    },function(){

      $.ajax({
        type: "post",
        url: "{{url('models/destroyImage')}}",
        data: {img_id:id},
        success: function (response) {
          if(response['delete'] == 1){
              // location.reload ();
              swal({
                title: "Photo Deleted",
                type: "success",
                 timer: 1700,
                 showConfirmButton: false
              },function(){
                location.reload();
              });
          }
      }
    });
  });
});
</script>
<script>
	$('#photoupload').submit(function() {
		swal({title:"", text:"UPLOADING PHOTO", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
	});
</script>
@endsection
