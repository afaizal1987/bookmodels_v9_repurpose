@extends('layouts.app')

@section('title')
	@if (Auth::guest())
		<title>{{$profiles->fullname}} - BookModels.asia</title>
		<meta property="og:title" content="{{$profiles->fullname}} - BookModels.asia"/>
	@else
    <title>My Profile - <?php $test = (App\Test::name()); echo ($test); ?></title>
	<meta property="og:title" content="My Profile - <?php $test = (App\Test::name()); echo ($test); ?>"/>
	@endif
@stop

@section('meta')
  <meta property="og:image" content="https://bookmodels.asia/uploads/avatars/{{$profiles->avatar}}"/>
	@if (Auth::guest())
  <meta name="keywords" content="Model, talent, booking, agency, photographer, fashion designer">
  <meta name="description" content="BookModels.asia is a fresh new platform to effectively connect Malaysian models and aspiring talents directly to verified agencies or photographers.">
	@endif
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta property="og:url" content="https://bookmodels.asia/{{$profiles->nickname}}"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id='content'>
  <div id="profile">


        @if ($message = Session::get('status'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif

      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
				@if ($errors->has('image'))
						<span style="color:red" class="help-block">
								<strong>{{ $errors->first('image') }}</strong>
						</span>
				@endif
			</div>

      <div class="profileimg-box{{ $errors->has('title') ? ' has-error' : '' }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
              <img class="profileimg" src='/uploads/avatars/{{$profiles->avatar}}' alt="Profile Image"/>

            @if ($user && $user->owns($profiles))
              <input class="buttonlink2 pink" type="submit" name="image" style='width:100%' value="UPDATE PROFILE IMAGE" onClick="window.location = './models/image_upload';"/>
            @endif

      </div>

      <div class="profile-box">
                <h2>{{$profiles->fullname}} (<?php $complink = strtr($profiles->nickname, array('.' => ' '));
					echo ucwords($complink);?>)</h2>
                  {{$age}} years, {{$profiles->location}} <!--<span class="active">(active)</span>--><br/>
                  <!-- Location: {{ ($profiles->location) ? $profiles->location : "Not stated" }}<br/> -->
                  Agency: {{($profiles->agency)}}<br/><br/>

                  <br/>
                <!-- <strong>Model Rating - 80% (24 reviews)</strong><br/> -->
                @if ($user && $user->owns($profiles))
                <input class="buttonlink pink" type="submit" value="EDIT PROFILE" onClick="window.location = './models/update';"/>
                <input class="buttonlink pink" type="submit" value="MY PHOTOS" onClick="window.location = './models/myphotos';"/>
                @endif

  			</div> <!-- End Profile Box -->

        <div class="profilemeta-box">
  				<h3>Physical Information</h3>

  				<span class="meta">Gender</span>: {{$profiles->gender_select}}<br/>
  				<span class="meta">Ethnicity</span>: {{$profiles->ethnicity}}<br/>
  				<span class="meta">Height</span>: {{$profiles->height}} cm<br/>
          <?php if ($profiles->gender_select == 'Female'): ?>
            <span class="meta">Bust</span>: {{$profiles->bust}} in<br/>
            <span class="meta">Hips</span>: {{$profiles->hips}} in<br/>
            <span class="meta">Waist</span>: {{$profiles->waist}} in<br/>
          <?php else: ?>
          <span class="meta">Chest</span>: {{$profiles->chest}} in<br/>
		   <span class="meta">Hips</span>: {{$profiles->hips}} in<br/>
		   <span class="meta">Waist</span>: {{$profiles->waist}} in<br/>
          <!--<span class="meta">Leg Length</span>: {{$profiles->leg_length}} in<br/>-->
          <?php endif; ?>
  			</div> <!-- End Profile Box -->

        <div class="profilemeta-box">
  				<h3>Other Information</h3>

  				<span class="meta">Language</span>: {{$profiles->language}}<br/>
  				<span class="meta">Availability</span>: {{$interest}}<br/>
        </div> <!-- End Profile Box -->

        <br clear="all" />
      @if ($user && $user->owns($profiles))

        <h2>My Photos</h2>

        <!-- Images available -->
        @if (count($arrayTest) > 0)
        @foreach ($arrayTest as $key=>$value)
        <div class="photo-box">
          <a href='/uploads/thumbnails/{{$value}}' data-lightbox="model_photos">
            <img class="photo" src="/uploads/model_photos/{{$value}}" alt="Images" /></a>
        </div>
        @endforeach
        <!-- // ends available -->
        <!-- Images unavailable -->
        @else
        <div id="loginbox">
          <h1>Start adding photos <a href='./models/myphotos'>here</a>!</h1>
        </div>
        @endif
        <!-- // End unavailable -->

      <!-- This is for no access -->
      @else
        @foreach ($arrayTest as $key=>$value)
        <div class="photo-box">
          @if($value != 'photo_add.png')
          <a href='/uploads/thumbnails/{{$value}}' data-lightbox="model_photos">
          <img class="photo" src="/uploads/model_photos/{{$value}}" alt="Images" /></a>
          @endif
        </div>
        @endforeach
      @endif
          <br clear="all" /><br/>
  </div> <!-- End Profile -->
</div>

@endsection

@section('scripts')

<script>
    lightbox.option({
      'alwaysShowNavOnTouchDevices':true,
      'resizeDuration': 200,
      'fitImagesInViewport':true,
      'wrapAround': true
    })
</script>

@endsection
