@extends('layouts.app')
@section('content')

<img src="/images/banner_content.jpg" class="banner-content">
<div id='content'>
  <div id="profile">
    <div id="loginbox">
        <!-- <h1>Seems like someone is lost!</h1>
        <h1>Click here to return to our <a href='/'>Welcome Page</a>!</h1> -->
        <h1> Oops... the page you're looking for does not exist or may have been removed!</h1>
        <h1>Click here to return to the <a href='/'>Home Page</a>!</h1>
    </div>
    <br clear="all" /><br/>
    </div> <!-- End Profile -->
</div>

@endsection
