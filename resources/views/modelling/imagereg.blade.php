@extends('layouts.app2')

@section('title')
    <title>Create your Model Profile - BookModels.asia</title>
	<meta property="og:title" content="Create your Model Profile - BookModels.asia"/>
@stop

@section('meta')
    <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
	<meta name="description" content="Create your model profile at BookModels.asia and apply for modelling jobs around you.">
	<meta property="og:description" content="Create your model profile at BookModels.asia and apply for modelling jobs around you."/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
	<div id="loginbox">

		<h1>UPLOAD YOUR PROFILE IMAGE</h1>

		@if ($message = Session::get('success'))
		<div class="alert alert-success alert-block">
		  <button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{{ $message }}</strong>
		</div>
		@endif

		<div>
			
			<br/>
			<input  class="foo2" type="file" id="upload" value="UPDATE PROFILE IMAGE"/>
		</div>

		<!-- Trying a new style-->
		<div class="form-group" style='text-align:center'>
				<div id="logo">
				</div>
				<img id="upload-demo" src='/uploads/avatars/{{$profilesImg}}' width="400" height="400" alt="Profile Image" class="logo img-responsive center-block">
		</div>
		<div>
			Use slider to Resize/Reposition the image
			<br/>
			<br/>
			<input class="buttonlink pink upload-result" type="submit" value="SAVE PROFILE IMAGE"/>
		</div>
		
		<div class="form-group" style='text-align:center'>
			<br/>
			<a href='./thankyou'> Or you can skip this for now </a>
		</div>
	</div> <!-- End Login Box -->
</div> <!-- End Content -->

<div id="overlay">
	<p>The Image is uploading, be patient...</p><br/><br/>
	<img src="/images/loading_white.gif" width="32" height="32" alt="loading">
</div>
  <!-- THANK YOU FOR REGISTRING WITH US! YOUR PROFILE IS IN THE WELCOMING EMAIL -->

@endsection

@section('scripts')
<script type="text/javascript">

$uploadCrop = $('#upload-demo').croppie({

    enableExif: true,
		viewport: {
			width: 300,
			height: 300,
		},
		boundary: {
			width: 300,
			height: 300
		}
});


$('#upload').on('change', function () {

	var reader = new FileReader();
	reader.onload = function (e) {
		$uploadCrop.croppie('bind', {
			url: e.target.result
		}).then(function(){
			console.log('jQuery bind complete');
		});
	}
	reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {

	var delay = 1000;
	$.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
	$uploadCrop.croppie('result', {

		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {
		// $('#overlay').show();
		swal({title:"", text:"UPDATING IMAGE", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
		$.ajax({
			url: "/models/img_upload2",
			paramName: "images",
			type: "PATCH",
			data: {"image":resp},
			success: function (response) {
				swal({
					title: "PROFILE IMAGE UPDATED!",
					type: "success",
					closeOnConfirm: true
				},function(){
					setTimeout(function(){ window.location = "/models/thankyou"; }, delay);
				});
			}
		});
	});
});
</script>
@endsection
