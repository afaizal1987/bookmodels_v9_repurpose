@extends('layouts.app')

@section('title')
    <title>My Booking - <?php $test = (App\Test::name()); echo ($test); ?></title>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">

            <div id="jobs">
            <h1>My Booking</h1>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </div>
            @endif


            <!-- Navigation -->
			<div id="tabbing">
				<ul class="nav nav-tabs" role="tablist">
				  <li role="presentation" class='active'><a href="#view" aria-controls="view" role="tab" data-toggle="tab">Active Booking</a></li>
				  <li role="presentation"><a href="#applied" aria-controls="applied" role="tab" data-toggle="tab">Booking History</a></li>
				  <!-- <li role="presentation"><a href="#saved" aria-controls="saved" role="tab" data-toggle="tab">Saved)</a></li> -->
				</ul>
			</div>
            <div class="tab-content">
                  <!-- Tab 1 -->
				          <div role="tabpanel" class="tab-pane active" id="view">
                    @if ($jobsgalore->count() > 0)
                    @foreach ($jobsgalore as $job)
                      <div class="job-box">
                     <h3>{{ $job->title }}</h3>
					  <?php $complink=strtolower($job->name);
					 			$complink = strtr($complink, array(' ' => '.', ',' => ' '));
					 ?>
                     <p>Posted by <strong><a href="/a/{{ $complink }}">{{ $job->company_name}}</a></strong><br/> on <?php $ori=strtotime($job->created_at);
																	 echo $new=date("j-F-Y",$ori);  ?> | Job ID: <?php echo(sprintf("%04s",$job->id));?></p>
                     <p>
                     <!--<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$job->id));?><br/>-->
                     <span class="meta">Job Date</span>: <?php echo $newDate = date("j-F-Y", strtotime($job->start_date)); ?><br/>
                     <span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
                     <span class="meta">Venue</span>: {{ $job->venue }}<br/>
                     <span class="meta">City</span>: {{ $job->location }}<br/>
                     <span class="meta">Payment</span>: RM {{ $job->payment }} (<?php
                       switch($job->payment_terms){
                         case 1:
                         echo 'On The Spot'; break;
                         case 2:
                         echo '1-2 Weeks'; break;
                         case 3:
                         echo '2-4 Weeks'; break;
                         case 4:
                         echo 'More than 1 Month'; break;
                       }
                     ?>)<br/>
                     <span class="meta">Gender</span>: {{ $job->gender }}<br/>
                     <span class="meta">Age</span>: {{ $job->age_min }} - {{ $job->age_max }} years old<br/>
                     <span class="meta">Height</span>: {{ $job->height_min }} - {{ $job->height_max }} cm<br/>
                     <span class="meta">Ethnicity</span>: {{ $job->ethnicity }}<br/>

                       @if(!empty($job->language))<!-- Language -->
                       <span class="meta">Language</span>: {{ $job->language}}<br/>
                     @endif


                       @if(!empty($job->additional_info))<!-- Additional Info -->
                       <br/>
                       <span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
                     @endif
                     </p>
						<?php $mytime = \Carbon\Carbon::now();
								$today =  $mytime->toDateString();
								$main = date( 'Y-m-d H:i:s', strtotime( $today ) );
								$secondary = date( 'Y-m-d H:i:s', strtotime($job->start_date .' -1 day') );
						?>


                      <input class="buttonlink green" type="submit" value="BOOKED" />
					  @if(($secondary)>$main)
							<input id='save' class="buttonlink grey" type="submit"
                      data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="BOOK_CANCEL" value="CANCEL" />
						@endif

                      </div> <!-- End Job Box -->
                      @endforeach
                      @else
                        <div class="job-box">
                          <h3>There are no Bookings yet</h3>
                        </div> <!-- End Job Box -->
                      @endif
                   {{ $jobsgalore->links() }}
                 </div>
                  <!-- Tab 2 -->
				          <div role="tabpanel" class="tab-pane" id="applied">
                    @if ($jobsgalore_old->count() > 0)
                    @foreach ($jobsgalore_old as $job)
                      <div class="job-box">
                     <h3>{{ $job->title }}</h3>
					 <?php $complink=strtolower($job->name);
					 			$complink = strtr($complink, array(' ' => '.', ',' => ' ')); ?>
                     <p>Posted by <strong><a href="/a/{{ $complink }}">{{ $job->company_name}}</a></strong><br/> on <?php $ori=strtotime($job->created_at);
																	 echo $new=date("j-F-Y",$ori);  ?> | Job ID: <?php echo(sprintf("%04s",$job->id));?></p>
                     <p>
                       <!--<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$job->id));?><br/>-->
                       <span class="meta">Job Date</span>: <?php echo $newDate = date("j-F-Y", strtotime($job->start_date)); ?><br/>
                       <span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
                       <span class="meta">Venue</span>: {{ $job->venue }}<br/>
                       <span class="meta">City</span>: {{ $job->location }}<br/>
                       <span class="meta">Payment</span>: RM {{ $job->payment }} (<?php
                       switch($job->payment_terms){
                         case 1:
                         echo 'On The Spot'; break;
                         case 2:
                         echo '1-2 Weeks'; break;
                         case 3:
                         echo '2-4 Weeks'; break;
                         case 4:
                         echo 'More than 1 Month'; break;
                       }
                       ?>)<br/>
                       <span class="meta">Gender</span>: {{ $job->gender }}<br/>
                       <span class="meta">Age</span>: {{ $job->age_min }} - {{ $job->age_max }} years old<br/>
                       <span class="meta">Height</span>: {{ $job->height_min }} - {{ $job->height_max }} cm<br/>
                       <span class="meta">Ethnicity</span>: {{ $job->ethnicity }}<br/>

                       @if(!empty($job->language))<!-- Language -->
                       <span class="meta">Language</span>: {{ $job->language}}<br/>
                       @endif


                       @if(!empty($job->additional_info))<!-- Additional Info -->
                         <br/>
                         <span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
                       @endif
                     </p>

                     <!-- <input id='save' class="buttonlink grey" type="submit"
                             data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="REVIEW" value="REVIEW" /> -->


                             <div class="stars">
                               <form action="./test">
                                 <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
                                 <label class="star star-5" for="star-5"></label>
                                 <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
                                 <label class="star star-4" for="star-4"></label>
                                 <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
                                 <label class="star star-3" for="star-3"></label>
                                 <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
                                 <label class="star star-2" for="star-2"></label>
                                 <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
                                 <label class="star star-1" for="star-1"></label>

                                 <input id='save' class="buttonlink grey" type="submit"
                                         data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="REVIEW" value="REVIEW" />
                               </form>
                             </div>
                      <!--<input class="buttonlink green" type="submit" value="BOOKED" />-->
                      </div> <!-- End Job Box -->
                      @endforeach
                      
                      @else
                        <div class="job-box">
                          <h3>You haven't had a previous job yet</h3>
                        </div> <!-- End Job Box -->
                      @endif
                   {{ $jobsgalore->links() }}

                </div>
           </div><!-- end Testing -->

     </div><!-- End Job -->

	</div> <!-- End Content -->

@endsection

@section('scripts')
<script type='text/javascript' async>
$('input#save').on('click',function(){
    var job_id = $(this).data('job');
    var id = $(this).data('id');
    var status = $(this).data('value');

  	if(status =="BOOK_CANCEL"){
  		var status1 = "Cancel this Booking?";
  	}

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    swal({
      title: status1,
      // text: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },function(){
      swal({title:"", text:"UPDATING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
			$.ajax({
        type: "post",
        url: "{{url('/models/book_cancel')}}",
        data: {job_id:job_id, id:id, status:status},
        success: function (response) {

          if(response.bookcancel == 1){
            swal({
              title: "Booking Cancelled!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
							swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
              location.reload ();
            });
          }
		    }
      });
    });
});
</script>
@endsection
