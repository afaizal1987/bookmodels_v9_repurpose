@extends('layouts.app')

@section('title')
    <title>Create Model Profile - BookModels.asia</title>
@stop

@section('meta')
  <meta property="og:title" content="Create Model Profile - BookModels.asia"/>
@stop


@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
		<div id="loginbox">

			<h1>CREATE MODEL PROFILE</h1>

				<form method="link" action='redirect'>
					<input class="button blue" type="submit" value="SIGN UP WITH FACEBOOK"/>
				</form>
                <br/><br/>
				<p>NOTE: ALL MODEL ACCOUNTS REQUIRE FACEBOOK LOGIN FOR THE PURPOSE OF SOCIAL VERIFICATION. WE'D LIKE TO ENSURE THAT ALL MODELS IN OUR PLATFORM ARE GENUINE.</p>


		</div> <!-- End Login Box -->
	</div> <!-- End Content -->

@endsection
