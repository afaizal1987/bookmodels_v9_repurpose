@extends('layouts.app')

@section('title')
    <title>My Booking - <?php
$test = (App\Test::name());
echo ($test);
?></title>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">

            <div id="jobs">
            <h1>My Booking</h1>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </div>
            @endif


            <!-- Navigation -->
            <div id="tabbing">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active" ><a href="#applied" aria-controls="applied" role="tab" data-toggle="tab">Booking History</a></li>
                  <!-- <li role="presentation"><a href="#saved" aria-controls="saved" role="tab" data-toggle="tab">Saved)</a></li> -->
                </ul>
            </div>
            <div class="tab-content">

                <!-- Tab 2 -->
                <div role="tabpanel" class="tab-pane active" id="applied">
                    @if ($jobsgalore_old->count() > 0)
                    @foreach ($jobsgalore_old as $job)
                      <div class="job-box">
                     <h3>{{ $job->title }}</h3>
                     <?php
                        $complink = strtolower($job->name);
                        $complink = strtr($complink, array(
                            ' ' => '.',
                            ',' => ' '
                          ));
                          ?>
                    <p>Posted by <strong><a href="/a/{{ $complink }}">{{ $job->company_name}}</a></strong><br/> on <?php
                          $ori = strtotime($job->created_at);
                          echo $new = date("j F Y", $ori);
                          ?> | Job ID: <?php
                          echo (sprintf("%04s", $job->id));
                          ?></p>
                     <p>
                       @if(strtotime($job->start_date)==strtotime($job->end_date))
                          <span class="meta">Job Date</span>: <?php
                            echo $newDate = date("j F Y", strtotime($job->start_date));
                            ?><br/>
                        @else
                          <span class="meta">Job Date</span>: <?php
                            echo $newDate = date("j F Y", strtotime($job->start_date));
                            ?> to <?php
                            echo $newDate = date("j F Y", strtotime($job->end_date));
                            ?><br/>
                        @endif
                       <span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
                       <span class="meta">Venue</span>: {{ $job->venue }}<br/>
                       <span class="meta">City</span>: {{ $job->location }}<br/>
                       <span class="meta">Payment</span>: RM {{ $job->payment }} (<?php
                          switch ($job->payment_terms) {
                              case 1:
                                  echo 'On The Spot';
                                  break;
                              case 2:
                                  echo '1-2 Weeks';
                                  break;
                              case 3:
                                  echo '2-4 Weeks';
                                  break;
                              case 4:
                                  echo 'More than 1 Month';
                                  break;
                          }
                          ?>)<br/>
                       <span class="meta">Gender</span>: {{ $job->gender }}<br/>
                       <span class="meta">Age</span>: {{ $job->age_min }} - {{ $job->age_max }} years old<br/>
                       <span class="meta">Height</span>: {{ $job->height_min }} - {{ $job->height_max }} cm<br/>
                       <span class="meta">Ethnicity</span>: {{ $job->ethnicity }}<br/>

                       @if(!empty($job->language))<!-- Language -->
                       <span class="meta">Language</span>: {{ $job->language}}<br/>
                       @endif


                       @if(!empty($job->additional_info))<!-- Additional Info -->
                         <br/>
                         <span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
                       @endif
                     </p>
                        <?php $test = str_replace(' ', '_', $job->title); ?>
                       <input data-id="ISBN-<?php echo (sprintf("%04s", $job->id)); ?>"
                              data-title=<?php echo $test; ?>
                              data-rating="5"
                              id="rate"
                              class="buttonlink green"
                              href="#addBookDialog"
                              type="submit"
                              value="REVIEW NOW!"/>
                      <!--<input class="buttonlink green" type="submit" value="BOOKED" />-->
                      </div> <!-- End Job Box -->
                      @endforeach
                      @else
                        <div class="job-box">
                          <h3>You haven't had a previous job yet</h3>
                        </div> <!-- End Job Box -->
                      @endif
                </div>
            </div><!-- end Testing -->


            <!-- Modal of the review page -->
            <div id="ratingModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                    <div class="modal-content"  style="width:65%">
                      <!-- Modal header -->
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Rating for <span class="modal-title"></span></h4>
                      </div>
                      <!-- Modal body -->
                      <div class="modal-body">
                        <div class="form-group">
                           <label for="name">Name</label>
                           <br/>
                           <input type="hidden" name="bookId" id="bookId" value=""/>
                            <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
                                <label class="star star-5" for="star-5"></label>
                            <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
                                <label class="star star-4" for="star-4"></label>
                            <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
                                <label class="star star-3" for="star-3"></label>
                            <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
                                <label class="star star-2" for="star-2"></label>
                            <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
                                <label class="star star-1" for="star-1"></label>
                        </div>
                        <br/>
                      </div>
                      <!-- Modal Footer -->
                      <div class="modal-footer">
                         <input type="submit" id="submittions" class="buttonlink2 green" value="SUBMIT">
                         <br/>
                        <button type="submit" class="buttonlink2 btn-danger rateme" data-dismiss="modal"> CANCEL</button>
                      </div>
                  </div>
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->

     </div><!-- End Job -->

    </div> <!-- End Content -->

@endsection

@section('scripts')

<script>
    $("input#rate").on('click',function(){
        var pageTitle = $(this).data('title');
        var rate = $(this).data('rating');
        $(".modal-body #bookId").val(rate);
        pageTitle = pageTitle.replace(/_/g,' ');
        $(".modal .modal-title").html(pageTitle);
        $(".modal").modal("show");
    });


</script>

<script>
    $('input#submittions').on('click',function(){
      // console.log('Elementary hit');

      var radiotest = $('.star:checked').val();
      // console.log(radiotest);
      //Once submitted
      $('#ratingModal').modal('hide');

      $("#ratingModal").on('hide.bs.modal', function () {
        $(this)
          .find("input,textarea,select")
             .val(' ')
             .end()
          .find("input[type=checkbox], input[type=radio]")
             .prop("checked", " ")
             .end();
      });
    });
</script>

@endsection
