@extends('layouts.app')

@section('title')
    <title>Job Listing - <?php $test = (App\Test::name()); echo ($test); ?></title>
	<meta property="og:title" content="Job Listing - <?php $test = (App\Test::name()); echo ($test); ?>"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
	<div id="jobs">
            <h1>Job Listing</h1>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </div>
            @endif


            <!-- Navigation -->
			<div id="tabbing">
				<ul class="nav nav-tabs" role="tablist">
				  <li role="presentation" class='active'><a href="#view" aria-controls="view" role="tab" data-toggle="tab">Latest Jobs <!--({{$jobsgalorecount}})--></a></li>
				  <li role="presentation"><a href="#applied" aria-controls="applied" role="tab" data-toggle="tab">Applied ({{$j1count}})</a></li>
				  <li role="presentation"><a href="#saved" aria-controls="saved" role="tab" data-toggle="tab">Saved ({{$j3count}})</a></li>
				</ul>
			</div>

            <div class="tab-content">
							<!-- FIRST TAB -->
            <div role="tabpanel" class="tab-pane active" id="view">

               @if ($jobsgalore2->count() > $jobtestcount)
               <?php $i=0;?>
                @foreach ($jobsgalore2 as $key=>$job)

					<?php $test = (App\Test::testing($job->id,$p_id))?>
					@if (is_bool($test) === true)
					<div class="job-box">
					 <h3>{{ $job->title }}</h3>
					 <?php $complink=strtolower($job->name);
					 			$complink = strtr($complink, array(' ' => '.', ',' => ' '));
					 ?>
					  <p>Posted by <strong><a href="/a/{{ $complink }}">{{ $job->company_name}}</a></strong><br/> on <?php $ori=strtotime($job->created_at);
																	 echo $new=date("j F Y",$ori);  ?> | Job ID: <?php echo(sprintf("%04s",$job->id));?></p>
					  <p>
					  
						@if(!empty($job->casting_start_date))
						
							@if(!empty($job->casting_end_date))
								<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->casting_start_date)); ?>
									to <?php echo $newDate = date("j F Y", strtotime($job->casting_end_date)); ?><br/>
							@else
								<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->casting_start_date)); ?>
									<br/>
							@endif
							
							@if(!empty($job->casting_end_time))
								<span class="meta">Casting Time</span>: {{ $job->casting_start_time}} to {{ $job->casting_end_time}}<br/>
							@else
								<span class="meta">Casting Time</span>: {{ $job->casting_start_time}}<br/>
							@endif
							
							<span class="meta">Location</span>: {{ $job->casting_venue}}, {{ $job->casting_location}}<br/><br/>
						@endif
					  
						<!--<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$job->id));?><br/>-->
						<span class="meta">Job Type</span>: {{ $job->jobtype }}<br/>
						@if(strtotime($job->start_date)==strtotime($job->end_date))
							<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->start_date)); ?><br/>
						@else
							<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->start_date)); ?> to <?php echo $newDate = date("j F Y", strtotime($job->end_date)); ?><br/>
						@endif
						<span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
						<span class="meta">Venue</span>: {{ $job->venue }}<br/>
						<span class="meta">City</span>: {{ $job->location }}<br/>
						<span class="meta">Payment</span>: RM {{ $job->payment }} (<?php
								  switch($job->payment_terms){
									case 1:
									  echo 'On The Spot'; break;
									case 2:
									  echo '1-2 Weeks'; break;
									case 3:
									  echo '2-4 Weeks'; break;
									case 4:
									  echo 'More than 1 Month'; break;
								  }
								?>)<br/>
						<span class="meta">Gender</span>: {{ $job->gender }}<br/>
						<span class="meta">Age</span>: {{ $job->age_min }} - {{ $job->age_max }} years old<br/>
						<span class="meta">Height</span>: {{ $job->height_min }} - {{ $job->height_max }} cm<br/>
						<span class="meta">Ethnicity</span>: {{ $job->ethnicity }}<br/>

						@if(!empty($job->language))<!-- Language -->
							<span class="meta">Language</span>: {{ $job->language}}<br/>
						@endif
			
						@if(!empty($job->additional_info))<!-- Additional Info -->
							<br/>
							<span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
						@endif
					</p>
					<!-- <br/> -->
						
						@if(!empty($job->casting_start_date))
						<p>*Note: Only shortlisted models will be invited to Casting</p>
						<input id='save' class="buttonlink pink" type="submit"
							data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="ATTEND" value="ATTEND CASTING" />
						@else
						<input id='save' class="buttonlink pink" type="submit"
							data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="APPLY" value="APPLY NOW" />
						@endif
						<input id='save' class="buttonlink grey" type="submit"
						 data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="SAVE" value="SAVE" />

					</div> <!-- End Job Box -->
					@endif<!-- Tracker-->
				   @endforeach
				   <!-- Add another checker here -->

                   @else
                       <div class="job-box">
                           We haven't found any new jobs for you yet. Please check again later.
                       </div> <!-- End Job Box -->
                   @endif
                   
			</div>
			<!--/ FIRST TAB -->
			<!-- SECOND TAB -->
            <div role="tabpanel" class="tab-pane" id="applied">
				@if ($job1->count() > 0)
					@foreach ($job1 as $job)
						@if(($job->status == 1)||($job->status == 5))
							<div class="job-box">
								<h3>{{ $job->title }}</h3>
								<?php $complink=strtolower($job->name);
		 					 			$complink = strtr($complink, array(' ' => '.', ',' => ' '));
		 					 	?>
								<p>Posted by <strong><a href="/a/{{ $complink }}">{{ $job->company_name}}</a></strong><br/> on <?php $ori=strtotime($job->created_at);
																	 echo $new=date("j F Y",$ori);  ?> | Job ID: <?php echo(sprintf("%04s",$job->id));?></p>
								<p>
								
								@if(!empty($job->casting_start_date))
						
									@if(!empty($job->casting_end_date))
										<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->casting_start_date)); ?>
											to <?php echo $newDate = date("j F Y", strtotime($job->casting_end_date)); ?><br/>
									@else
										<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->casting_start_date)); ?>
											<br/>
									@endif
									
									@if(!empty($job->casting_end_time))
										<span class="meta">Casting Time</span>: {{ $job->casting_start_time}} to {{ $job->casting_end_time}}<br/>
									@else
										<span class="meta">Casting Time</span>: {{ $job->casting_start_time}}<br/>
									@endif
									
									<span class="meta">Location</span>: {{ $job->casting_venue}}, {{ $job->casting_location}}<br/><br/>
								@endif
								
								<!--<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$job->id));?><br/>-->
								<span class="meta">Job Type</span>: <?php echo $job->jobtype == ('Runwayshow') ? 'Runway Show' : $job->jobtype; ?><br/>
								@if(strtotime($job->start_date)==strtotime($job->end_date))
									<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->start_date)); ?><br/>
								@else
									<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->start_date)); ?> to <?php echo $newDate = date("j F Y", strtotime($job->end_date)); ?><br/>
								@endif
								<span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
								<span class="meta">Venue</span>: {{ $job->venue }}<br/>
								<span class="meta">City</span>: {{ $job->location }}<br/>
								<span class="meta">Payment</span>: RM {{ $job->payment }} (<?php
									switch($job->payment_terms){
										case 1:
											echo 'On The Spot'; break;
										case 2:
											echo '1-2 Weeks'; break;
										case 3:
											echo '2-4 Weeks'; break;
										case 4:
											echo 'More than 1 Month'; break;
									}
								?>)<br/>
								<span class="meta">Gender</span>: {{ $job->gender }}<br/>
								<span class="meta">Age</span>: {{ $job->age_min }} - {{ $job->age_max }} years old<br/>
								<span class="meta">Height</span>: {{ $job->height_min }} - {{ $job->height_max }} cm<br/>
								<span class="meta">Ethnicity</span>: {{ $job->ethnicity }}<br/>

								@if(!empty($job->language))<!-- Language -->
									<span class="meta">Language</span>: {{ $job->language}}<br/>
								@endif

								@if($job->casting_date != '0' && !empty($job->casting_date))<!-- Casting -->
									<!--<span class="meta">Casting</span>: {{ $job->casting_time}}, <?php echo $newDate = date("j F Y", strtotime($job->casting_date)); ?><br/>-->
								@endif

								@if(!empty($job->additional_info))<!-- Additional Info -->
									<br/>
									<span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
								@endif
								</p>
								<br/>
								<input class="buttonlink orange" type="submit" value="APPLIED" />
								<input id='save' class="buttonlink grey" type="submit"
										data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="CANCEL" value="CANCEL" />
							</div> <!-- End Job Box -->
						@endif
					@endforeach
				@else
                    <div class="job-box">
                        <h3>You haven't applied for any jobs yet.</h3>
                    </div> <!-- End Job Box -->
             @endif
             
            </div>
						<!--/ SECOND TAB -->
						<!-- THIRD TAB -->
            <div role="tabpanel" class="tab-pane" id="saved">
				@if ($job3->count() > 0)
					@foreach ($job3 as $job)
						@if($job->status == 3)
						<div class="job-box">
							<h3>{{ $job->title }}</h3>
								<?php $complink=strtolower($job->name);
										$complink = strtr($complink, array(' ' => '.', ',' => ' '));
								?>

								<p>Posted by <strong><a href="/a/{{ $complink }}">{{ $job->company_name}}</a></strong><br/> on <?php $ori=strtotime($job->created_at);
																	 echo $new=date("j F Y",$ori);  ?> | Job ID: <?php echo(sprintf("%04s",$job->id));?></p>
							<p>
								@if(!empty($job->casting_start_date))
							
									@if(!empty($job->casting_end_date))
										<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->casting_start_date)); ?>
											to <?php echo $newDate = date("j F Y", strtotime($job->casting_end_date)); ?><br/>
									@else
										<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->casting_start_date)); ?>
											<br/>
									@endif
									
									@if(!empty($job->casting_end_time))
										<span class="meta">Casting Time</span>: {{ $job->casting_start_time}} to {{ $job->casting_end_time}}<br/>
									@else
										<span class="meta">Casting Time</span>: {{ $job->casting_start_time}}<br/>
									@endif
									
									<span class="meta">Location</span>: {{ $job->casting_venue}}, {{ $job->casting_location}}<br/><br/>
								@endif
								<!--<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$job->id));?><br/>-->
								<span class="meta">Job Type</span>: {{ $job->jobtype }}<br/>
								@if(strtotime($job->start_date)==strtotime($job->end_date))
									<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->start_date)); ?><br/>
								@else
									<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($job->start_date)); ?> to <?php echo $newDate = date("j F Y", strtotime($job->end_date)); ?><br/>
								@endif
								<span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
								<span class="meta">Venue</span>: {{ $job->venue }}<br/>
								<span class="meta">City</span>: {{ $job->location }}<br/>
								<span class="meta">Payment</span>: RM {{ $job->payment }} (<?php
								  switch($job->payment_terms){
									case 1:
									  echo 'On The Spot'; break;
									case 2:
									  echo '1-2 Weeks'; break;
									case 3:
									  echo '2-4 Weeks'; break;
									case 4:
									  echo 'More than 1 Month'; break;
								  }
								?>)<br/>
								<span class="meta">Gender</span>: {{ $job->gender }}<br/>
								<span class="meta">Age</span>: {{ $job->age_min }} - {{ $job->age_max }} years old<br/>
								<span class="meta">Height</span>: {{ $job->height_min }} - {{ $job->height_max }} cm<br/>
								<span class="meta">Ethnicity</span>: {{ $job->ethnicity }}<br/>


								@if(!empty($job->language))<!-- Language -->
									<span class="meta">Language</span>: {{ $job->language}}<br/>
								@endif

								@if($job->casting_date != '0' && !empty($job->casting_date))<!-- Casting -->
									<!--<span class="meta">Casting</span>: {{ $job->casting_time}}, <?php echo $newDate = date("j F Y", strtotime($job->casting_date)); ?><br/>-->
								@endif

								@if(!empty($job->additional_info))<!-- Additional Info -->
								<br/>
								<span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
								@endif
							</p>
							<br/>
							<input id='save' class="buttonlink pink" type="submit"
							   data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="APPLY" value="APPLY NOW" />
							   <input id='save' class="buttonlink grey" type="submit"
							   data-job="{{$job->id}}" data-id="{{$p_id}}" data-value="REMOVE" value="CANCEL" />
						</div> <!-- End Job Box -->
						@endif
					@endforeach
               @else
					<div class="job-box">
						<h3>You haven't saved any jobs. </h3>
					</div> <!-- End Job Box -->
               @endif
               
            </div>
						<!--/ THIRD TAB -->
           </div><!-- end Testing -->

	</div><!-- End Job -->
</div> <!-- End Content -->

@endsection

@section('scripts')
<script type='text/javascript' async>
$('input#save').on('click',function(){
    var job_id = $(this).data('job');
    var id = $(this).data('id');
    var status = $(this).data('value');

	if(status =="APPLY"){
		status1 = "Apply for this Job?";
	}
	
	if(status =="ATTEND"){
		status1 = "Attend this Casting?";
	}

	if(status =="SAVE"){
		status1 = "Save this Job?";
	}
	if(status =="CANCEL"){
		status1 = "Cancel this Job application?";
	}
	if(status =="REMOVE"){
		status1 = "Remove from Saved items?";
	}

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    swal({
      title: status1,
      // text: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },function(){
			swal({title:"", text:"UPDATING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
			$.ajax({
        type: "post",
        url: "{{url('/models/job_test')}}",
        data: {job_id:job_id, id:id, status:status},
        success: function (response) {
          if(response.save == 1){
            swal({
              title: "Job Saved!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
          else if(response.applied == 1){
            swal({
              title: "Job Applied!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
		  else if(response.attend == 1){
            swal({
              title: "Casting Applied!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
		  else if(response.removed == 1){
            swal({
              title: "Saved Job has been removed!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
								    location.reload();
								},700);
            });
          }
          else{

            swal({
              title: "Job Application cancelled!",
              type: "warning",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
        }
      });
    });
});
</script>
<script>

</script>
@endsection
