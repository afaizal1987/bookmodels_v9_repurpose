@extends('layouts.app')

@section('title')
    <title>{{$jobs->title}} - Bookmodels.Asia</title>

@stop

@section('meta')
  <meta property="og:url" content="https://bookmodels.asia/job/<?php echo(sprintf("%04s",$jobs->id));?>"/>
  <meta property="og:title" content="{{$jobs->title}}- Bookmodels.Asia"/>
  <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
  <meta name="description" content="Who and What we are">
  <meta charset="utf-8">
  <meta property="og:image" content="https://bookmodels.asia/uploads/avatars/{{$jobs->avatar}}"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
	<div id="jobs">
            <h2>Job Info: <?php echo(sprintf("%04s",$jobs->id));?></h2>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </div>
            @endif


  					 <h3>{{ $jobs->title }}</h3>
  					 <?php $complink=strtolower($jobs->name);
  					 			$complink = strtr($complink, array(' ' => '.', ',' => ' '));
  					 ?>
  					  <p>Posted by <strong><a href="/a/{{ $complink }}">{{ $jobs->company_name}}</a></strong><br/> on <?php $ori=strtotime($jobs->created_at);
  																	 echo $new=date("j F Y",$ori);  ?> | Job ID: <?php echo(sprintf("%04s",$jobs->id));?></p>
  					  <p>

  						@if(!empty($jobs->casting_start_date))

  							@if(!empty($jobs->casting_end_date))
  								<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($jobs->casting_start_date)); ?>
  									to <?php echo $newDate = date("j-F-Y", strtotime($jobs->casting_end_date)); ?><br/>
  							@else
  								<span class="meta">Casting Date</span>: <?php echo $newDate = date("j F Y", strtotime($jobs->casting_start_date)); ?>
  									<br/>
  							@endif

  							@if(!empty($jobs->casting_end_time))
  								<span class="meta">Casting Time</span>: {{ $jobs->casting_start_time}} to {{ $jobs->casting_end_time}}<br/>
  							@else
  								<span class="meta">Casting Time</span>: {{ $jobs->casting_start_time}}<br/>
  							@endif

  							<span class="meta">Location</span>: {{ $jobs->casting_venue}}, {{ $jobs->casting_location}}<br/><br/>
  						@endif

  						<!--<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$jobs->id));?><br/>-->
  						<span class="meta">Job Type</span>: {{ $jobs->jobtype }}<br/>
						
						@if(strtotime($jobs->start_date)==strtotime($jobs->end_date))
							<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($jobs->start_date)); ?><br/>
						@else
							<span class="meta">Job Date</span>: <?php echo $newDate = date("j F Y", strtotime($jobs->start_date)); ?> to <?php echo $newDate = date("j F Y", strtotime($jobs->end_date)); ?><br/>
						@endif
  						<span class="meta">Job Time</span>: {{ $jobs->start_time }} to {{ $jobs->end_time }} <br/>
  						<span class="meta">Venue</span>: {{ $jobs->venue }}<br/>
  						<span class="meta">City</span>: {{ $jobs->location }}<br/>
  						<span class="meta">Payment</span>: RM {{ $jobs->payment }} (<?php
  								  switch($jobs->payment_terms){
  									case 1:
  									  echo 'On The Spot'; break;
  									case 2:
  									  echo '1-2 Weeks'; break;
  									case 3:
  									  echo '2-4 Weeks'; break;
  									case 4:
  									  echo 'More than 1 Month'; break;
  								  }
  								?>)<br/>
  						<span class="meta">Gender</span>: {{ $jobs->gender }}<br/>
  						<span class="meta">Age</span>: {{ $jobs->age_min }} - {{ $jobs->age_max }} years old<br/>
  						<span class="meta">Height</span>: {{ $jobs->height_min }} - {{ $jobs->height_max }} cm<br/>
  						<span class="meta">Ethnicity</span>: {{ $jobs->ethnicity }}<br/>

  						@if(!empty($jobs->language))<!-- Language -->
  							<span class="meta">Language</span>: {{ $jobs->language}}<br/>
  						@endif

  						@if(!empty($jobs->additional_info))<!-- Additional Info -->
  							<br/>
  							<span class="meta">More Info</span>:<br/>{{ $jobs->additional_info }}<br/>
  						@endif
  					</p>
  					<!-- <br/> -->

				
            @if (Auth::guest())
              <?php
                $job_id = sprintf("%04s",$jobs->id);
                $url =   'https://bookmodels.asia/job/'.$job_id;
              ?>
              @include('partials.share', ['url' => $url])
            @elseif($marker==3)
              @if(!empty($jobs->casting_start_date))
              <p>*Note: Only shortlisted models will be invited to Casting</p>
              <input id='save' class="buttonlink pink" type="submit"
                data-job="{{$jobs->id}}" data-id="{{$p_id}}" data-value="ATTEND" value="ATTEND CASTING" />
              @else
              <input id='save' class="buttonlink pink" type="submit"
                data-job="{{$jobs->id}}" data-id="{{$p_id}}" data-value="APPLY" value="APPLY NOW" />
              @endif
              <input id='save' class="buttonlink grey" type="submit"
               data-job="{{$jobs->id}}" data-id="{{$p_id}}" data-value="SAVE" value="SAVE" />
			@else
				<div class="job-box">
                      We're sorry but you do not meet the requirements for this job.
					  Go to <a href='/models/job_model'>Job Listings</a> for other suitable jobs to apply!
                  </div> <!-- End Job Box -->
            @endif


	</div><!-- End Job -->
</div> <!-- End Content -->

@endsection

@section('scripts')

<script>

    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.social-buttons > a', function(e){

        var
            verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horisontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });
</script>

<!-- <script type='text/javascript' async>
$('input#save').on('click',function(){
    var job_id = $(this).data('job');
    var id = $(this).data('id');
    var status = $(this).data('value');

	if(status =="APPLY"){
		status1 = "Apply for this Job?";
	}

	if(status =="ATTEND"){
		status1 = "Attend this Casting?";
	}

	if(status =="SAVE"){
		status1 = "Save this Job?";
	}
	if(status =="CANCEL"){
		status1 = "Cancel this Job application?";
	}
	if(status =="REMOVE"){
		status1 = "Remove from Saved items?";
	}

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    swal({
      title: status1,
      // text: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },function(){
			swal({title:"", text:"UPDATING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
			$.ajax({
        type: "post",
        url: "{{url('/models/job_test')}}",
        data: {job_id:job_id, id:id, status:status},
        success: function (response) {
          if(response.save == 1){
            swal({
              title: "Job Saved!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
          else if(response.applied == 1){
            swal({
              title: "Job Applied!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
		  else if(response.attend == 1){
            swal({
              title: "Casting Applied!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
		  else if(response.removed == 1){
            swal({
              title: "Saved Job has been removed!",
              type: "success",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
								    location.reload();
								},700);
            });
          }
          else{

            swal({
              title: "Job Application cancelled!",
              type: "warning",
							timer: 1700,
							showConfirmButton: true
            },function(){
								setTimeout(function(){
									swal({title:"", text:"REFRESHING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
										location.reload();
								},700);
            });
          }
        }
      });
    });
});
</script> -->

@endsection
