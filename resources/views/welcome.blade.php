@extends('layouts.app')
@section('title')
    <title>Welcome to BookModels.asia - Model Booking Made Easy</title>
@stop

@section('meta')

  <meta property="og:title" content="Welcome to BookModels.asia - Model Booking Made Easy"/>
  <meta property="og:image" content="https://bookmodels.asia/images/banner_models.jpg"/>
  <meta property="og:url" content="https://bookmodels.asia"/>
  <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
  <meta name="description" content="BookModels.asia is a fresh new platform to effectively connect Malaysian models and aspiring talents directly to verified agencies or photographers.">
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
@stop

@section('content')
<!-- Begin Banner -->
<div id="banner-container">
		<img src="images/banner_models.jpg" class="banner">
		<div class="banner-text">
		<p>MODEL BOOKING MADE EASY. CREATE YOUR PROFILE NOW »</p>
		<a href="/signup" class="button">MODEL</a>
		<a href="agency" class="button">AGENCY</a>
		</div>
		</div> <!-- End Banner -->


		<br clear="all" />
		<div id="content">

		<div class="blurb"><h1>Join Malaysian models & talents who are already <span class="textred">getting booked</span> on BookModels.asia. We have created a <span class="textred">simple, easy to use</span> booking platform for everyone.</h1></div>

				<div id="features">

		        <div class="features-box">
				<h2>ONLINE PROFILE</h2>
				<p>If you're a model or talent, simply create your online profile you can regularly update and easily share with potential clients, casting directors, agents, etc. You can even generate compcards via our online tools.</p>
		        </div>

		        <div class="features-box">
				<h2>VERIFIED CLIENTS</h2>
				<p>If you've been searching for a safe platform to get legit jobs, look out for our &quot;verified&quot; icon. We encourage all agency users to verify their business so that we can eventually weed out the risky ones.</p>
		        </div>

		        <div class="features-box">
				<h2>SECURE PLATFORM</h2>
				<p>We have built a secure platform where all your personal details will remain safe, and we will continue to improve our security to keep up with emerging threats. you can be assured that you're in safe hands.</p>
		        </div>

				</div> <!-- End Features -->

				<div id="testimonial">

		        <div class="testi-box">
		        <img src="images/testi_sindhu.png" width="180" height="180">
				<h3>Sindhu Menon, 25 years old</h3>
				<div class="testi">"Working with the BookModels.asia team has been pretty amazing so far. They have worked really hard to build a great platform to help models easily get booked for jobs, in a safe environment."</div>
		        </div>

		        <div class="testi-box">
		        <img src="images/testi_kyle.png" width="180" height="180">
				<h3>Kyle, 21 years old</h3>
				<div class="testi">"I've never seen any good model booking app in Malaysia, so I'm really happy about BookModels.asia. I hope everyone starts using this site so we can bring all models and clients together."</div>
		        </div>

		        <div class="testi-box">
		        <img src="images/testi_monica.png" width="180" height="180">
				<h3>Monica Picca, 20 years old</h3>
				<div class="testi">"I'm relatively still new to modelling, so I'm glad to be part of BookModels.asia and their efforts to make it easy for new talents to be discovered by clients. I also need a safe app to avoid shady talent agents."</div>
		        </div>

				</div> <!-- End Testimonials -->

		</div> <!-- End Content -->
@endsection
