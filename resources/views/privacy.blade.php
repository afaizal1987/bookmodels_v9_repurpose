@extends('layouts.app')
@section('title')
    <title>Privacy - Model Booking Made Easy</title>

@stop

@section('meta')
  <meta property="og:url" content="https://bookmodels.asia"/>
  <meta property="og:title" content="Privacy - Model Booking Made Easy"/>
  <meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
  <meta name='description' content='The number one model booking platform to connect Malaysian Models and Talents directly to verified Agencies and Photographers'>
  <meta charset="utf-8">
  <meta property="og:image" content="https://bookmodels.asia/images/banner_models.jpg"/>
@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
  <div id="loginbox">
    <h1> Page is under construction</h1>
  </div>

</div> <!-- End Content -->
@endsection
