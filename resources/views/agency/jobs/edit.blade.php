@extends('agency.layout.auth')

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
    <div id="loginbox">

      <div>
      @include('agency.jobs.form2')
        <input class="buttonlink pink" style="height:35px;padding-bottom:5px;" type="submit" value="UPDATE JOB"/>
      {{ Form::close() }}
      <form action="/agency/job_listing">
        <input class="buttonlink grey" style="height:35px;padding-bottom:5px;" type="submit" value="CANCEL"/>
      </form>
      </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/fluid-labels.js') }}"></script>
<script type='text/javascript'>
  $('.fluid-label').fluidLabel();
</script>
<script>
$(function() {
  $( "#castingpicker" ).datepicker({
    format: 'dd-mm-yyyy',
    startDate: '-1d',
    autoclose: 'true'
  });
});
</script>
<script>
// $('#starttime').timepicker({'minTime': '12:00am','maxTime': '11:30pm'});
// $('#endtime').timepicker({'minTime': '12:00am','maxTime': '11:30pm'});
$('#castingtime').timepicker({'minTime': '12:00am','maxTime': '11:30pm'});
</script>

<!-- <script type='text/javascript'>
$(document).ready(function(){
        $("#name").change(function(){
             $("#message").html("<i class='fa fa-spinner' aria-hidden='true'></i>");


        var name=$("#name").val();

          //alert(username);
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
                type:"post",
                 dataType: "json",
                 url :"{{URL::to('agency/yourname') }}",
                 data: {"name": name},
                 success: function(receivedData) {
                   if(receivedData['status']==1){
                      $('#usercheck').parent('div').removeClass('has-error').addClass('has-success');
                   }else{
                     $('#usercheck').parent('div').removeClass('has-success').addClass('has-error');
                   }
                   $('#usercheck').html(receivedData.message);
                }
             });

        });

     });
</script> -->
<script>
    // initialize input widgets first
    $('#basicExample .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('#basicExample .date').datepicker({
        'format': 'dd-mm-yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('basicExample');
    var datepair = new Datepair(basicExampleEl,{
      'defaultTimeDelta': 10800000 // milliseconds
    });
</script>
@endsection
