@extends('agency.layout.auth')

@section('title')
<title>Job Listing - <?php $test = (Auth::guard('agency')->user()->name); echo ucwords($test); ?></title>
@endsection

@section('content')

<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
        <div id="jobs">


			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="view">
				</div>
			</div>

          <h1>Job Listing</h1>
          <!--<a href="http://bookmodels.asia/agency/jobs/create" class="btn2 pink">CREATE A NEW JOB</a><br/>-->
		  <input class="buttonlink pink" type="submit" value="CREATE A NEW JOB" onClick="window.location = './jobs/create';"/>
		  <input class="buttonlink orange" type="submit" value="CREATE A NEW CASTING JOB" onClick="window.location = './casting/create';"/><br/>


          @if ($jobs->count() > 0)
            @foreach ($jobs as $job)
              <div class="job-box">
                <h3>{{ $job->title }}</h3>

                <p>Posted by <strong>{{$company_name}}</strong><br/> on <?php echo $newDate = date("j-F-Y", strtotime($job->created_at)); ?></p>
				<p>
				<span class="meta">Job ID</span>: <?php echo(sprintf("%04s",$job->id));?><br/>
				<span class="meta">Job Date</span>: <?php echo $newDate = date("j-F-Y", strtotime($job->start_date)); ?><br/>
                <span class="meta">Job Time</span>: {{ $job->start_time }} to {{ $job->end_time }} <br/>
                <span class="meta">Venue</span>: {{ $job->venue}}<br/>
                <span class="meta">City</span>: {{ $job->location}}<br/>
                <span class="meta">Payment</span>: RM {{ $job->payment}} ( <?php
                      switch($job->payment_terms){
                        case 1:
                          echo 'On The Spot'; break;
                        case 2:
                          echo '1-2 Weeks'; break;
                        case 3:
                          echo '2-4 Weeks'; break;
                        case 4:
                          echo 'More than 1 Month'; break;
                      }
                    ?> )<br/>
                <span class="meta">Gender</span>:
                @if($job->gender=='1')
                  Male
                @elseif($job->gender=='2')
                  Female
                @else
                  All
                @endif
                <br/>
                <span class="meta">Age</span>: {{ $job->age_min}} - {{ $job->age_max}} years old<br/>
                <span class="meta">Height</span>: {{ $job->height_min}} - {{ $job->height_max}} cm<br/>
                <span class="meta">Ethnicity</span>: {{ $job->ethnicity}}<br/>

				@if(!empty($job->language))
				  <span class="meta">Language</span>: {{ $job->language}}<br/>
				@endif

				@if(!empty($job->casting_time))
                  <!--<span class="meta">Casting</span>: {{ $job->casting_time}}, <?php echo $newDate = date("j-F-Y", strtotime($job->casting_date)); ?><br/>-->
				@endif

				@if(!empty($job->additional_info))
				<br/>
				<span class="meta">More Info</span>:<br/>{{ $job->additional_info }}.<br/>
				@endif
					</p>
					<br/>

						@if($job->application_status_count==0)
							{{ link_to_route('jobs.edit', 'EDIT', $job->id, ['class' => 'btn2 grey']) }}
						@endif


						@if($job->enable =='1')
						<input id="disable" data-job="{{$job->id}}" data-value="DISABLE" class="buttonlink grey" type="submit" value="DISABLE" />
						@else
						<input id="disable" data-job="{{$job->id}}" data-value="ENABLE" class="buttonlink grey" type="submit" value="ENABLE" />
						@endif

						@if($job->application_status_count>0)
						<a class="btn2 green" href="/agency/applications/{{ $job->id }}">APPLIED ({{ $job->application_status_count }})</a>
						@else
						<a class="btn2 green">APPLIED ({{ $job->application_status_count }})</a>
						@endif

                        <!-- <input class="buttonlink grey" type="submit" value="EDIT" /> -->
				</div> <!-- End Job Box -->
              @endforeach
              @else
                  <div class="job-box">
                      <h3>No jobs created yet</h3>
                  </div> <!-- End Job Box -->
              @endif
			  <div>
				{{ $jobs->links() }}
			  </div>

          </div> <!-- End Jobs -->

      </div> <!-- End Content -->


@endsection

@section('scripts')
<script type='text/javascript' async>
$('input#disable').on('click',function(){
    var job_id = $(this).data('job');
	var status = $(this).data('value');
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    swal({
      title: status+" this Job?",
      // text: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },function(){
      $.ajax({
        type: "post",
        url: "{{url('/agency/disable')}}",
        data: {job_id:job_id, status:status},
        success: function (response) {
          if(response['disabled'] == 1){
            // location.reload ();
            swal({
              title: "Job Disabled",
              type: "success",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.reload();
            });
          }

		  if(response['enabled'] == 1){
            // location.reload ();
            swal({
              title: "Job Enabled",
              type: "success",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.reload();
            });
          }
        }
      });
    });
});
</script>
@endsection
