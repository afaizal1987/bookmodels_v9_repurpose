@extends('agency.layout.auth')

@section('title')
<title>BookModels.asia - <?php $test = (Auth::guard('agency')->user()->name); echo ucwords($test); ?></title>
@endsection

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
 <div id="profile">
   @if ($message = Session::get('status'))
   <div class="alert alert-success alert-block">
     <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
   <div class="profileimg-box">
            <img class="profileimg" src="/uploads/avatars/{{$agentProfile->avatar}}" alt="Profile Image"/><br/>
            <!-- <form action="{{ url('/agency/img_upload') }}" enctype="multipart/form-data" id='form' method="POST">
              {{ csrf_field() }}
              {{ method_field('PATCH') }}
                <input class="foo" accept="image/*" type="file" name="image" style='width:100%' onchange="this.form.submit()"/>

            </form> -->
            <input class="buttonlink2 pink" type="submit" name="image" style='width:100%' value="UPDATE PROFILE IMAGE" onClick="window.location = '/agency/image_upload';"/>
            </div>
            <div class="profile-box">
              <h2>{{$agentProfile->company_name}} @if($agentProfile->verification=='No')
                </h2><a href='./verification'><span style="color:red">(Unverified)</span></a><br/>
                @else
                <img src="/images/verified.png" width="24" height="23" alt="Verified"></h2>
                @endif
                Since {{$agentProfile->founding}}, {{$agentProfile->business_type}}

                <br/><br/>
                <!--Address	:<br/>{{$agentProfile->office_address}}.<br/>-->
                <!-- Public Profile: <a href='./{{$linkname}}'>{{$agentProfile->company_name}}</a> -->
                <br/><br/>

                <!-- <strong>Agency Rating - 80% (24 reviews)</strong><br/> -->
                <input class="buttonlink pink" type="submit" value="EDIT PROFILE" onClick="window.location = './update';"/>
                <input class="buttonlink pink" type="submit" value="MY PHOTOS" onClick="window.location = './myphotos';"/>
			</div> <!-- End Profile Box -->
            <div class="agencymeta-box">
				<h3>Contact Information</h3>

				<span class="meta">E-mail</span>: {{$agentProfile->email}}<br/>
				<span class="meta">Contact</span>: {{$agentProfile->contact_person}}<br/>
				<span class="meta">Phone</span>: {{$agentProfile->phone_no}}<br/>
        <span class="meta">Website</span>: @if(!empty($agentProfile->website))
                                              <a href="{{$agentProfile->website}}" target="_blank">{{$agentProfile->website}}</a>
                                            @else
                                              None
                                            @endif
        <br/><br/>


        @if(!empty($agentProfile->facebook))
          <a href="{{$agentProfile->facebook}}" target="_blank"><img src="/images/ico_fb.png" width="48" height="47" alt="Facebook" /></a>
        @endif
        @if(!empty($agentProfile->twitter))
          <a href="{{$agentProfile->twitter}}" target="_blank"><img src="/images/ico_tw.png" width="48" height="47" alt="Twitter" /></a>
        @endif
        @if(!empty($agentProfile->instagram))
          <a href="{{$agentProfile->instagram}}" target="_blank"><img src="/images/ico_ig.png" width="48" height="47" alt="Instagram" /></a>
        @endif

  </div> <!-- End Profile Box -->

        <br clear="all" />
        <h2>My Photos</h2>

        @foreach ($arrayTest as $key=>$value)
        <div class="photo-box">
          <a href='/uploads/thumbnails/{{$value}}' data-lightbox="agency_photos">
            <img class="photo" src="/uploads/model_photos/{{$value}}" alt="Images" /></a>
        </div>
        @endforeach
            <br clear="all" /><br/>
  </div> <!-- End Profile -->
</div>
@endsection

@section('scripts')
<script>
    lightbox.option({
      'alwaysShowNavOnTouchDevices':true,
      'resizeDuration': 200,
      'maxHeight':600,
      'wrapAround': true,
      'positionFromTop': 100
    })
</script>

@endsection
