@extends('agency.layout.auth')

@section('title')
<title>COmpany Verification - <?php $test = (Auth::guard('agency')->user()->name); echo ucwords($test); ?></title>
@endsection

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
  <div id="loginbox">
    <h1>AGENCY VERIFICATION</h1>

    <form action="./verify" enctype="multipart/form-data" id='form' method="POST">
      {{ csrf_field() }}
	  1. Upload your business registration document.<br/><br/>
      <input class="foo2" accept="file/*" type="file" name="file" style='width:100%'/><br/>

	  2. Submit your business registration document.<br/><br/>
      <button id='verify' class="buttonlink pink" type='submit'>SUBMIT DOCUMENT</button>

    </form>

  </div> <!-- End Profile -->
</div>
@endsection

@section('scripts')

@endsection
