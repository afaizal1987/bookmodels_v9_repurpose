@extends('agency.layout.auth')

@section('content')
    <div class="container">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ Session::get('message') }}asd
            </div>
        @endif
        <div class="row">
            {{ link_to_route('jobs.create', 'Create Job', [], ['class' => 'btn btn-info']) }}
            <table class="table table-responsive table-bordered">
                <tr>
                    <td>Title</td>
                    <td>Date</td>
                    <td>Time</td>
                    <td>Action</td>
                </tr>
                @if ($jobs->count() > 0)
                @foreach ($jobs as $job)
                    <tr>
                        <td>{{ $job->title }}</td>
                        <td>{{ $job->casting_date }}</td>
                        <td>{{ $job->casting_time }}</td>
                        <td>
                            <div class="btn-group">
                                {{ Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'DELETE']) }}
                                    {{ link_to_route('jobs.edit', 'Edit', $job->id, ['class' => 'btn btn-info']) }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <td colspan="4">No jobs</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection
