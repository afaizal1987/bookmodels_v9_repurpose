@extends('agency.layout.auth')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Job</div>
                    @include('agency.jobs.form')
                </div>
            </div>
        </div>
    </div>
@endsection
