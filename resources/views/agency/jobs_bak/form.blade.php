<div class="panel-body">
    {{ Form::model($job, ['route' => $route, 'method' => $method]) }}
    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', old('title'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('start_date', 'Start Date') }}
        {{ Form::date('start_date', old('start_date'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('end_date', 'End Date') }}
        {{ Form::date('end_date', old('end_date'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('start_time', 'Start Time') }}
        {{ Form::text('start_time', old('start_time'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('end_time', 'End Time') }}
        {{ Form::text('end_time', old('end_time'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('venue', 'Venue') }}
        {{ Form::text('venue', old('venue'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('location', 'Location') }}
        {{ Form::text('location', old('location'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('gender', 'Gender') }}
        {{ Form::select('gender', [1 => 'Male', 2 => 'Female', 3 => 'Both'], old('gender'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('payment', 'Payment') }}
        {{ Form::text('payment', old('payment'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('payment_terms', 'Payment Terms') }}
        {{ Form::text('payment_terms', old('payment_terms'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('casting_date', 'Casting Date') }}
        {{ Form::date('casting_date', old('casting_date'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('casting_time', 'Casting Time') }}
        {{ Form::text('casting_time', old('casting_time'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('age_min', 'Min Age') }}
        {{ Form::number('age_min', old('age_min'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('age_max', 'Max Age') }}
        {{ Form::number('age_max', old('age_max'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('height_min', 'Min Height') }}
        {{ Form::number('height_min', old('height_min'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('height_max', 'Max Height') }}
        {{ Form::number('height_max', old('height_max'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('ethnicity', 'Ethnicity') }}
        {{ Form::text('ethnicity', old('ethnicity'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('language', 'Language') }}
        {{ Form::text('language', old('language'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('additional_info', 'Additional Info') }}
        {{ Form::text('additional_info', old('additional_info'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('status', 'Status') }}
        {{ Form::select('status', [1 => 'Enabled', 0 => 'Disabled'], old('status'), ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::submit('Save', ['class' => 'btn']) }}
    </div>
    {{ Form::close() }}
</div>
