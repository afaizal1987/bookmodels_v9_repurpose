@extends('agency.layout.auth')

<!-- Main Content -->
@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
		<div id="loginbox">
      <h1>RESET PASSWORD</h1>

            <!-- <div class="error">Incorrect login details! Try again.</div> -->
						@if (session('status'))
								<div class="alert alert-success">
										{{ session('status') }}
								</div>
						@endif
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/agency/password/email') }}">
						{{ csrf_field() }}

						<div class="{{ $errors->has('email') ? ' has-error' : '' }}">
							<input id="formInput" type="email" placeholder="E-mail Address" class="form-control" name="email" value="{{ old('email') }}" autofocus>
							@if ($errors->has('email'))
									<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
									</span>
							@endif
						</div>
            <input class="button pink" type="submit" value="RESET" />
          </form>
    </div>
</div>
@endsection
