@extends('agency.layout.auth')

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
		<div id="loginbox">
      <h1>RESET PASSWORD</h1>

            <!-- <div class="error">Incorrect login details! Try again.</div> -->
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/agency/password/reset') }}">
					{{ csrf_field() }}
					<input type="hidden" name="token" value="{{ $token }}">

						<!-- Email -->
						<div class="{{ $errors->has('email') ? ' has-error' : '' }}">
								<div>
										<input id="formInput" type="email" placeholder="E-mail Address" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus>

										@if ($errors->has('email'))
												<span class="help-block">
														<strong>{{ $errors->first('email') }}</strong>
												</span>
										@endif
								</div>
						</div>

            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
							<input id="formInput" type="password" placeholder="Password" class="form-control" name="password">
							@if ($errors->has('password'))
									<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
									</span>
							@endif
						</div>

            <div class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<div>
              <input id="formInput" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
							</div>
            </div>


            <input class="button pink" type="submit" value="RESET" />
          </form>

					<!--/ Bookmodels and Original /-->

    </div>
</div>
@endsection
