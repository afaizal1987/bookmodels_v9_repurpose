@extends('agency.layout.auth2')

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<!-- Version 3-->
<div id="content">
		<div id="loginbox">
      <!-- Error message -->
    @if (session('error'))
      <div class="alert alert-danger">
        {{ session('error') }}
      </div>
    @endif

		<h1>CREATE AGENCY PROFILE</h1>
		<hr/>
    <form id='signupform' role="form" action="{{ url('/agency/register') }}" method="POST" name="signupform">
    {{ csrf_field() }}
		<h3 style="text-align:center">AGENCY PROFILE</h3>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="fluid-label">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address (Username)" required>
          </div>
          <div class="erroremail register-error-block"></div>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          <!-- </div> -->
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

          <!-- <div class="col-md-6"> -->
          <div class="fluid-label">
              <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
          </div>
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          <!-- </div> -->
      </div>

      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
          <!-- <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label> -->

          <!-- <div class="col-md-6"> -->
          <div class="fluid-label">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder='Confirm Password' required>
          </div>
              @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
          <!-- </div> -->
      </div>

      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <!-- <label for="name" class="col-md-4 control-label">Agency Name</label> -->

          <!-- <div class="col-md-6"> -->
          <div class="fluid-label">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="bookmodels.asia/agency/yourname" autofocus required>
              <span id="usercheck" class="help-block"></span>
          </div>
              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
          <!-- </div> -->
      </div>
		<h3 style="text-align:center">AGENCY DETAILS</h3>
    <div class="form-group{{ $errors->has('business_type') ? ' has-error' : '' }}">
      <!-- Model Agency / Event Management / Advertising Agency, Photographer / Fashion Designer / Others -->
        <select class='form-control form-control2' name='business_type' required>
              <option value=" "> -- Select Business Type -- </option>
              <option id="model_agency" value="model_agency">Model Agency</option>
              <option id="event" value="event">Event Management</option>
              <option id="advertising" value="advertising">Advertising Agency</option>
              <option id="photographer" value="photographer">Photographer</option>
              <option id="fashion" value="fashion">Fashion Designer</option>
              <option id="others" value="others">Others</option>
            </select>
        <!-- </div> -->
					@if ($errors->has('business_type'))
							<span class="help-block">
									<strong>{{ $errors->first('business_type') }}</strong>
							</span>
					@endif
    </div>

    <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
        <!-- <label for="name" class="col-md-4 control-label">Company Name</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="agency_name" type="text" class="form-control" name="agency_name" value="{{ old('agency_name') }}" placeholder='Agency Name' required>
        </div>
            @if ($errors->has('agency_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('agency_name') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('founding') ? ' has-error' : '' }}">
        <!-- <label for="street_name" class="col-md-4 control-label">Street Name</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="founding" type="number" class="form-control" name="founding" value="{{ old('founding') }}" placeholder='Founding Year'>
        </div>

            @if ($errors->has('founding'))
                <span class="help-block">
                    <strong>{{ $errors->first('founding') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('reg_type') ? ' has-error' : '' }}">
        <select class='form-control form-control2' name='reg_type' required>
              <option value=" "> -- Select Registration Type -- </option>
              <option id="sole_prop" value="sole_prop">Sole Proprieter</option>
              <option id="sdn_bhd" value="sdn_bhd">Sendirian Berhad (Sdn Bhd)</option>
              <option id="llp" value="llp">LLP</option>
              <option id="bhd" value="bhd">Berhad (Bhd)</option>
              <option id="non_profit" value="non_profit">Non-profit</option>
              <option id="freelancer" value="freelancer">Freelancer</option>
            </select>
        <!-- </div> -->
						@if ($errors->has('reg_type'))
								<span class="help-block">
										<strong>{{ $errors->first('reg_type') }}</strong>
								</span>
						@endif
    </div>

	<div class="form-group{{ $errors->has('reg_no') ? ' has-error' : '' }}">
        <!-- <label for="reg_no" class="col-md-4 control-label">Registration No</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="reg_no" type="text" class="form-control" name="reg_no" value="{{ old('reg_no') }}" placeholder='Registration Number (Optional)'>
        </div>

            @if ($errors->has('reg_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('reg_no') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('street_name') ? ' has-error' : '' }}">
        <!-- <label for="street_name" class="col-md-4 control-label">Street Name</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="street_name" type="street_name" class="form-control" name="street_name" value="{{ old('street_name') }}" placeholder='Street Name'>
        </div>

            @if ($errors->has('street_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('street_name') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="postcode" type="postcode" class="form-control" name="postcode" value="{{ old('postcode') }}"placeholder='Postcode'>
        </div>

            @if ($errors->has('postcode'))
                <span class="help-block">
                    <strong>{{ $errors->first('postcode') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="city" type="city" class="form-control" name="city" placeholder='City' value="{{ old('city') }}">
        </div>

            @if ($errors->has('city'))
                <span class="help-block">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
        <!-- <label for="state" class="col-md-4 control-label">State</label> -->

        <!-- <div class="col-md-6"> -->
        <!--<div class="fluid-label">
            <input id="state" type="state" class="form-control" name="state" placeholder='State' value="{{ old('state') }}" required>
        </div>-->
		<select class='form-control form-control2' name='state' required>
			<option value=" "> -- Select your State -- </option>
            <option id='Johor' value='Johor'>Johor</option>
            <option id='Kedah' value='Kedah'>Kedah</option>
            <option id='Kelantan' value='Kelantan'>Kelantan</option>
            <option id='KualaLumpur' value='Kuala Lumpur'>Kuala Lumpur</option>
            <option id='NegeriSembilan' value='Negeri Sembilan'>Negeri Sembilan</option>
            <option id='Pahang' value='Pahang'>Pahang</option>
            <option id='Perak' value='Perak'>Perak</option>
            <option id='Perlis' value='Perlis'>Perlis</option>
            <option id='PulauPinang' value='Pulau Pinang'>Pulau Pinang</option>
            <option id='Sabah' value='Sabah'>Sabah</option>
            <option id='Sarawak' value='Sarawak'>Sarawak</option>
            <option id='Selangor' value='Selangor'>Selangor</option>
            <option id='Terengganu' value='Terengganu'>Terengganu</option>
        </select>
            @if ($errors->has('state'))
                <span class="help-block">
                    <strong>{{ $errors->first('state') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
        <!-- <label for="country" class="col-md-4 control-label">Country</label> -->

        <!-- <div class="col-md-6"> -->
          <select class='form-control form-control2' name='country' required>
            <!--<option value=" "> -- Select your country -- </option>-->
            <option id='Malaysia' value='Malaysia'>Malaysia</option>
            <!--<option id='Singapore' value='Singapore'>Singapore</option>-->
          </select>
          <!-- </div> -->
					@if ($errors->has('country'))
							<span class="help-block">
									<strong>{{ $errors->first('country') }}</strong>
							</span>
					@endif
    </div>

    <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
        <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="contact_person" type="contact_person" class="form-control" name="contact_person" placeholder='Contact Person' value="{{ old('contact_person') }}">
        </div>
            @if ($errors->has('contact_person'))
                <span class="help-block">
                    <strong>{{ $errors->first('contact_person') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('phone_no') ? ' has-error' : '' }}">
        <!-- <label for="phone_no" class="col-md-4 control-label">Phone Number</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="phone_no" type="phone_no" class="form-control" name="phone_no" placeholder='Phone Number' value="{{ old('phone_no') }}" required>
        </div>
            @if ($errors->has('phone_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone_no') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
        <!-- <label for="phone_no" class="col-md-4 control-label">Phone Number</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="website" type="website" class="form-control" name="website" placeholder='Website' value="{{ old('website') }}"required>
        </div>
            @if ($errors->has('website'))
                <span class="help-block">
                    <strong>{{ $errors->first('website') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
        <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="facebook" type="facebook" class="form-control" name="facebook" value="{{ old('facebook') }}" placeholder='Facebook page'>
          </div>
            @if ($errors->has('facebook'))
                <span class="help-block">
                    <strong>{{ $errors->first('facebook') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
        <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="twitter" type="twitter" class="form-control" name="twitter" value="{{ old('twitter') }}" placeholder='Twitter page'>
          </div>

            @if ($errors->has('twitter'))
                <span class="help-block">
                    <strong>{{ $errors->first('twitter') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

    <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
        <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

        <!-- <div class="col-md-6"> -->
        <div class="fluid-label">
            <input id="instagram" type="instagram" class="form-control" name="instagram" value="{{ old('instagram') }}" placeholder='Instagram page'>
          </div>

            @if ($errors->has('instagram'))
                <span class="help-block">
                    <strong>{{ $errors->first('instagram') }}</strong>
                </span>
            @endif
        <!-- </div> -->
    </div>

		<div class="form-group">
			<label>
				<input id='cb1' type="checkbox" name="terms" value="1" /><span class="terms">I agree to the <a href="/terms">Terms of Use</a><!-- and Privacy policy-->.</span>
			</label>
		</div>

			<!--<div class="form-group">
					<input type="submit" class="button pink" value='REGISTER'/>
			</div>-->

			<div class="form-group">
					<input id='register' type="submit" class="button pink" value='REGISTER' onclick="if(!this.form.cb1.checked){swal('', 'Please accept the Terms of Use', 'warning');return false}"/>
			</div>
    </form>
  </div><!--/ Loginbox-->
</div><!--/ Content-->
@endsection

@section('scripts')
<script type='text/javascript' src="{{ asset('/js/conditionize.js') }}" async></script>

<script src="{{ asset('/js/fluid-labels.js') }}"></script>
<script type='text/javascript'>
  $('.fluid-label').fluidLabel();
</script>
<script type='text/javascript' async>
  $('#datepicker').datepicker('show');
</script>

<script type='text/javascript'>
$(document).ready(function(){
        $("#name").change(function(){
             $("#message").html("<i class='fa fa-spinner' aria-hidden='true'></i>");


        var name=$("#name").val();

          //alert(username);
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
                type:"post",
                 dataType: "json",
                 url :"{{URL::to('agency/yourname') }}",
                 data: {"name": name},
                 success: function(receivedData) {
                   if(receivedData['status']==1){
                      $('#usercheck').parent('div').removeClass('has-error').addClass('has-success');
                   }else{
                     $('#usercheck').parent('div').removeClass('has-success').addClass('has-error');
                   }
                   $('#usercheck').html(receivedData.message);
                }
             });

        });

     });
</script>

<script>
	$('#signupform').submit(function() {
		swal({title:"", text:"CREATING PROFILE", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
	});
</script>
@endsection
