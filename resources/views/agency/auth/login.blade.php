@extends('agency.layout.auth')

@section('title')
<title>BookModels.asia - Agency Login</title>
@endsection

@section('meta')
<title>BookModels.asia - Agency Login</title>
@endsection

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
		<div id="loginbox">

			<h1>AGENCY LOGIN</h1>

            <!-- <div class="error">Incorrect login details! Try again.</div> -->
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/agency/login') }}">
						{{ csrf_field() }}

						<div class="{{ $errors->has('email') ? ' has-error' : '' }}">
							<input id="formInput" type="email" placeholder="E-mail" class="form-control" name="email" value="{{ old('email') }}" autofocus>
							@if ($errors->has('email'))
									<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
									</span>
							@endif
						</div>
						<div class="{{ $errors->has('password') ? ' has-error' : '' }}">
							<input id="formInput" type="password" placeholder="Password" class="form-control" name="password">
							@if ($errors->has('password'))
									<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
									</span>
							@endif
						</div>
                <label><input type="checkbox" id="formBox" name="remember" value="" /><span class="remember">Remember Me</span></label><br/><br/>
                <input class="button pink" type="submit" value="LOGIN" />

				</form>
                <p>NOT AN AGENCY USER YET? <a href="{{ url('/agency/register') }}">CREATE PROFILE</a></p>
                <p>FORGOT PASSWORD? <a href="{{ url('/agency/password/reset') }}">RESET PASSWORD</a></p>
		</div> <!-- End Login Box -->
	</div> <!-- End Content -->
@endsection
