@extends('agency.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Agency Registration</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/agency/register') }}">
                        {{ csrf_field() }}
                        <fieldset>
                          <legend>Agency Login Details</legend>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Agency Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </fieldset>

                      <fieldset>
                        <legend>Agency Details</legend>
                          <!-- This be here the details for agencies -->

                          <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                              <label for="name" class="col-md-4 control-label">Company Name</label>

                              <div class="col-md-6">
                                  <input id="company_name" type="text" class="form-control" name="company_name">

                                  @if ($errors->has('company_name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('company_name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
                              <label for="contact_person" class="col-md-4 control-label">Contact Person</label>

                              <div class="col-md-6">
                                  <input id="contact_person" type="contact_person" class="form-control" name="contact_person">

                                  @if ($errors->has('contact_person'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('contact_person') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('phone_no') ? ' has-error' : '' }}">
                              <label for="phone_no" class="col-md-4 control-label">Phone Number</label>

                              <div class="col-md-6">
                                  <input id="phone_no" type="phone_no" class="form-control" name="phone_no">

                                  @if ($errors->has('phone_no'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('phone_no') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('reg_no') ? ' has-error' : '' }}">
                              <label for="reg_no" class="col-md-4 control-label">Registration No</label>

                              <div class="col-md-6">
                                  <input id="reg_no" type="text" class="form-control" name="reg_no">

                                  @if ($errors->has('reg_no'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('reg_no') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('reg_type') ? ' has-error' : '' }}">
                              <label for="reg_type" class="col-md-4 control-label">Registration Type</label>

                              <div class="col-md-6">
                                  <!-- <input id="reg_type" type="password" class="form-control" name="password"> -->

                                  <!-- @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif -->

                                  <select class='form-control' name='reg_type'>
                                    <option> -- Select One -- </option>
                                    <option id="sole_prop" value="sole_prop">Sole Proprieter</option>
                                    <option id="sdn_bhd" value="sdn_bhd">Sendirian Berhad (Sdn Bhd)</option>
                                    <option id="llp" value="llp">LLP</option>
                                    <option id="bhd" value="bhd">Berhad (Bhd)</option>
                                    <option id="non_profit" value="non_profit">Non-profit</option>
                                  </select>
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('street_name') ? ' has-error' : '' }}">
                              <label for="street_name" class="col-md-4 control-label">Street Name</label>

                              <div class="col-md-6">
                                  <input id="street_name" type="street_name" class="form-control" name="street_name">

                                  @if ($errors->has('street_name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('street_name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                              <label for="postcode" class="col-md-4 control-label">Postcode</label>

                              <div class="col-md-6">
                                  <input id="postcode" type="postcode" class="form-control" name="postcode">

                                  @if ($errors->has('postcode'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('postcode') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                              <label for="state" class="col-md-4 control-label">State</label>

                              <div class="col-md-6">
                                  <input id="state" type="state" class="form-control" name="state">

                                  @if ($errors->has('state'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('state') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                              <label for="country" class="col-md-4 control-label">Country</label>

                              <div class="col-md-6">
                                <select class='form-control' name='country'>
                                  <option id='Malaysia' value='Malaysia'>Malaysia</option>
                                  <option id='Singapore' value='Singapore'>Singapore</option>
                                </select>


                              </div>
                          </div>

                        </fieldset>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
