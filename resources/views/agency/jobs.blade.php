@extends('agency.layout.auth')


@section('content')
<div class="container">
    <div class="row">
        <!-- First part -->
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in Agency Profile!
                </div>
            </div>
        </div>

        <?php if (!empty($allJobs)): ?>
          <!-- Got Job -->
          <?php foreach ($allJobs as $key => $value): ?>
              <div class="col-md-8 col-md-offset-2">
                  <div class="panel panel-default">
                    <!-- Later add completion -->
                    <div class='panel-heading'> Job #{{$increment++}}</div>
                      <div class="panel-body">
                        Date of Job: {{$value->job_date}} <br/>
                        Start Time: {{$value->start_time}}  <br/>
                        End Time: {{$value->end_time}} <br/>
                        Address: {{$value->address}} <br/>
                        Payment: {{$value->payment}}  <br/>
                        Payment Terms: {{$value->payment_terms}} <br/>
                        Gender Required: {{$value->gender}} <br/>
                        Ethnicity: {{$value->ethnicity}}  <br/>
                      </div>
                  </div>
              </div>
            <?php endforeach; ?>

        <?php else:?>
          <!-- No Job -->
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-body">
                      <h3>No Jobs available yet</h3>
                  </div>
              </div>
          </div>
        <?php endif; ?>

    </div>
</div>
@endsection
