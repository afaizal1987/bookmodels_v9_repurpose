@extends('agency.layout.auth')

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
		<div id="loginbox">

			<h1>UPLOAD YOUR PROFILE IMAGE</h1>

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
            @endif

						<div>

							<br/>
							<input  class="foo2" type="file" id="upload" value="UPDATE PROFILE IMAGE"/>
						</div>

						<!-- Trying a new style-->
						<div class="form-group" style='text-align:center'>
								<div id="logo">
								</div>
								<img id="upload-demo" src='/uploads/avatars/{{$profilesImg}}' width="400" height="400" alt="Profile Image" class="logo img-responsive center-block">
						</div>
						<div>
							Use slider to Resize/Reposition the image
							<br/>
							<br/>
							<input class="buttonlink pink upload-result" type="submit" value="SAVE PROFILE IMAGE"/>
							<input type="" class="button grey" value='CANCEL'onClick="window.location = '/agency/home';"/>
						</div>



		</div> <!-- End Login Box -->
	</div> <!-- End Content -->
	<!--<div id="divLoading"></div>-->


  <!-- THANK YOU FOR REGISTRING WITH US! YOUR PROFILE IS IN THE WELCOMING EMAIL -->

@endsection

@section('scripts')
<script type="text/javascript">

$uploadCrop = $('#upload-demo').croppie({

    enableExif: true,
		viewport: {
			width: 300,
			height: 300,
		},
		boundary: {
			width: 300,
			height: 300
		}
});


$('#upload').on('change', function () {

	var reader = new FileReader();
	reader.onload = function (e) {
		$uploadCrop.croppie('bind', {
			url: e.target.result
		}).then(function(){
			console.log('jQuery bind complete');
		});
	}
	reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
	$("div#divLoading").addClass('show');
	var delay = 1000;
	$.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
	$uploadCrop.croppie('result', {

		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {


		swal({title:"", text:"UPDATING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
		 $('.overlay').css("visibility", "visible");
		$.ajax({
			url: "/agency/image_uploader",
			paramName: "images",
			type: "PATCH",
			data: {"image":resp},
			success: function (response) {
				swal({
					title: "Profile Image Updated!",
					type: "success",
					closeOnConfirm: false
				},function(){
					setTimeout(function(){ window.location = "/agency/"+response.data; }, delay);
				});

			}
		});
	});
});
</script>



@endsection
