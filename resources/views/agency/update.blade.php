@extends('agency.layout.auth')

@section('title')
<title>Update Profile - <?php $test = (Auth::guard('agency')->user()->name); echo ucwords($test); ?></title>
@endsection

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
		<div id="loginbox">
			<h1>UPDATE PROFILE</h1>
			<hr/>
    <form id='signupform' class="form" role="form" method="POST" action="{{ url('/agency/updateprofile') }}">
      {{ csrf_field() }}
      {{ method_field('PATCH') }}

          <div class="form-group{{ $errors->has('business_type') ? ' has-error' : '' }}">
      <!-- Model Agency / Event Management / Advertising Agency, Photographer / Fashion Designer / Others -->

        <select class='form-control form-control2' name='business_type' required>
              <option value=" "> -- Select Business Type -- </option>
              <option id="model_agency" value="model_agency" <?php if (!empty($agentProfile->business_type) && $agentProfile->business_type == 'model_agency')  echo 'selected'; ?>>Model Agency</option>
              <option id="event" value="event" <?php if (!empty($agentProfile->business_type) && $agentProfile->business_type == 'event')  echo 'selected'; ?>>Event Management</option>
              <option id="advertising" value="advertising" <?php if (!empty($agentProfile->business_type) && $agentProfile->business_type == 'advertising')  echo 'selected'; ?>>Advertising Agency</option>
              <option id="photographer" value="photographer" <?php if (!empty($agentProfile->business_type) && $agentProfile->business_type == 'photographer')  echo 'selected'; ?>>Photographer</option>
              <option id="fashion" value="fashion" <?php if (!empty($agentProfile->business_type) && $agentProfile->business_type == 'fashion')  echo 'selected'; ?>>Fashion Designer</option>
              <option id="others" value="others" <?php if (!empty($agentProfile->business_type) && $agentProfile->business_type == 'others')  echo 'selected'; ?>>Others</option>
            </select>
        <!-- </div> -->
					@if ($errors->has('business_type'))
							<span class="help-block">
									<strong>{{ $errors->first('business_type') }}</strong>
							</span>
					@endif
    </div>

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		          <!-- <label for="name" class="col-md-4 control-label">Agency Name</label> -->

		          <!-- <div class="col-md-6"> -->
		          <div class="fluid-label">
		              <input id="name" type="text" class="form-control" name="name" value="{{$agentProfile->name}}" placeholder="bookmodels.asia/agency/yourname" autofocus readonly>
		              <span id="usercheck" class="help-block"></span>
		          </div>
		              @if ($errors->has('name'))
		                  <span class="help-block">
		                      <strong>{{ $errors->first('name') }}</strong>
		                  </span>
		              @endif
		          <!-- </div> -->
		      </div>

					<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
			        <!-- <label for="name" class="col-md-4 control-label">Company Name</label> -->

			        <!-- <div class="col-md-6"> -->
			        <div class="fluid-label">
			            <input id="agency_name" type="text" class="form-control" name="agency_name" value="{{$agentProfile->company_name}}" placeholder='Agency Name' required>
			        </div>
			            @if ($errors->has('agency_name'))
			                <span class="help-block">
			                    <strong>{{ $errors->first('agency_name') }}</strong>
			                </span>
			            @endif
			        <!-- </div> -->
			    </div>

					<div class="form-group{{ $errors->has('founding') ? ' has-error' : '' }}">
              <!-- <label for="street_name" class="col-md-4 control-label">Street Name</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="founding" type="founding" class="form-control" name="founding" value='{{$agentProfile->founding}}' placeholder='Founding Year'>
              </div>

                  @if ($errors->has('founding'))
                      <span class="help-block">
                          <strong>{{ $errors->first('founding') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('reg_no') ? ' has-error' : '' }}">
              <!-- <label for="reg_no" class="col-md-4 control-label">Registration No</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="reg_no" type="text" class="form-control" name="reg_no" value='{{$agentProfile->reg_no}}' placeholder='Registration Number'>
              </div>

                  @if ($errors->has('reg_no'))
                      <span class="help-block">
                          <strong>{{ $errors->first('reg_no') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

            <div class="form-group{{ $errors->has('reg_type') ? ' has-error' : '' }}">
        <select class='form-control form-control2' name='reg_type' required>
              <option value=" "> -- Select Registration Type -- </option>
              <option id="sole_prop" value="sole_prop" <?php if (!empty($agentProfile->reg_type) && $agentProfile->reg_type == 'sole_prop')  echo 'selected'; ?>>Sole Proprieter</option>
              <option id="sdn_bhd" value="sdn_bhd"<?php if (!empty($agentProfile->reg_type) && $agentProfile->reg_type == 'sdn_bhd')  echo 'selected'; ?>>Sendirian Berhad (Sdn Bhd)</option>
              <option id="llp" value="llp"<?php if (!empty($agentProfile->reg_type) && $agentProfile->reg_type == 'llp')  echo 'selected'; ?>>LLP</option>
              <option id="bhd" value="bhd"<?php if (!empty($agentProfile->reg_type) && $agentProfile->reg_type == 'bhd')  echo 'selected'; ?>>Berhad (Bhd)</option>
              <option id="non_profit" value="non_profit"<?php if (!empty($agentProfile->reg_type) && $agentProfile->reg_type == 'non_profit')  echo 'selected'; ?>>Non-profit</option>
              <option id="freelancer" value="freelancer"<?php if (!empty($agentProfile->reg_type) && $agentProfile->reg_type == 'freelancer')  echo 'selected'; ?>>Freelancer</option>
            </select>
        <!-- </div> -->
						@if ($errors->has('reg_type'))
								<span class="help-block">
										<strong>{{ $errors->first('reg_type') }}</strong>
								</span>
						@endif
    </div>

          <div class="form-group{{ $errors->has('street_name') ? ' has-error' : '' }}">
              <!-- <label for="street_name" class="col-md-4 control-label">Street Name</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="street_name" type="street_name" class="form-control" name="street_name" value='{{$agentProfile->street_name}}' placeholder='Street Name'>
              </div>

                  @if ($errors->has('street_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('street_name') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
              <!-- <label for="postcode" class="col-md-4 control-label">Postcode</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="postcode" type="postcode" class="form-control" name="postcode" value='{{$agentProfile->postcode}}' placeholder='Postcode'>
              </div>

                  @if ($errors->has('postcode'))
                      <span class="help-block">
                          <strong>{{ $errors->first('postcode') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">

              <div class="fluid-label">
                  <input id="city" type="city" class="form-control" name="city" value='{{$agentProfile->city}}' placeholder='City'>
              </div>
                  @if ($errors->has('city'))
                      <span class="help-block">
                          <strong>{{ $errors->first('city') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
              <!-- <label for="state" class="col-md-4 control-label">State</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="state" type="state" class="form-control" name="state" value='{{$agentProfile->state}}' placeholder='State'>
              </div>
                  @if ($errors->has('state'))
                      <span class="help-block">
                          <strong>{{ $errors->first('state') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
        <!-- <label for="country" class="col-md-4 control-label">Country</label> -->

        <!-- <div class="col-md-6"> -->
          <select class='form-control form-control2' name='country' required>
            <option value=" "> -- Select your country -- </option>
            <option id='Malaysia' value='Malaysia' <?php if (!empty($agentProfile->country) && $agentProfile->country == 'Malaysia')  echo 'selected'; ?>>Malaysia</option>
            <option id='Singapore' value='Singapore' <?php if (!empty($agentProfile->country) && $agentProfile->country == 'Singapore')  echo 'selected'; ?>>Singapore</option>
          </select>
          <!-- </div> -->
					@if ($errors->has('country'))
							<span class="help-block">
									<strong>{{ $errors->first('country') }}</strong>
							</span>
					@endif
    </div>

          <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
              <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="contact_person" type="contact_person" class="form-control" name="contact_person" value='{{$agentProfile->contact_person}}' placeholder='Contact Person'>
              </div>
                  @if ($errors->has('contact_person'))
                      <span class="help-block">
                          <strong>{{ $errors->first('contact_person') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('phone_no') ? ' has-error' : '' }}">
              <!-- <label for="phone_no" class="col-md-4 control-label">Phone Number</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="phone_no" type="phone_no" class="form-control" name="phone_no" value='{{$agentProfile->phone_no}}' placeholder='Phone Number' required>
              </div>
                  @if ($errors->has('phone_no'))
                      <span class="help-block">
                          <strong>{{ $errors->first('phone_no') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
              <!-- <label for="phone_no" class="col-md-4 control-label">Phone Number</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="website" type="website" class="form-control" name="website" value='{{$agentProfile->website}}' placeholder='Website' >
              </div>
                  @if ($errors->has('website'))
                      <span class="help-block">
                          <strong>{{ $errors->first('website') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
              <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="facebook" type="facebook" class="form-control" name="facebook" value='{{$agentProfile->facebook}}' placeholder='Facebook page'>
                </div>
                  @if ($errors->has('facebook'))
                      <span class="help-block">
                          <strong>{{ $errors->first('facebook') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
              <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="twitter" type="twitter" class="form-control" name="twitter" value='{{$agentProfile->twitter}}' placeholder='Twitter page'>
                </div>

                  @if ($errors->has('twitter'))
                      <span class="help-block">
                          <strong>{{ $errors->first('twitter') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>

          <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
              <!-- <label for="contact_person" class="col-md-4 control-label">Contact Person</label> -->

              <!-- <div class="col-md-6"> -->
              <div class="fluid-label">
                  <input id="instagram" type="instagram" class="form-control" name="instagram" value='{{$agentProfile->instagram}}' placeholder='Instagram page'>
                </div>

                  @if ($errors->has('instagram'))
                      <span class="help-block">
                          <strong>{{ $errors->first('instagram') }}</strong>
                      </span>
                  @endif
              <!-- </div> -->
          </div>


        <div class="form-group">
            <!-- <div class="col-md-6 col-md-offset-4"> -->
                <button type="submit" class="button pink"> UPDATE PROFILE </button>
				<input type="" class="button grey" value='CANCEL'onClick="window.location = '/agency/home';"/>
            <!-- </div> -->
        </div>
    </form>
  </div> <!-- End Profile -->
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/fluid-labels.js') }}"></script>
<script type='text/javascript' async>
  $('.fluid-label').fluidLabel();
</script>

<script>
	$('#signupform').submit(function() {
		swal({title:"", text:"UPDATING PROFILE", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
	});
</script>
@endsection
