@extends('agency.layout.auth')

@section('title')
<title>Job Applications - <?php $test = (Auth::guard('agency')->user()->name); echo ucwords($test); ?></title>
@endsection

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
  <div id="content">

    <div id="jobs">
      <h1>Job Applications</h1>

      @if (Session::has('message'))
          <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{ Session::get('message') }}
          </div>
      @endif

      <h3><a href="/agency/job_listing">Back &raquo;</a> 	@if ($job_applications_applied->count() > 0)
																{{ $job_applications_applied[0]['job']->title }} New Talents
															@else
																0 New Talents
															@endif
														</h3>

		<div id="tabbing">
			<ul class="nav nav-tabs" role="tablist">
				  <li role="presentation" class='active'><a href="#view" aria-controls="view" role="tab" data-toggle="tab">Models Applied ({{$number}})</a></li>
				  <li role="presentation"><a href="#applied" aria-controls="applied" role="tab" data-toggle="tab">Models Booked ({{$number2}})</a></li>
			</ul>
		</div>

		<div class="tab-content">

			<!-- FIRST TAB -->
			<div role="tabpanel" class="tab-pane active" id="view">
				@if ($job_applications_applied->count() > 0)
				@foreach ($job_applications_applied as $job_application)

				<?php
				$birthdate = new DateTime($job_application->profile->birthday);
				$today = new DateTime('today');
				$age = $birthdate->diff($today)->y;

				$nickname = str_replace('.',' ',$job_application->profile->nickname);
				$nickname = ucwords($nickname);
				?>
				<div class="job-box">
					<a href="/{{ $nickname }}"><img class="profileimg" src='/uploads/avatars/{{$job_application->profile->avatar}}' alt="Profile Image"/></a>
					<div class="description">
					  <strong><a href="/{{ $job_application->profile->nickname }}"> <!--<?php $string = (strlen($job_application->profile->fullname) > 20) ? substr($job_application->profile->fullname,0,20).'...' : $job_application->profile->fullname;?>-->
								{{ $nickname }}</a></strong><br/>
					 {{$age}} years old, {{ $job_application->profile->location }}<br/>


					@if($job_application['status']!=2)
					<input id="confirm" class="buttonlink pink" name='submission'
					  data-job="{{$job_application['job_id']}}" data-profile="{{$job_application->profile->profile_id}}"
					  data-value="CONFIRM" type="submit" value="CONFIRM" />
					@else
					  <!--<input class="buttonlink pink" type="submit" value="MODEL BOOKED" />-->
					@endif
					<input id="confirm" class="buttonlink grey" name='submission'
					  data-job="{{$job_application['job_id']}}" data-profile="{{$job_application->profile->profile_id}}"
					  data-value="CANCEL" type="submit" value="CANCEL" />
					</div> <!-- End Description -->
					</div> <!-- End Booking Box -->
				@endforeach
				@else
				<div class="job-box">
					<h3>No Applicants yet</h3>
				</div> <!-- End Booking Box -->
				@endif
			</div>
			
			<!-- SECOND TAB -->
			<div role="tabpanel" class="tab-pane" id="applied">
				@if ($job_applications_booked->count() > 0)
				@foreach ($job_applications_booked as $job_application)

				<?php
				$birthdate = new DateTime($job_application->profile->birthday);
				$today = new DateTime('today');
				$age = $birthdate->diff($today)->y;

				$nickname = str_replace(' ','.',$job_application->profile->nickname);
				$nickname = ucwords($nickname);
				?>
				<div class="job-box">
					<a href="/{{ $nickname }}"><img class="profileimg" src='/uploads/avatars/{{$job_application->profile->avatar}}' alt="Profile Image"/></a>
					<div class="description">
					  <strong><a href="/{{ $job_application->profile->nickname }}"> <!--<?php $string = (strlen($job_application->profile->fullname) > 20) ? substr($job_application->profile->fullname,0,20).'...' : $job_application->profile->fullname;?>-->
								{{ $nickname }}</a></strong><br/>
					 {{$age}} years old | {{ $job_application->profile->location }}<br/>
					 {{$job_application->profile->workemail}} | {{ $job_application->profile->phone_number }}<br/>

					@if($job_application['status']!=2)
					<input id="confirm" class="buttonlink pink" name='submission'
					  data-job="{{$job_application['job_id']}}" data-profile="{{$job_application->profile->profile_id}}"
					  data-value="CONFIRM" type="submit" value="CONFIRM" />
					@else
					  <!--<input class="buttonlink pink" type="submit" value="MODEL BOOKED" />-->
					@endif

					<?php $mytime = \Carbon\Carbon::now();
								$today =  $mytime->toDateString();
								$main = date( 'Y-m-d H:i:s', strtotime( $today ) );

								$secondary = date( 'Y-m-d H:i:s', strtotime($job_application->job->start_date .' -1 day') );
					?>
					@if(($secondary)>$main)
						<input id="confirm" class="buttonlink grey" name='submission'
						  data-job="{{$job_application['job_id']}}" data-profile="{{$job_application->profile->profile_id}}"
						  data-value="REMOVE" type="submit" value="REMOVE" />
					@endif
					</div> <!-- End Description -->
					</div> <!-- End Booking Box -->
				@endforeach
				@else
				<div class="job-box">
					<h3>No Applicants yet</h3>
				</div> <!-- End Booking Box -->
				@endif
			</div>
		</div>



        <br clear="all" />
 		   <!-- <div id="pagination"><a href="#">PREV</a><a href="#">1</a><a href="#">2</a><a href="#">...</a><a href="#">9</a><a href="#">NEXT</a></div> -->

    </div> <!-- End Jobs Booking -->

</div> <!-- End Content -->

        <!-- <div id="overlay">
        <p>The image is uploading, be patient...</p><br/><br/>
        <img src="images/loading_white.gif" width="32" height="32" alt="loading">
        </div> -->


@endsection

@section('scripts')
<script type='text/javascript' async>
$('input#confirm').on('click',function(){
    var job_id = $(this).data('job');
    var user_id = $(this).data('profile');
    var submission = $(this).data('value');
	if(submission =="CONFIRM"){
		submission1 = "Book this Model?";
	}

	if(submission =="REMOVE"){
		submission1 = "Remove this Model?";
	}

	if(submission =="CANCEL"){
		submission1 = "Cancel this Application?";
	}

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    swal({
      title: submission1,
      // text: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },function(){
      swal({title:"", text:"UPDATING", imageUrl: "/css/images/27.gif", showConfirmButton:false, allowOutsideClick:false});
      $.ajax({
        type: "post",
        url: "{{url('/agency/jobconfirm')}}",
        data: {job_id:job_id, user_id:user_id, submission:submission},
        success: function (response) {
          if(response['confirm'] == 1){
            // location.reload ();
            swal({
              title: "Model BOOKED",
              type: "success",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.reload();
            });
          }else if(response['cancel'] == 1){
            // location.reload ();
            swal({
              title: "Application Cancelled",
              type: "success",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.href = "http://bookmodels.asia/agency/job_listing"
            });
          }else if(response['remove'] == 1){
            // location.reload ();
            swal({
              title: "Application Removed",
              type: "success",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.href = "http://bookmodels.asia/agency/job_listing"
            });
          }
        }
      });
    });
});
</script>
@endsection
