<ul class="navigation">
  @if (Auth::guard('agency')->guest())
	  <li class="profile"> DASHBOARD</li>
      <li><a href="{{ url('/login') }}"></i>Guest</a></li>
      <li><a href="{{ url('/login') }}"><i class="fa fa-user"></i>Login</a></li>
      <li><a href="{{ url('/register') }}"><i class="fa fa-user"></i>Register</a></li>
      <!--<li><a href="{{ url('/agency/register') }}">Register</a></li>-->
  @else
	<li class="profile"> WELCOME, <?php $test = (Auth::guard('agency')->user()->name); echo strtoupper($test); ?></li>
    <li class="profile"><a href="/agency/home"><i class="fa fa-user"></i> My Profile</a></li>
    <li class="profile"><a href="/agency/myphotos"><i class="fa fa-file-text-o"></i> My Photos</a></li>
	<!-- <li class="profile"><a href="/agency/applications"><i class="fa fa-file-text-o"></i> Job Application</a></li> -->
    <li class="profile"><a href="/agency/job_listing"><i class="fa fa-file-text-o"></i> Job Listing</a></li>
    <li class="profile"><a href="/agency/history"><i class="fa fa-file-text-o"></i> Job History</a></li>
    
    <li class="profile">
          <a href="{{ url('/agency/logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
            Logout
        </a>

        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
    @endif
    <li><a href="/index.php"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="/about"><i class="fa fa-file-text-o"></i> About BookModels.asia</a></li>
    <li><a href="/agency"><i class="fa fa-file-text-o"></i> Plans & Pricing</a></li>
    <li><a href="/terms"><i class="fa fa-file-text-o"></i> Terms of Use</a></li>
    <!--<li><a href="/privacy"><i class="fa fa-file-text-o"></i> Privacy Policy</a></li>-->
    <!--<li><a href="/help"><i class="fa fa-file-text-o"></i> Help Centre</a></li>-->
    <li><a href="/contactus"><i class="fa fa-phone-square"></i> Contact Us</a></li>
</ul>
