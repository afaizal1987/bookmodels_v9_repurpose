<!DOCTYPE html>
<html>
<head>

@yield('title')

@yield('meta')
<meta http-equiv="content-type" content="text/html; charset=utf-8">


<meta name="description" content="">
<meta charset="utf-8">

<meta property="og:site_name" content="Bookmodels.asia"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/column_normaliser.css">
<link rel="stylesheet" href="/css/fluid-labels.css">
<link rel="stylesheet" href="/css/font-awesome.css">
<link rel="stylesheet" href="/css/lightbox.min.css">
<link rel="stylesheet" type="text/css" href="/css/sweetalert.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="/js/exif.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3190436-84', 'auto');
  ga('send', 'pageview');

</script>

<!--[if lt IE 9]>
  <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!--
<div id="wrapper">
-->
	<?php //include('sidemenu.php'); ?>
	<div class="site-wrap"><!-- for slide menu -->

	<header class="clearfix">
		<div class="header-inner">
		<div id="logo"><a href="/index.php"><img src="/images/BookModels_Logo.png" class="logo"></a></div>
				@if (Auth::guest())
        <div id="headerlogin"><a href=" /login" class="button">LOGIN</a></div>
				@else
				<div style="margin: 13px 0 10px 0;"></div>
				@endif
        <!--<div id="search"><input type="text" id="searchField" placeholder="Search WebTVasia" name="search" value="" /></div>-->
    </div>
	</header>
				@yield('content')

  <footer>
		<a href="http://facebook.com/bookmodelsasia" target="_blank">
			<img src="/images/ico_fb.png" width="57" height="56" alt="Facebook" /></a>
		<a href="http://twitter.com/bookmodelsasia" target="_blank">
			<img src="/images/ico_tw.png" width="57" height="56" alt="Twitter" /></a>
		<a href="http://instagram.com/bookmodelsasia" target="_blank">
			<img src="/images/ico_ig.png" width="57" height="56" alt="Instagram" /></a>
		<a href="http://www.youtube.com/channel/UCNkRGbulbxwlIupLnZShVhg" target="_blank">
			<img src="/images/ico_yt.png" width="57" height="56" alt="YouTube" /></a>
	<br/><br/>

	&copy; Copyright 2017 BookModels.asia. All rights reserved.<br/>
	<a href="/terms">Terms of Use</a> |
	<!-- <a href="/privacy">Privacy Policy</a> |-->
	<a href="/contactus">Contact</a><br/>
	</footer>


	<script src="/js/app.js" async></script>
	<script src="/js/sweetalert.min.js"></script>
	@yield('scripts')

	</div><!-- /.site_wrap -->

	</body>
	</html>
