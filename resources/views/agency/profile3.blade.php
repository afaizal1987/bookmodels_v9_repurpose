@extends('agency.layout.auth2')

@section('title')
@if (Auth::guard('agency')->guest())
	<title><?php echo ucwords($agentProfile->name); ?> - BookModels.asia</title>
@else
	<title><?php echo ucwords($agentProfile->name); ?> - BookModels.asia</title>
@endif
@endsection

@section('meta')
	<meta property="og:title" content="BookModels.asia - <?php echo ucwords($agentProfile->name); ?>"/>
	<meta property="og:url" content="https://bookmodels.asia/a/{{$agentProfile->name}}"/>
  <meta property="og:image" content="https://bookmodels.asia/uploads/avatars/{{$agentProfile->avatar}}"/>
	<meta property="og:type" content="website"/>
	<meta property="fb:app_id" content="1622986338008242"/>
	@if (Auth::guest())
  <meta name="keywords" content="Model, talent, booking, agency, photographer, fashion designer">
  <meta name="description" content="BookModels.asia is a fresh new platform to effectively connect Malaysian models and aspiring talents directly to verified agencies or photographers.">
	@endif

	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta charset="utf-8">

@stop

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">
 <div id="profile">
   @if ($message = Session::get('status'))
   <div class="alert alert-success alert-block">
     <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
   <div class="profileimg-box">
            <img class="profileimg" src="/uploads/avatars/{{$agentProfile->avatar}}" alt="Profile Image"/><br/>
            <!-- <form action="{{ url('/agency/img_upload') }}" enctype="multipart/form-data" id='form' method="POST">
              {{ csrf_field() }}
              {{ method_field('PATCH') }}
                <input class="foo" accept="image/*" type="file" name="image" style='width:100%' onchange="this.form.submit()"/>

            </form> -->
            </div>
            <div class="profile-box">
              <h2>{{$agentProfile->company_name}}
								@if($agentProfile->verification=='No')
                </h2>
                @else
                <img src="/images/verified.png" width="24" height="23" alt="Verified"></h2>
                @endif
                Since {{$agentProfile->founding}}, {{$agentProfile->business_type}}

                <br/><br/>
                Address	:<br/>{{$agentProfile->office_address}}.<br/>
                <!-- Public Profile: <a href='./{{$linkname}}'>{{$agentProfile->company_name}}</a> -->
                <br/><br/>

                <!-- <strong>Agency Rating - 80% (24 reviews)</strong><br/> -->

			</div> <!-- End Profile Box -->
            <div class="agencymeta-box">
				<h3>Contact Information</h3>

				<span class="meta">E-mail</span>: {{$agentProfile->email}}<br/>
				<span class="meta">Contact</span>: {{$agentProfile->contact_person}}<br/>
				<span class="meta">Phone</span>: {{$agentProfile->phone_no}}<br/>
        <span class="meta">Website</span>: @if(!empty($agentProfile->website))
                                              <a href="{{$agentProfile->website}}" target="_blank">{{$agentProfile->website}}</a>
                                            @else
                                              None
                                            @endif
        <br/><br/>


        @if(!empty($agentProfile->facebook))
          <a href="{{$agentProfile->facebook}}" target="_blank"><img src="/images/ico_fb.png" width="48" height="47" alt="Facebook" /></a>
        @endif
        @if(!empty($agentProfile->twitter))
          <a href="{{$agentProfile->twitter}}" target="_blank"><img src="/images/ico_tw.png" width="48" height="47" alt="Twitter" /></a>
        @endif
        @if(!empty($agentProfile->instagram))
          <a href="{{$agentProfile->instagram}}" target="_blank"><img src="/images/ico_ig.png" width="48" height="47" alt="Instagram" /></a>
        @endif

  </div> <!-- End Profile Box -->

        <br clear="all" />
        @if($counter > 1)
        <h2>My Photos</h2>
        @endif

        @foreach ($arrayTest as $key=>$value)
        <div class="photo-box">
          <a href='/uploads/thumbnails/{{$value}}' data-lightbox="agency_photos">
            <img class="photo" src="/uploads/model_photos/{{$value}}" alt="Images" /></a>
        </div>
        @endforeach
            <br clear="all" /><br/>
  </div> <!-- End Profile -->
</div>
@endsection

@section('scripts')
<script>
    lightbox.option({
      'alwaysShowNavOnTouchDevices':true,
      'resizeDuration': 200,
      'maxHeight':600,
      'wrapAround': true,
      'positionFromTop': 100
    })
</script>

@endsection
