@extends('agency.layout.auth')

@section('content')
<div id="content">

            <div id="profile">
            <div class="profileimg-box">
            <a href="profile-model-avatar.php"><img class="profileimg" alt="Profile Image"/></a><br/>
            <input class="buttonlink2 pink" type="submit" value="UPDATE PHOTO" />
            </div>
            <div class="profile-box">
              <h2>{{$agentProfile->company_name}}</h2>
                Since 2008, Model Agency <span class="active">(verified)</span><br/><br/>
                {{$agentProfile->office_address}}.<br/><br/>

                <strong>Agency Rating - 80% (24 reviews)</strong><br/>
                <input class="buttonlink pink" type="submit" value="EDIT PROFILE" />
                <!-- <input class="buttonlink pink" type="submit" value="ADD PHOTOS" /> -->
			</div> <!-- End Profile Box -->
            <div class="agencymeta-box">
				<h3>Contact Information</h3>

				<span class="meta">E-mail</span>: {{$agentProfile->email}}<br/>
				<span class="meta">Contact</span>: {{$agentProfile->contact_person}}<br/>
				<span class="meta">Phone</span>: {{$agentProfile->phone_no}}<br/>
				<span class="meta">Website</span>: http://titaniumtalents.my<br/><br/>

                <a href="http://facebook.com/titaniumtalents" target="_blank"><img src="/images/ico_fb.png" width="48" height="47" alt="Facebook" /></a>
                <a href="http://twitter.com/titaniumtalents" target="_blank"><img src="/images/ico_tw.png" width="48" height="47" alt="Twitter" /></a>
                <a href="http://instagram.com/titaniumtalents" target="_blank"><img src="/images/ico_ig.png" width="48" height="47" alt="Instagram" /></a>
			</div> <!-- End Profile Box -->

                <br clear="all" />

                <h2>My Photos</h2>
            	<div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
            	<div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
            	<div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_large.jpg" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="DELETE PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_add.png" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="ADD PHOTO" />
				</div> <!-- End Photo Box -->
                <div class="photo-box">
            	<img class="photo" src="images/photo_add.png" alt="Profile Image"/>
                <input class="buttonlink2 pink" type="submit" value="ADD PHOTO" />
				</div> <!-- End Photo Box -->

                <br clear="all" /><br/>

                <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fwww.pixaworks.pw%2Fprojects%2Fbma%2Fprofile.php&layout=button_count&size=large&mobile_iframe=false&width=84&height=28&appId" width="84" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div> <!-- End Profile -->

	</div> <!-- End Content -->

<div id="overlay">
<p>The image is uploading, be patient...</p><br/><br/>
<img src="images/loading_white.gif" width="32" height="32" alt="loading">
</div>
@endsection
