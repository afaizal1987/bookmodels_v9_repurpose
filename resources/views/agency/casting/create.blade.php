@extends('agency.layout.auth')

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
    <div id="loginbox">

		<div>
		
			@include('agency.casting.form')
			<input class="buttonlink pink" style="height:35px;padding-bottom:5px;" type="submit" value="CREATE CASTING"/>
			{{ Form::close() }}
			<form action="/agency/job_listing">
				<input class="buttonlink grey" style="height:35px;padding-bottom:5px;" type="submit" value="CANCEL"/>
			</form>
		</div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/fluid-labels.js') }}"></script>
<script type='text/javascript'>
  $('.fluid-label').fluidLabel();
</script>
<script>
$(function() {
  $( "#castingpicker" ).datepicker({
    format: 'dd-mm-yyyy',
    startDate: '-1d',
    autoclose: 'true'
  });
});
</script>

<script>
    // initialize input widgets first
    $('#casting .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('#casting .date').datepicker({
        'format': 'dd-mm-yyyy',
		 'startDate': '0d',
        'autoclose': true
    });

    // initialize datepair
    var castingEl = document.getElementById('casting');
    var datepair = new Datepair(castingEl,{
      'defaultTimeDelta': 10800000 // milliseconds
    });
</script>

<script async>
    // initialize input widgets first
    $('#basicExample .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('#basicExample .date').datepicker({
        'format': 'dd-mm-yyyy',
		 'startDate': '0d',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('basicExample');
    var datepair = new Datepair(basicExampleEl,{
      'defaultTimeDelta': 10800000 // milliseconds
    });
</script>


@endsection
