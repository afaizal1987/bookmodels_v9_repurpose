<h2 style="text-align:center">CASTING DETAILS</h2>
			{{ Form::open(['action' => 'JobsController@casting_store', 'method' => 'POST']) }}

			<div class="form-group">
				<div class="fluid-label">
				{{ Form::text('title', old('title'), ['class' => 'form-control','placeholder'=>'Title', 'required'=>'required']) }}</div>
			</div>
			<div class="form-group">
				<div class="fluid-label">
				{{ Form::select('jobtype', ['Runwayshow' => 'Runway Show', 'Photoshoot' => 'Photoshoot', 'Video' => 'Video',
				'Event' => 'Event', 'Others' => 'Others'],old('jobtype'),
				  ['class' => 'form-control2','placeholder'=>'-- Select Job Type --']) }}</div>
			</div>			
			<div id="casting">
				<div class="form-group">
					<div class="fluid-label">
					  {{ Form::text('casting_start_date', old('casting_start_date'), ['class' => 'form-control date start', 'placeholder'=>'Casting Start Date','required'=>'required']) }}
					</div>
				</div>
				<div class="form-group">
					<div class="fluid-label">
					  {{ Form::text('casting_end_date', old('casting_end_date'), ['class' => 'form-control date end', 'placeholder'=>'Casting End Date']) }}
					</div>
				</div>
				<div class="form-group">
					<div class="fluid-label">
					  {{ Form::text('casting_start_time', old('casting_start_time'), ['class' => 'form-control time start','placeholder' => 'Casting Start Time','id' => 'starttime','required'=>'required']) }}
					</div>
				</div>
				<div class="form-group">
					<div class="fluid-label">
					  {{ Form::text('casting_end_time', old('casting_end_time'), ['class' => 'form-control time end', 'placeholder'=>'Casting End Time','id'=>'endtime','required'=>'required']) }}
					</div>
				</div>
				
				<div class="form-group">
				  <div class="fluid-label">
					<!-- {{ Form::label('venue', 'Venue') }} -->
					{{ Form::text('casting_venue', old('venue'), ['class' => 'form-control', 'placeholder'=>'Casting Venue','required'=>'required']) }}
				  </div>
				</div>
				
				<div class="form-group">
				  <div class="fluid-label">
					<!-- {{ Form::label('location', 'Location') }} -->
					{{ Form::text('casting_location', old('location'), ['class' => 'form-control',
					'placeholder'=>'Casting Location (City)','required'=>'required']) }}
				  </div>
				</div>
			</div>
			<br/>
			<h3 style="text-align:center">JOB DETAILS</h3>
			
			<div id="basicExample">
			  <div class="form-group">
				<div class="fluid-label">
				  {{ Form::text('start_date', old('start_date'), ['class' => 'form-control date start', 'placeholder'=>'Start Date','required'=>'required']) }}
				</div>
			  </div>
			  <div class="form-group">
				<div class="fluid-label">
				  {{ Form::text('start_time', old('start_time'), ['class' => 'form-control time start','placeholder' => 'Start Time','id' => 'starttime','required'=>'required']) }}
				</div>
			  </div>
			  <div class="form-group">
				<div class="fluid-label">
				  {{ Form::text('end_date', old('end_date'), ['class' => 'form-control date end', 'placeholder'=>'End Date','required'=>'required']) }}
				</div>
			  </div>
			  <div class="form-group">
				<div class="fluid-label">
				  {{ Form::text('end_time', old('end_time'), ['class' => 'form-control time end', 'placeholder'=>'End Time','id'=>'endtime','required'=>'required']) }}
				</div>
			  </div>
			</div>
			
			<div class="form-group">
			  <div class="fluid-label">
				<!-- {{ Form::label('venue', 'Venue') }} -->
				{{ Form::text('venue', old('venue'), ['class' => 'form-control', 'placeholder'=>'Venue','required'=>'required']) }}
			  </div>
			</div>
			
			<div class="form-group">
			  <div class="fluid-label">
				<!-- {{ Form::label('location', 'Location') }} -->
				{{ Form::text('location', old('location'), ['class' => 'form-control',
				'placeholder'=>'Location (City)','required'=>'required']) }}
			  </div>
			</div>
			<div class="form-group">
			  <div class="fluid-label">
				<!-- {{ Form::label('gender', 'Gender') }} -->
				{{ Form::select('gender', ['Male' => 'Male', 'Female' => 'Female', 'Any' => 'Any'], old('gender'), ['class' => 'form-control2',
				'placeholder'=>'-- Select Gender --']) }}
			  </div>
			</div>
			<div class="form-group">
			  <div class="fluid-label">
				{{ Form::text('payment', old('payment'), ['class' => 'form-control','placeholder'=>'Payment (RM)','required'=>'required']) }}
			  </div>
			</div>
			<div class="form-group">
				<!-- {{ Form::label('payment_terms', 'Payment Terms') }} -->
				<div class="fluid-label">
				{{ Form::select('payment_terms', ['1' => 'On The Spot', '2' => '1-2 weeks', '3' => '2-3 weeks', '4' => 'More than 1 Month'],old('payment_terms'),
				  ['class' => 'form-control2','placeholder'=>'-- Select Payment Terms --']) }}</div>
			</div>
			
			<div class="form-group">
			  <div class="fluid-label">
				<!-- {{ Form::label('age_min', 'Min Age') }} -->
				{{ Form::number('age_min', old('age_min'), ['class' => 'form-control','placeholder'=>'Min Age','required'=>'required']) }}</div>
			</div>
			<div class="form-group">
				<!-- {{ Form::label('age_max', 'Max Age') }} --><div class="fluid-label">
				{{ Form::number('age_max', old('age_max'), ['class' => 'form-control','placeholder'=>'Max Age','required'=>'required']) }}</div>
			</div>
			<div class="form-group">
				<!-- {{ Form::label('height_min', 'Min Height') }} -->
				<div class="fluid-label">
				{{ Form::number('height_min', old('height_min'), ['class' => 'form-control','placeholder'=>'Min Height','required'=>'required']) }}</div>
			</div>
			<div class="form-group">
				<!-- {{ Form::label('height_max', 'Max Height') }} --><div class="fluid-label">
				{{ Form::number('height_max', old('height_max'), ['class' => 'form-control','placeholder'=>'Max Height','required'=>'required']) }}</div>
			</div>

			<div class="form-group">
				<!-- {{ Form::label('payment_terms', 'Payment Terms') }} -->
				<div class="fluid-label">
				{{ Form::select('ethnicity', ['Malay' => 'Malay', 'Indian' => 'Indian', 'Chinese' => 'Chinese',
				'Caucasian' => 'Caucasian', 'Panasian' => 'Pan-asian', 'Any' => 'Any'],old('ethnicity'),
				  ['class' => 'form-control2','placeholder'=>'-- Select Ethnicity --']) }}</div>
			</div>
			<div class="form-group">
				<!-- {{ Form::label('language', 'Language') }} --><div class="fluid-label">
				{{ Form::text('language', old('language'), ['class' => 'form-control','placeholder'=>'Language (Optional)']) }}</div>
			</div>
			<div class="form-group">
				<!-- {{ Form::label('additional_info', 'Additional Info') }} --><div class="fluid-label">
				{{ Form::text('additional_info', old('additional_info'), ['class' => 'form-control','placeholder'=>'Additional Info']) }}</div>
			</div>