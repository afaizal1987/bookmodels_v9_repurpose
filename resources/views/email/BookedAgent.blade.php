Dear {{$input['agencycontactperson']}},
<br/><br/>

You have booked <?php echo(ucfirst($input['talentname']))?> for the following Job:
<br/><br/>

Job ID: <?php echo(sprintf("%04s",$input['jobid']));?><br/>
Job Title: {{$input['jobtitle']}}<br/>
<br/>
Model Contact:<br/>
E-mail: {{$input['emailmodel']}}<br/>
Phone No: {{$input['phonenumbermodel']}}<br/>
<br/>
Good luck with your project. We wish you all the best!

<br/><br/><br/>
Regards,
<br/><br/>
BookModels.asia Team
