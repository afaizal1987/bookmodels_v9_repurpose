Dear {{$input['job_contact']}},
<br/><br/>

<?php echo(ucfirst($input['talentname']))?> has applied for the following Job:
<br/><br/>

Job ID: <?php echo(sprintf("%04s",$input['job_id']));?><br/>
Job Title: {{$input['job_title']}}<br/>
<br/>
Please review the application to book your models.

<br/><br/><br/>
Regards,
<br/><br/>
BookModels.asia Team
