<h2>Welcome to BookModels.asia<h2>
---------------------------------------------<br/>
Dear {{$nickname->nickname->nickname}},<br/>

<p>Thank you for creating your Model Profile at BookModels.asia talent booking platform. You're just one step away from getting booked for your first modelling job. Update your Profile with your latest portfolio and start applying for suitable jobs that fit your requirements.</p>

<br/>
Regards,
<br/>
BookModels.asia Team
