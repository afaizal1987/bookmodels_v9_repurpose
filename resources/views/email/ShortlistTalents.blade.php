Dear <?php echo ucfirst($input['talentname']);?>,
<br/><br/>

Congratulations, you have been shortlisted for the following Casting!
<br/><br/>

<?php if(($input['jobendcastingdate'])!= '0'){?>
Casting Date: <?php echo $castStartDate = date("j-F-Y", strtotime($input['jobstartcastingdate'])); ?>
	to <?php echo $castEndDate = date("j-F-Y", strtotime($input['jobendcastingdate'])); ?><br/>
<?php }else{?>
Casting Date: <?php echo $castStartDate = date("j-F-Y", strtotime($input['jobstartcastingdate'])); ?><br/>
<?php }?>

Casting Time: {{$input['jobstartcastingtime']}} to {{$input['jobendcastingtime']}} <br/>
Casting Location: {{$input['jobcastingvenue']}}, {{$input['jobcastinglocation']}}<br/>

Job Title: {{$input['jobtitle']}}<br/>
Agency: {{$input['agencyname']}}<br/>
Job Date: <?php echo $newDate = date("j-F-Y", strtotime($input['jobstartdate'])); ?><br/>
Call Time: {{$input['jobstarttime']}}<br/>
Venue: {{$input['jobvenue']}}<br/>
City: {{$input['jobcity']}}<br/>

<?php if(($input['joblanguage'])!= '0'){?>
  Language: {{$input['joblanguage']}}<br/>
<?php }?>
<?php if(($input['jobinfo'])!= '0'){?>
  Additional information: {{$input['jobinfo']}} <br/>
<?php }?>
<br/>
Contact Person: {{$input['agencycontactperson']}}<br/>
Phone No: {{$input['agencynumber']}}<br/>

<br/><br/>
Regards,
<br/><br/>
BookModels.asia Team
