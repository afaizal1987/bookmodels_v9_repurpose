Dear {{$input['job_contact']}},
<br/><br/>
We're sorry but {{$input['nickname']}} has cancelled the following Job booking:
<br/><br/>
Job ID: <?php echo(sprintf("%04s",$input['job_id']));?><br/>
Job Title: {{$input['job_title']}}
<br/><br/>
Hope you will be able to find a replacement as soon as possible. Good luck!
<br/><br/><br/>
Regards,
<br/><br/>
BookModels.asia Team
