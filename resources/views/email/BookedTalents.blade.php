Dear <?php echo ucfirst($input['talentname']);?>,
<br/><br/>

Congratulations, you have been booked for the following Job!
<br/><br/>

Job ID: <?php echo(sprintf("%04s",$input['jobid']));?><br/>
Job Title: {{$input['jobtitle']}}<br/>
Agency: {{$input['agencyname']}}<br/>
Job Date: <?php echo $newDate = date("j-F-Y", strtotime($input['jobstartdate'])); ?><br/>
Call Time: {{$input['jobstarttime']}}<br/>
Venue: {{$input['jobvenue']}}<br/>
City: {{$input['jobcity']}}<br/>

<?php if(($input['joblanguage'])!= '0'){?>
  Language: {{$input['joblanguage']}}<br/>
<?php }?>
<?php if(($input['jobcastingdate'])!= '0'){?>
  Casting Date: <?php echo $newDate = date("j-F-Y", strtotime($input['jobcastingdate'])); ?><br/>
  Casting TIme: {{$input['jobcastingtime']}}<br/>
<?php }?>
<?php if(($input['jobinfo'])!= '0'){?>
  Additional information: {{$input['jobinfo']}} <br/>
<?php }?>
<br/>
Contact Person: {{$input['agencycontactperson']}}<br/>
Phone No: {{$input['agencynumber']}}<br/>

<br/><br/>
Regards,
<br/><br/>
BookModels.asia Team
