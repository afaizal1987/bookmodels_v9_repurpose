@extends('layouts.app_agency')

@section('content')


		<!-- Begin Banner -->
		<div id="banner-container">
        <img src="/images/banner_models.jpg" class="banner">
        <div class="banner-text">
        <p>CREATE YOUR AGENCY PROFILE NOW »</p>
        </div>
        </div> <!-- End Banner -->

<br clear="all" />
<div id="content">

<div class="blurb"><h1>Join Malaysian talent agencies, fashion designers, photographers and many more on BookModels.asia. We have created a <span class="textred">simple, easy to use</span> booking platform for everyone.</h1></div>

		<div id="features">

        <div class="features-box">
		<h2>EASY BOOKING</h2>
		<p>We have simplified the talent booking process for you. Just list your jobs with the required details and we will match it to the suitable candidates. All you have to do is confirm the talents you want from the list of applicants.</p>
        </div>

        <div class="features-box">
		<h2>RELIABLE TALENTS</h2>
		<p>Once you have booked a talent, they can only cancel the job 48 hours before call time if they can't make it, which gives you ample time to book a replacement. This hopefully will reduce cases of last minute FFK!</p>
        </div>

        <div class="features-box">
		<h2>SECURE PLATFORM</h2>
		<p>We have built a secure platform where all your personal details will remain safe, and we will continue to improve our security to keep up with emerging threats. you can be assured that you're in safe hands.</p>
        </div>

		</div> <!-- End Features -->

        <div id="plans">

		<h2>Our Subscription Plans</h2>

		<div class="plan">
		<div class="plans">BASIC PLAN</div><br/>
		<div class="price">FREE<span class="price2"></span></div>
        <p>&nbsp;</p>

		<div class="plan-list">Post 2 jobs per month</div>
		<div class="plan-list">Listed in Agency directory</div>
		<div class="plan-list">Ad supported profile</div>
		<div class="plan-list">-</div>
		<div class="plan-list">-</div>
        <div class="plan-list">-</div>
        <div class="plan-list2"><p><a href="/agency/register" class="button">SIGN-UP NOW!</a></p></div>
		</div>

		<div class="plan2">
		<div class="plans">PRO PLAN</div><br/>
		<div class="price">RM29.90<span class="price2"> / month</span></div>
        <p>&nbsp;</p>

		<div class="plan-list">Post 10 jobs per month</div>
		<div class="plan-list">Listed as Premium Agency</div>
        <div class="plan-list">History of past jobs</div>
		<div class="plan-list">No ads on profile</div>
		<div class="plan-list">-</div>
        <div class="plan-list">-</div>
        <div class="plan-list2"><p><a href="/agency/register" class="button">SIGN-UP NOW!</a></p></div>
		</div>

		<div class="plan">
		<div class="plans">PREMIUM PLAN</div><br/>
		<div class="price">RM99.90<span class="price2"> / month</span></div>
        <p>&nbsp;</p>

		<div class="plan-list">Post unlimited jobs per month</div>
		<div class="plan-list">Listed as Premium Agency</div>
		<div class="plan-list">History of past jobs</div>
		<div class="plan-list">List models under Agency (coming soon)</div>
		<div class="plan-list">Payment guaranteed jobs (coming soon)</div>
        <div class="plan-list">30 days free trial *</div>
        <div class="plan-list2"><p><a href="/agency/register" class="button">SIGN-UP NOW!</a></p></div>
		</div>

<br clear="all" />
* Agency users will get to try the full PREMIUM features for 30 days, no credit card required. Once the trial expires, you'll be prompted to pick a suitable plan. Start booking!
		</div> <!-- End Plans -->

</div> <!-- End Content -->

@endsection
