@extends('layouts.app')

@section('title')
    <title>Contact Us - Model Booking Made Easy</title>
@stop

@section('meta')
<meta property="og:url" content="https://bookmodels.asia/contactus"/>
	<meta property="og:title" content="About BookModels.asia - Model Booking Made Easy"/>
	<meta name="keywords" content="model, talent, booking, agency, photographer, fashion designer">
	<meta name="description" content="Inquiries about Us?">
	<meta charset="utf-8">
	<meta property="og:image" content="https://bookmodels.asia/images/banner_models.jpg"/>
@endsection

@section('content')
<img src="/images/banner_content.jpg" class="banner-content">

<div id="content">
    <div id="loginbox">

		<div>

			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
			  <button type="button" class="close" data-dismiss="alert">×</button>
					<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('failure'))
			<div class="alert alert-danger alert-block">
			  <button type="button" class="close" data-dismiss="alert">×</button>
					<strong>{{ $message }}</strong>
			</div>
			@endif

			<h2 style="text-align:center">CONTACT US NOW!</h2>

      		{{ Form::open(['action' => 'ContactUsController@contactuspost', 'method' => 'POST']) }}

      			<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
      				<div class="fluid-label">
      				{{ Form::text('title', old('title'), ['class' => 'form-control','placeholder'=>'Title', 'required'=>'required']) }}</div>

				@if ($errors->has('title'))
                  <span class="help-block">
                      <strong>{{ $errors->first('title') }}</strong>
                  </span>
				@endif
      			</div>

      			<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
      				<div class="fluid-label">
                {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'required']) !!}</div>
      			@if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
				@endif
				</div>

				<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
      				<div class="fluid-label">
                {!! Form::text('email', old('email'), ['class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required']) !!}</div>
      			@if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
				@endif
				</div>

      			<div class="form-group {{ $errors->has('inquiry') ? 'has-error' : '' }}">
      				<div class="fluid-label">
      				{!! Form::textarea('inquiry', old('inquiry'), ['class'=>'form-control', 'placeholder'=>'Enter Inquiry', 'required'=>'required']) !!}</div>
      			@if ($errors->has('inquiry'))
                  <span class="help-block">
                      <strong>{{ $errors->first('inquiry') }}</strong>
                  </span>
				@endif
				</div>

				<div class="form-group {{ $errors->has('math') ? 'has-error' : '' }}">
      				<div class="fluid-label">
      				{{ Form::text('math', old('math'), ['class' => 'form-control','placeholder'=>'What is 3 + 5', 'required'=>'required']) }}</div>
      			@if ($errors->has('math'))
                  <span class="help-block">
                      <strong>{{ $errors->first('math') }}</strong>
                  </span>
				@endif
				</div>


			<input class="buttonlink pink" style="height:35px;padding-bottom:5px;" type="submit" value="SUBMIT INQUIRY"/>
			{{ Form::close() }}
		</div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/fluid-labels.js') }}"></script>
<script type='text/javascript'>
  $('.fluid-label').fluidLabel();
</script>

@endsection
