@extends('voting.layout.auth')

@section('content')

<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">

            <div id="profile">

            <div class="profileimg-box"><br/>
            <img class="profileimg" src="/images/ModelSearch_Logo.png" alt="Profile Image"/>
            </div>
            <div class="profile-box">
              <h2>Vote for the Top 30 Finalists!</h2>
               We have selected these 30 amazing contestants for the first round of BookModels.asia Model Search 2017.
               Vote for your favourite contestant now! Online votes will count for 50% of the total score and
               our panel of judges will be scoring the remaining 50%. Please login via Facebook to vote.
			</div> <!-- End Profile Box -->
            <div class="agencymeta-box">


        @if(Auth::guest())
        <!--<h2>Login to vote!</h2>

        <form method="link" action='/modelsearch/redirectv'>
          <input class="buttonlink blue" type="submit" value="LOGIN WITH FACEBOOK" /><br/><br/>
        </form>
        For more information on our Model Search programme, follow us on social media:<br/><br/>
        @else
        <h2>Want to know more?</h2>
        Follow us on social media:<br/><br/>-->

        @endif




                <a href="http://facebook.com/bookmodelsasia" target="_blank"><img src="/images/ico_fb.png" width="48" height="47" alt="Facebook" />
                </a> <a href="http://twitter.com/bookmodelsasia" target="_blank"><img src="/images/ico_tw.png" width="48" height="47" alt="Twitter" />
                </a> <a href="http://instagram.com/bookmodelsasia" target="_blank"><img src="/images/ico_ig.png" width="48" height="47" alt="Instagram" /></a>
			</div> <!-- End Profile Box -->

                <br clear="all" />

                <h1>Top 30 Finalists!</h1>
                <?php $i=1;?>
                @foreach($profiles as $profile)
                <div class="voting-box">
                  <img src="/uploads/avatars/{{$profile->avatar}}" alt="Profile Image"/>
                  <!--<input id='vote' class="buttonlink2 pink" data-image="{{$profile->avatar}}" data-profileid="{{$profile->profile_id}}" data-userid="{{$voting_id}}"
                  data-nickname="{{$profile->nickname}}" type="submit" value="VOTE NOW" />-->
                  <span class="bmano">{{$i++}}</span>
                  <?php  echo strtok($profile->nickname,' ');?>,
                  <?php
                    $var =  $profile->birthday;
                    $date = date('Y-m-d', strtotime($var));
                    echo date_diff(date_create($date), date_create('today'))->y;
                  ?>
                  ({{$profile->vote}} votes)<br/><br/>
  				      </div> <!-- End Voting Box -->
                @endforeach

                <br clear="all" />
            </div> <!-- End Profile -->

	</div> <!-- End Content -->
@endsection

@section('scripts')
<script type='text/javascript'>
$('input#vote').on('click',function(){

  var profileid = $(this).data('profileid');
  var userid = $(this).data('userid');
  var image = $(this).data('image');
  var nickname = $(this).data('nickname');
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  swal({
    title: "Vote for "+nickname+"?",
    // text: "Are you sure?",
    // type: "warning",
     imageUrl: "/uploads/avatars/"+image,
     imageSize: '180x110',
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes!",
    closeOnConfirm: true
    },function(){

      $.ajax({
        type: "post",
        url: "{{url('modelsearch/verify')}}",
        data: {profile_id:profileid, user_id:userid},
        success: function (response) {
          if(response=='done'){
            swal({
              title: "Sorry, you have already voted",
              type: "warning",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.reload();
            });
          }else{
            swal({
              title: "Voted!",
              type: "success",
               timer: 1700,
               showConfirmButton: true
            },function(){
              location.reload();
            });
          }
        }
    });
  });

});
</script>
@endsection
