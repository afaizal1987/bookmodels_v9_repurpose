<!DOCTYPE html>
<html>
<head>
<title>BookModels.asia - Model Booking Made Easy</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">

<meta name="keywords" content="">
<meta name="description" content="">
<meta charset="utf-8">

<meta property="og:title" content="Vote for the Top 30 Finalists! - Model Search 2017"/>
<meta property="og:type" content="Talent and Agency"/>
<meta property="og:url" content="www.bookmodels.aisa/modelsearch"/>
<meta property="og:image" content="http://bookmodels.asia/images/BookModelsasia_backdrop_V1.png"/>
<meta property="og:site_name" content="Bookmodels.asia"/>
<meta property="fb:app_id" content="328585973905734"/>
<meta property="og:description" content="Vote for the Top 30 Finalists! - Model Search 2017. BookModels.asia is a fresh new platform to effectively connect Malaysian models and aspiring talents directly to verified agencies or photographers."/>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
<!-- <link rel="stylesheet" href="/css/app.css"> -->
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/sweetalert.css">
<!-- <link rel="stylesheet" href="/css/column_normaliser.css"> -->
<link rel="stylesheet" href="/css/fluid-labels.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css">

<script src="https://code.jquery.com/jquery-1.12.4.min.js"
			integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
			crossorigin="anonymous"></script>



<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!--
<div id="wrapper">
-->

	<div class="site-wrap"><!-- for slide menu -->

	<header class="clearfix">
		<div class="header-inner">

		<!-- <div id="logo"><a href="/voting2/index"><img src="/images/BookModels_Logo.png" class="logo"></a></div> -->
		<div id="logo">
			<a href="{{ url('/modelsearch/logout') }}"
					onclick="event.preventDefault();
									 document.getElementById('logout-form').submit();">
					<img src="/images/BookModels_Logo.png" class="logo">
			</a>
			<form id="logout-form" action="{{ url('/modelsearch/logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
			</form>
		</div>
				<!-- <div id="headerlogin"><a href=" /login" class="button">LOGIN</a></div> -->


				<div style="margin: 0px 0 10px 0;"></div>
        <!--<div id="search"><input type="text" id="searchField" placeholder="Search WebTVasia" name="search" value="" /></div>-->
        </div>
	</header>
				@yield('content')

  <footer>
	<a href="http://facebook.com/bookmodelsasia" target="_blank">
		<img src="/images/ico_fb.png" width="57" height="56" alt="Facebook" /></a>
	<a href="http://twitter.com/bookmodelsasia" target="_blank">
		<img src="/images/ico_tw.png" width="57" height="56" alt="Twitter" /></a>
	<a href="http://instagram.com/bookmodelsasia" target="_blank">
		<img src="/images/ico_ig.png" width="57" height="56" alt="Instagram" /></a>
	<a href="https://www.youtube.com/channel/UCNkRGbulbxwlIupLnZShVhg" target="_blank">
		<img src="/images/ico_yt.png" width="57" height="56" alt="YouTube" /></a>
	<br/><br/>

	&copy; Copyright 2017 BookModels.asia. All rights reserved.<br/>
	<a href="terms.php">Terms of Use</a> | <a href="privacy.php">Privacy Policy</a> | <a href="contact.php">Contact</a><br/>
	</footer>


	<script src="/js/app.js" async></script>
	<script src="/js/sweetalert.min.js" async></script>
	@yield('scripts')

	</div><!-- /.site_wrap -->

	</body>
	</html>
