@extends('voting.layout.auth')

@section('content')

<img src="/images/banner_content.jpg" class="banner-content">
<div id="content">

            <div id="profile">

            <div class="profileimg-box"><br/>
            <img class="profileimg" src="/images/ModelSearch_Logo.png" alt="Profile Image"/>
            </div>
            <div class="profile-box">
              <h2>Vote for the Top 30 Finalists!</h2>
               We have selected these 30 amazing contestants for the first round of BookModels.asia Model Search 2017.
               Vote for your favourite contestant now! Online votes will count for 50% of the total score and
               our panel of judges will be scoring the remaining 50%. Please login via Facebook to vote.
			</div> <!-- End Profile Box -->
            <div class="agencymeta-box">

        <h2>Want to know more?</h2>
        Follow us on social media:<br/><br/>




                <a href="http://facebook.com/bookmodelsasia" target="_blank"><img src="/images/ico_fb.png" width="48" height="47" alt="Facebook" />
                </a> <a href="http://twitter.com/bookmodelsasia" target="_blank"><img src="/images/ico_tw.png" width="48" height="47" alt="Twitter" />
                </a> <a href="http://instagram.com/bookmodelsasia" target="_blank"><img src="/images/ico_ig.png" width="48" height="47" alt="Instagram" /></a>
			</div> <!-- End Profile Box -->

                <br clear="all" />

                <h1>Top 30 Finalists!</h1>
                <?php $i=1;?>
                @foreach($profiles as $profile)
                <div class="voting-box">
                  <img src="/uploads/avatars/{{$profile->avatar}}" alt="Profile Image"/>
				  <!--<form method="link" action='/modelsearch/redirectv'>
					<input class="buttonlink2 pink" type="submit" value="VOTE NOW" />
				  </form>-->

                  <span class="bmano">{{$i++}}</span>
                  <?php  echo strtok($profile->nickname,' ');?>,
                  <?php
                    $var =  $profile->birthday;
                    $date = date('Y-m-d', strtotime($var));
                    echo date_diff(date_create($date), date_create('today'))->y;
                  ?>
                  ({{$profile->vote}} votes)<br/><br/>
  				      </div> <!-- End Voting Box -->
                @endforeach


                <br clear="all" />
            </div> <!-- End Profile -->

	</div> <!-- End Content -->
@endsection
